import { defineConfig } from "vite";
import react from "@vitejs/plugin-react";
import svgr from "vite-plugin-svgr";
import { NodeGlobalsPolyfillPlugin } from "@esbuild-plugins/node-globals-polyfill";
import { NodeModulesPolyfillPlugin } from "@esbuild-plugins/node-modules-polyfill";
import rollupNodePolyFill from "rollup-plugin-node-polyfills";
import builtins from "rollup-plugin-node-builtins";
import viteCompression from "vite-plugin-compression";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [svgr(), react()],
  optimizeDeps: {
    esbuildOptions: {
      plugins: [
        NodeGlobalsPolyfillPlugin({
          process: true,
          buffer: true,
        }),
        NodeModulesPolyfillPlugin(),
      ],
    },
    disabled: false,
    include: [
      "@mui/material",
      "@mui/x-date-pickers",
      "@mui/material/styles",
      "@mui/material/LinearProgress",
      "@mui/x-date-pickers/AdapterDayjs",
      "@mui/x-date-pickers/LocalizationProvider",
      "@mui/x-date-pickers/CalendarPicker",
    ],
  },
  build: {
    rollupOptions: {
      plugins: [
        // Enable rollup polyfills plugin
        // used during production bundling
        builtins(),
        rollupNodePolyFill(),
        viteCompression(),
      ],
    },
    commonjsOptions: {
      include: [],
    },
  },
});
