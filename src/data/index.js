import { configureStore } from "@reduxjs/toolkit";
import exampleReducer from "./slices/exampleSlice";
import authReducer from "./slices/authSlice";
import settingsReducer from "./slices/settingsSlice";
import registerdataReducer from "./slices/registerData";

import contentReducer from "./slices/contentSlice";
import userReducer from "./slices/userSlice";
import walletReducer from "./slices/walletSlice";

import { reducer as modalsReducer } from "./slices/modals";
import subjectsReducer from "./slices/subjectsSlice";
import recordingsReducer from "./slices/recordingsSlice";
import chaptersReducer from "./slices/chaptersSlice";
import lessonsReducer from "./slices/lessonsSlice";
import commentsReducer from "./slices/commentsSlice";
import eventsReducer from "./slices/eventsSlice";
import offersReducer from "./slices/offersSlice";
import searchReducer from "./slices/search";

export const store = configureStore({
  reducer: {
    example: exampleReducer,
    auth: authReducer,
    settings: settingsReducer,
    content: contentReducer,
    modals: modalsReducer,
    subjects: subjectsReducer,
    recordings: recordingsReducer,
    chapters: chaptersReducer,
    lessons: lessonsReducer,
    comments: commentsReducer,
    user: userReducer,
    events: eventsReducer,
    wallet: walletReducer,
    offers: offersReducer,
    search: searchReducer,
    registerData: registerdataReducer,
  },
  devTools: import.meta.env.VITE_ENABLE_REDUX_DEV_TOOLS,
});
