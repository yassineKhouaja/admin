// import jwt from "jsonwebtoken";
import mock from "../../utils/mock";
import wait from "../../utils/wait";

const JWT_SECRET = "ostedhy-top-secret-key";
const JWT_EXPIRES_IN = "2 days";
const sessions = [
  {
    id: "5e86809283e28b96d2d38537",
    name: "Série de révision N°1",
    participants_nb: "number",
    price: "14",
    start_date: "14/01/2021 08:00",
    end_date: "14/01/2021 10:00",
    description: "string",
    start_url: "string",
    webinar_key: "string",
    webinar_url: "string",
    created_at: "12/01/2021",
    updated_at: "Date",
    deleted_at: "Date",
    group_session_id: "1",
    webinar_account_id: "12",
    session_type_id: "123",
  },
  {
    id: "5e86809283e28b96d2d38538",
    name: "Série de révision N°2",
    participants_nb: "number",
    price: "14",
    start_date: "14/01/2021 12:00",
    end_date: "14/01/2021 14:00",
    description: "string",
    start_url: "string",
    webinar_key: "string",
    webinar_url: "string",
    created_at: "12/01/2021",
    updated_at: "Date",
    deleted_at: "Date",
    group_session_id: "1",
    webinar_account_id: "12",
    session_type_id: "123",
  },
  {
    id: "5e86809283e28b96d2d38536",
    name: "Série de révision N°3",
    participants_nb: "number",
    price: "14",
    start_date: "14/01/2021 14:00",
    end_date: "14/01/2021 15:00",
    description: "string",
    start_url: "string",
    webinar_key: "string",
    webinar_url: "string",
    created_at: "12/01/2021",
    updated_at: "Date",
    deleted_at: "Date",
    group_session_id: "1",
    webinar_account_id: "12",
    session_type_id: "123",
  },
];
const chapters = [
  {
    id: "5e86809283e28b96d2d38537",
    teacher_id: 1,
    name: "Nombre Complex",
    short_description: "nombre complex is ...",
    long_description: "long description for this chapter",
    approved: true,
    price: 15,
    discount_price: 5,
    status: true,
    file_name: "file1",
    file_size: "14",
    slug: "slug1",
    reduction_type_id: 1,
    chapter_type_id: 1,
    student_level_id: 1,
    session_id: "124578",
    is_Archived: false,
    approved_at: "14/01/2021",
    created_at: "12/01/2021",
    updated_at: "15/10/2022",
  },
  {
    id: "5e86809283e28b96d2d38538",
    teacher_id: 2,
    name: "Nombre Complex",
    short_description: "nombre complex is ...",
    long_description: "long description for this chapter",
    approved: true,
    price: 15,
    discount_price: 5,
    status: true,
    file_name: "file1",
    file_size: "14",
    slug: "slug1",
    reduction_type_id: 2,
    chapter_type_id: 2,
    student_level_id: 2,
    session_id: "124570",
    is_Archived: false,
    approved_at: "14/01/2021",
    created_at: "12/01/2021",
    updated_at: "15/10/2022",
  },
  {
    id: "5e86809283e28b96d2d38539",
    teacher_id: 3,
    name: "Nombre Complex",
    short_description: "nombre complex is ...",
    long_description: "long description for this chapter",
    approved: true,
    price: 15,
    discount_price: 5,
    status: true,
    file_name: "file1",
    file_size: "14",
    slug: "slug1",
    reduction_type_id: 3,
    chapter_type_id: 3,
    student_level_id: 3,
    session_id: "124579",
    is_Archived: false,
    approved_at: "14/01/2021",
    created_at: "12/01/2021",
    updated_at: "15/10/2022",
  },
  {
    id: "5e86809283e28b96d2d38539",
    teacher_id: 3,
    name: "Nombre Complex",
    short_description: "nombre complex is ...",
    long_description: "long description for this chapter",
    approved: true,
    price: 15,
    discount_price: 5,
    status: true,
    file_name: "file1",
    file_size: "14",
    slug: "slug1",
    reduction_type_id: 3,
    chapter_type_id: 3,
    student_level_id: 3,
    session_id: "124579",
    is_Archived: false,
    approved_at: "14/01/2021",
    created_at: "12/01/2021",
    updated_at: "15/10/2022",
  },
  {
    id: "5e86809283e28b96d2d38539",
    teacher_id: 3,
    name: "Nombre Complex",
    short_description: "nombre complex is ...",
    long_description: "long description for this chapter",
    approved: true,
    price: 15,
    discount_price: 5,
    status: true,
    file_name: "file1",
    file_size: "14",
    slug: "slug1",
    reduction_type_id: 3,
    chapter_type_id: 3,
    student_level_id: 3,
    session_id: "124579",
    is_Archived: false,
    approved_at: "14/01/2021",
    created_at: "12/01/2021",
    updated_at: "15/10/2022",
  },
  {
    id: "5e86809283e28b96d2d38539",
    teacher_id: 3,
    name: "Nombre Complex",
    short_description: "nombre complex is ...",
    long_description: "long description for this chapter",
    approved: true,
    price: 15,
    discount_price: 5,
    status: true,
    file_name: "file1",
    file_size: "14",
    slug: "slug1",
    reduction_type_id: 3,
    chapter_type_id: 3,
    student_level_id: 3,
    session_id: "124579",
    is_Archived: false,
    approved_at: "14/01/2021",
    created_at: "12/01/2021",
    updated_at: "15/10/2022",
  },
];
let group_sessions = [
  {
    id: 1,
    name: "Complex1",
    description: "bla bla bla",
    nb_max_participation: 100,
    class_id: 1,
    teacher_id: 5,
    student_level_id: 1,
    subject_id: 2,
    sessions: [
      {
        id: 3,
        name: "Session 3",
        description: null,
        start_date: "2022-09-14T08:00:00.000Z",
        end_date: "2022-09-14T10:00:00.000Z",
        price: 15,
        sessionAttachments: [],
      },
      {
        id: 3,
        name: "Session 4",
        description: null,
        start_date: "2022-09-14T08:00:00.000Z",
        end_date: "2022-09-14T10:00:00.000Z",
        price: 15,
        sessionAttachments: [],
      },
      {
        id: 3,
        name: "Session 5",
        description: null,
        start_date: "2022-09-14T08:00:00.000Z",
        end_date: "2022-09-14T10:00:00.000Z",
        price: 15,
        sessionAttachments: [],
      },
    ],
    subjects: {
      id: 2,
      name: "Physique",
    },
    StudentLevels: {
      id: 1,
      name: "Hard",
    },
    classes: {
      id: 1,
      name: "7eme",
    },
    teachers: {
      first_name: "selmen",
      last_name: "karkni",
      photo_url: null,
    },
  },
];

mock.onGet("/api/chapters").reply(async (config) => {
  try {
    await wait(1000);
    const { Authorization } = config.headers;

    if (!Authorization) {
      return [401, { message: "Authorization token missing" }];
    }

    return [
      200,
      {
        chapters: chapters,
      },
    ];
  } catch (err) {
    console.error(err);
    return [500, { message: "Internal server error" }];
  }
});
//group sessions
mock.onGet("/api/sessions/week").reply(async (config) => {
  try {
    await wait(1000);
    const { Authorization } = config.headers;

    if (!Authorization) {
      return [401, { message: "Authorization token missing" }];
    }

    return [
      200,
      {
        group_sessions: group_sessions,
      },
    ];
  } catch (err) {
    console.error(err);
    return [500, { message: "Internal server error" }];
  }
});
//get sessions
mock.onGet("/api/sessions").reply(async (config) => {
  try {
    await wait(1000);
    const { Authorization } = config.headers;

    if (!Authorization) {
      return [401, { message: "Authorization token missing" }];
    }

    return [
      200,
      {
        sessions: sessions,
      },
    ];
  } catch (err) {
    console.error(err);
    return [500, { message: "Internal server error" }];
  }
});
//get subjects
mock.onGet("/api/subjects").reply(async (config) => {
  try {
    const { Authorization } = config.headers;

    if (!Authorization) {
      return [401, { message: "Authorization token missing" }];
    }
    return [
      200,
      {
        subjects: [],
      },
    ];
  } catch (err) {
    console.error(err);
    return [500, { message: "Internal server error" }];
  }
});
