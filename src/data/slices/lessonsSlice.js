import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "../../utils/axios";

const initialState = {
  sections: {
    data: [],
    status: "idle",
    chapter: null,
    error: null,
    message: null,
  },
  refundReasons: {
    data: [],
    status: "idle",
    chapter: null,
    error: null,
    message: null,
  },
  refund: {
    data: [],
    status: "idle",
    chapter: null,
    error: null,
    message: null,
  },
  comments: {
    data: [],
    status: "idle",
    chapter: null,
    error: null,
    message: null,
  },
};

export const getSections = createAsyncThunk("lessons/sections", async (id) => {
  let data;
  try {
    const response = await axios.get(`/api/chapters/student/${id}`);
    data = await response.data;
    if (response.status === 200) {
      return data;
    }
    throw new Error(response.statusText);
  } catch (err) {
    const error = err;
    console.log(err);
    return Promise.reject(error.message ? error.message : data?.message);
  }
});

export const getRefundReasons = createAsyncThunk("lessons/getRefundReasons", async () => {
  let data;
  try {
    const response = await axios.get(`/api/refunds/reasons`);
    data = await response.data;
    if (response.status === 200) {
      return data;
    }
    throw new Error(response.statusText);
  } catch (err) {
    const error = err;
    console.log(err);
    return Promise.reject(error.message ? error.message : data?.message);
  }
});

export const postRefundReasons = createAsyncThunk("lessons/postRefundReasons", async (params) => {
  let data;
  try {
    const body = { ...params };
    if (body.description === "") delete body.description;
    const response = await axios.post(`/api/refunds`, body);
    data = await response.data;
    console.log("refunds response.status", response.status, response.status === 201);
    if (response.status === 201) {
      return data;
    }
    throw new Error(response.statusText);
  } catch (err) {
    return Promise.reject(err);
  }
});

export const lessonsSlice = createSlice({
  name: "lessons",
  initialState,
  reducers: {},
  extraReducers: {
    // getSections
    [getSections.pending]: (state) => {
      state.sections.status = "loading";
    },
    [getSections.fulfilled]: (state, action) => {
      state.sections.status = "succeeded";
      state.sections.data = action.payload.payload;
    },
    [getSections.rejected]: (state, action) => {
      state.sections.status = "failed";
      state.sections.error = action.error.message;
    },
    // refundReasons
    [getRefundReasons.pending]: (state) => {
      state.refundReasons.status = "loading";
    },
    [getRefundReasons.fulfilled]: (state, action) => {
      state.refundReasons.status = "succeeded";
      state.refundReasons.data = action.payload.payload;
    },
    [getRefundReasons.rejected]: (state, action) => {
      state.refundReasons.status = "failed";
      state.refundReasons.error = action.error.message;
    },
    [postRefundReasons.pending]: (state) => {
      state.refund.status = "loading";
    },
    [postRefundReasons.fulfilled]: (state, action) => {
      state.refund.status = "succeeded";
      state.refund.data = action.payload.payload;
    },
    [postRefundReasons.rejected]: (state, action) => {
      state.refund.status = "failed";
      state.refund.error = action.error.message;
    },
  },
});

export const {} = lessonsSlice.actions;

export default lessonsSlice.reducer;
