import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "../../utils/axios";

const initialState = {
  comments: {
    data: [],
    status: "idle",
    chapter: null,
    error: null,
    message: null,
  },
  reviews: {
    data: [],
    status: "idle",
    chapter: null,
    error: null,
    message: null,
  },
};

export const getComments = createAsyncThunk("comments/getComments", async (params) => {
  let data;
  try {
    const response = await axios.get(`/api/lessons/${params}/comments`);
    data = await response.data;
    if (response.status === 200) {
      return data;
    }
    throw new Error(response.statusText);
  } catch (err) {
    const error = err;
    console.log(err);
    return Promise.reject(error.message ? error.message : data?.message);
  }
});

export const postComment = createAsyncThunk("comments/postComment", async (params) => {
  let data;
  try {
    const response = await axios.post(`/api/comments`, params);
    data = await response.data;
    if (response.status === 201) {
      return data;
    }
    throw new Error(response.statusText);
  } catch (err) {
    const error = err;
    console.log(err);
    return Promise.reject(error.message ? error.message : data?.message);
  }
});

export const postReply = createAsyncThunk("comments/postReply", async (params) => {
  let data;
  try {
    const response = await axios.post(`/api/comments/${params.id}/replies`, params);
    data = await response.data;
    if (response.status === 201) {
      return data;
    }
    throw new Error(response.statusText);
  } catch (err) {
    const error = err;
    console.log(err);
    return Promise.reject(error.message ? error.message : data?.message);
  }
});

export const postCommentReaction = createAsyncThunk(
  "comments/postCommentsReaction",
  async (params) => {
    let data;
    try {
      const response = await axios.put(`/api/comments/${params.id}/reactions/${params.reactionId}`);
      data = await response.data;
      if (response.status === 200) {
        return data;
      }
      throw new Error(response.statusText);
    } catch (err) {
      const error = err;
      console.log(err);
      return Promise.reject(error.message ? error.message : data?.message);
    }
  }
);

export const postReplyReaction = createAsyncThunk(
  "comments/postCommentsReaction",
  async (params) => {
    let data;
    try {
      const response = await axios.put(`/api/replies/${params.id}/reactions/${params.reactionId}`);
      data = await response.data;
      if (response.status === 200) {
        return data;
      }
      throw new Error(response.statusText);
    } catch (err) {
      const error = err;
      console.log(err);
      return Promise.reject(error.message ? error.message : data?.message);
    }
  }
);

export const getReviews = createAsyncThunk("comments/getReviews", async (params) => {
  let data;
  try {
    const response = await axios.get(`/api/review?chapter_id=${params}&isVerified=1`);
    data = await response.data;
    if (response.status === 200) {
      return data;
    }
    throw new Error(response.statusText);
  } catch (err) {
    const error = err;
    console.log(err);
    return Promise.reject(error.message ? error.message : data?.message);
  }
});
export const postReview = createAsyncThunk("comments/postReview", async (params) => {
  let data;
  params.content === "" && delete params.content;
  try {
    const response = await axios.post(`/api/review`, params);
    data = await response.data;
    if (response.status === 201) {
      return data;
    }
    throw new Error(response.statusText);
  } catch (err) {
    const error = err;
    console.log(err);
    return Promise.reject(error.message ? error.message : data?.message);
  }
});

export const commentsSlice = createSlice({
  name: "comments",
  initialState,
  reducers: {},
  extraReducers: {
    [getComments.pending]: (state) => {
      state.comments.status = "loading";
    },
    [getComments.fulfilled]: (state, action) => {
      state.comments.status = "succeeded";
      state.comments.data = action.payload.payload;
    },
    [getComments.rejected]: (state, action) => {
      state.comments.status = "failed";
      state.comments.error = action.error.message;
    },
    [getReviews.pending]: (state) => {
      state.reviews.status = "loading";
    },
    [getReviews.fulfilled]: (state, action) => {
      state.reviews.status = "succeeded";
      state.reviews.data = action.payload.payload;
    },
    [getReviews.rejected]: (state, action) => {
      state.reviews.status = "failed";
      state.reviews.error = action.error.message;
    },
  },
});

export const {} = commentsSlice.actions;

export default commentsSlice.reducer;
