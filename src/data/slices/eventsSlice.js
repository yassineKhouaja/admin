import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "../../utils/axios";

const initialState = {
  events: {
    data: [],
    state: "idle",
    error: null,
    message: null,
    loading: null,
    copyEventState: "idle",
  },

  subscription: {
    data: {},
    state: "idle",
    error: null,
    message: null,
    loading: null,
  },
  attachments: {
    data: [],
    state: "idle",
    error: null,
    message: null,
    loading: null,
  },
};

//get sessions per week
export const getSessions = createAsyncThunk("sessions/week", async (value) => {
  let data;
  try {
    const response = await axios.post(`/api/sessions/week`, value);
    data = await response.data;
    if ((response.status = 200)) {
      return data;
    }
    throw new Error(response.statusText);
  } catch (err) {
    const error = err;
    return Promise.reject(error.message ? error.message : data?.message);
  }
});

//buy group session
export const buyGroupSession = createAsyncThunk(
  "sessions/bye",
  async (values) => {
    let { id, recording } = values;
    let data;
    try {
      const response = await axios.post(
        `/api/transactions/user/subscripe/${id}`,
        { recording: recording }
      );
      data = await response.data;
      if ((response.status = 200)) {
        return data;
      }
      throw new Error(response.statusText);
    } catch (err) {
      const error = err;
      return Promise.reject(error.message ? error.message : data?.message);
    }
  }
);

//get subscription
export const getSubscription = createAsyncThunk(
  "sessions/subs",
  async (values) => {
    let { id, session_id } = values;

    let data;
    // session_id
    try {
      const response = await axios.get(
        `/api/subscriptions?user_id=${id}&session_id=${session_id}`
      );
      data = await response.data;
      if ((response.status = 200)) {
        return data;
      }
      throw new Error(response.statusText);
    } catch (err) {
      const error = err;
      return Promise.reject(error.message ? error.message : data?.message);
    }
  }
);
//get attachments
export const getAttachments = createAsyncThunk("sessions/attch", async (id) => {
  let data;
  try {
    const response = await axios.get(
      `/api/session-attachments?session_id=${id}`
    );
    data = await response.data;
    if ((response.status = 200)) {
      return data;
    }
    throw new Error(response.statusText);
  } catch (err) {
    const error = err;
    return Promise.reject(error.message ? error.message : data?.message);
  }
});

export const eventSlice = createSlice({
  name: "events",
  initialState,
  reducers: {},
  extraReducers: {
    //expire soon chapters
    [getSessions.pending]: (state) => {
      state.events.error = null;
      state.events.copyEventState = "loading";
    },
    [getSessions.fulfilled]: (state, action) => {
      const events = action.payload.payload;
      state.events.data = events;
      state.events.copyEventState = "success";
    },
    [getSessions.rejected]: (state, action) => {
      state.events.error = action.error.message;
      state.events.copyEventState = "error";
    },
    //bye group sessions
    [buyGroupSession.pending]: (state) => {
      state.events.error = null;
      state.events.state = "loading";
    },
    [buyGroupSession.fulfilled]: (state, action) => {
      state.events.state = "success";
    },
    [buyGroupSession.rejected]: (state, action) => {
      state.events.error = action.error.message;
      state.events.state = "error";
    },
    //get susb
    [getSubscription.pending]: (state) => {
      state.subscription.error = null;
      state.subscription.state = "loading";
    },
    [getSubscription.fulfilled]: (state, action) => {
      const subs = action.payload.payload;
      state.subscription.data = subs;
      state.subscription.state = "success";
    },
    [getSubscription.rejected]: (state, action) => {
      state.subscription.error = action.error.message;
      state.subscription.state = "error";
    },
    //get attachments
    [getAttachments.pending]: (state) => {
      state.attachments.error = null;
      state.attachments.state = "loading";
    },
    [getAttachments.fulfilled]: (state, action) => {
      state.attachments.data = action.payload.payload;
      state.attachments.state = "success";
    },
    [getAttachments.rejected]: (state, action) => {
      state.attachments.error = action.error.message;
      state.attachments.state = "error";
    },
  },
});

export const {} = eventSlice.actions;

export default eventSlice.reducer;
