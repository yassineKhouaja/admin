import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "../../utils/axios";

const initialState = {
  data: null,
  error: null,
  message: null,
  state: "idle",
};

// search
export const search = createAsyncThunk("search/data", async (query) => {
  let data;
  try {
    const response = await axios.get(`api/search/student?search=${query}`);
    data = await response.data;
    if ((response.status = 200)) {
      return data;
    }
    throw new Error(response.statusText);
  } catch (err) {
    const error = err;
    return Promise.reject(error.message ? error.message : data?.message);
  }
});

export const searchSlice = createSlice({
  name: "search",
  initialState,
  reducers: {},
  extraReducers: {
    //search
    [search.pending]: (state) => {
      state.error = null;
      state.state = "loading";
    },
    [search.fulfilled]: (state, action) => {
      const { payload } = action.payload;
      state.data = payload;
      state.state = "succeeded";
    },
    [search.rejected]: (state, action) => {
      state.error = action.error.message;
      state.state = "error";
    },
  },
});

export default searchSlice.reducer;
