import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "../../utils/axios";

const initialState = {
  offers: {
    data: [],
    status: "idle",
    chapter: null,
    error: null,
    message: null,
  },
};

export const getOffers = createAsyncThunk("offers/getOffers", async (params) => {
  let data;
  try {
    const response = await axios.get(`/api/offer-purchase`);
    data = await response.data;
    if (response.status === 200) {
      return data;
    }
    throw new Error(response.statusText);
  } catch (err) {
    const error = err;
    console.log(err);
    return Promise.reject(error.message ? error.message : data?.message);
  }
});

export const buyOffer = createAsyncThunk("offers/buyOffer", async (params) => {
  let data;
  try {
    const response = await axios.post("/api/offer-purchase/buy", params);
    data = await response.data;
    if (response.status === 201) {
      return data;
    }
    throw new Error(response.statusText);
  } catch (err) {
    return Promise.reject(err);
  }
});

export const offersSlice = createSlice({
  name: "offers",
  initialState,
  reducers: {},
  extraReducers: {
    [getOffers.pending]: (state) => {
      state.offers.status = "loading";
    },
    [getOffers.fulfilled]: (state, action) => {
      state.offers.status = "succeeded";
      state.offers.data = action.payload.payload;
    },
    [getOffers.rejected]: (state, action) => {
      state.offers.status = "failed";
      state.offers.error = action.error.message;
    },
  },
});

export const {} = offersSlice.actions;

export default offersSlice.reducer;
