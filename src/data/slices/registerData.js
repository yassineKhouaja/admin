import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  data: {},
};

export const registerDataSlice = createSlice({
  name: "registerData",
  initialState,
  reducers: {
    saveData: (state, { payload }) => {
      state.data = payload;
    },
  },
});

export const { saveData } = registerDataSlice.actions;

export default registerDataSlice.reducer;
