import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "../../utils/axios";

const initialState = {
  governments: {
    data: [],
    state: "idle",
    error: null,
    message: null,
    loading: null,
  },
  classes: {
    data: [],
    state: "idle",
    error: null,
    message: null,
    loading: null,
  },
  expireSoonChapters: {
    data: [],
    state: "idle",
    chapter: null,
    error: null,
    message: null,
    loading: null,
  },
  lastChapters: {
    data: [],
    state: "idle",
    chapter: null,
    error: null,
    message: null,
  },
  purchaseChapters: {
    data: [],
    state: "idle",
    chapter: null,
    error: null,
    message: null,
  },
  //purchaseChapters
  sessions: {
    data: [],
    state: "idle",
    session: null,
    error: null,
    message: null,
  },
  subjects: {
    data: [],
    state: "idle",
    subject: null,
    error: null,
    message: null,
    loading: null,
  },
  group_sessions: {
    data: [],
    state: "idle",
    subject: null,
    error: null,
    message: null,
    loading: null,
  },
};

//governments
export const getGovernments = createAsyncThunk(
  "/api/governments",
  async (query) => {
    let data;
    try {
      const response = await axios.get(`/api/governments`);
      data = await response.data;
      if (response.status === 200) {
        return data;
      }
      throw new Error(response.statusText);
    } catch (err) {
      const error = err;
      return Promise.reject(error.message ? error.message : data?.message);
    }
  }
);
//classes
export const getClasses = createAsyncThunk("/api/classes", async (query) => {
  let data;
  try {
    const response = await axios.get(`/api/classes`);
    data = await response.data;
    if (response.status === 200) {
      return data;
    }
    throw new Error(response.statusText);
  } catch (err) {
    const error = err;
    return Promise.reject(error.message ? error.message : data?.message);
  }
});

export const getExpireSoonChapters = createAsyncThunk(
  "expire_soon/chapters",
  async (query) => {
    let data;
    try {
      const response = await axios.get(`/api/chapters${query}`);
      data = await response.data;
      if (response.status === 200) {
        return data;
      }
      throw new Error(response.statusText);
    } catch (err) {
      const error = err;
      return Promise.reject(error.message ? error.message : data?.message);
    }
  }
);

//latest uploaded chapters
export const getLastChapters = createAsyncThunk(
  "latest_chapters",
  async (query) => {
    let data;
    try {
      const response = await axios.get(`/api/chapters/student/last-uploaded`);
      data = await response.data;
      if (response.status === 200) {
        return data;
      }
      throw new Error(response.statusText);
    } catch (err) {
      const error = err;
      return Promise.reject(error.message ? error.message : data?.message);
    }
  }
);

export const getSessions = createAsyncThunk("sessions", async (query) => {
  let data;
  try {
    const response = await axios.get(`/api/sessions/today`, {
      params: { start_date: new Date(query) },
    });
    data = await response.data;
    if (response.status === 200) {
      return data;
    }
    throw new Error(response.statusText);
  } catch (err) {
    const error = err;
    return Promise.reject(error.message ? error.message : data?.message);
  }
});

//purchase chapters
export const getPurchaseChapters = createAsyncThunk(
  "purchase/chapters",
  async (query) => {
    let data;
    try {
      const response = await axios.get(`api/purchase/chapter`);
      data = await response.data;
      if (response.status === 200) {
        return data;
      }
      throw new Error(response.statusText);
    } catch (err) {
      const error = err;
      return Promise.reject(error.message ? error.message : data?.message);
    }
  }
);

export const contentSlice = createSlice({
  name: "chapters",
  initialState,
  reducers: {},
  extraReducers: {
    //expire soon chapters
    [getExpireSoonChapters.pending]: (state) => {
      state.expireSoonChapters.error = null;
      state.expireSoonChapters.state = "loading";
    },
    [getExpireSoonChapters.fulfilled]: (state, action) => {
      const expireSoonChapters = action.payload.payload;
      state.expireSoonChapters.data = expireSoonChapters;
      state.expireSoonChapters.state = "success";
    },
    [getExpireSoonChapters.rejected]: (state, action) => {
      state.expireSoonChapters.error = action.error.message;
      state.expireSoonChapters.state = "error";
    },
    //leatest chapters
    [getLastChapters.pending]: (state) => {
      state.lastChapters.error = null;
      state.lastChapters.state = "loading";
    },
    [getLastChapters.fulfilled]: (state, action) => {
      const lastChapters = action.payload.payload;
      state.lastChapters.data = lastChapters;
      state.lastChapters.state = "success";
    },
    [getLastChapters.rejected]: (state, action) => {
      state.lastChapters.error = action.error.message;
      state.lastChapters.state = "error";
    },
    //sessions
    [getSessions.pending]: (state) => {
      state.sessions.error = null;
      state.sessions.state = "loading";
    },
    [getSessions.fulfilled]: (state, action) => {
      const sessions = action.payload.payload;
      state.sessions.data = sessions;
      state.sessions.state = "success";
    },
    [getSessions.rejected]: (state, action) => {
      state.sessions.error = action.error.message;
      state.sessions.state = "error";
    },

    //purchase chapter
    [getPurchaseChapters.pending]: (state) => {
      state.purchaseChapters.error = null;
      state.purchaseChapters.state = "loading";
    },
    [getPurchaseChapters.fulfilled]: (state, action) => {
      const purchaseChapters = action.payload.payload;
      state.purchaseChapters.data = purchaseChapters;
      state.purchaseChapters.state = "success";
    },
    [getPurchaseChapters.rejected]: (state, action) => {
      state.purchaseChapters.error = action.error.message;
      state.purchaseChapters.state = "error";
    },

    //governments
    [getGovernments.pending]: (state) => {
      state.governments.error = null;
      state.governments.state = "loading";
    },
    [getGovernments.fulfilled]: (state, action) => {
      const { payload } = action.payload;
      state.governments.data = payload;
      state.governments.state = "succeeded";
    },
    [getGovernments.rejected]: (state, action) => {
      state.governments.error = action.error.message;
      state.governments.state = "error";
    },
    //classes
    [getClasses.pending]: (state) => {
      state.classes.error = null;
      state.classes.state = "loading";
    },
    [getClasses.fulfilled]: (state, action) => {
      const { payload } = action.payload;
      state.classes.data = payload;
      state.classes.state = "succeeded";
    },
    [getClasses.rejected]: (state, action) => {
      state.classes.error = action.error.message;
      state.classes.state = "error";
    },
  },
});

export const {} = contentSlice.actions;

export default contentSlice.reducer;
