import { Refresh } from "@mui/icons-material";
import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "../../utils/axios";
import { serialize } from "object-to-formdata";

const initialState = {
  user: null,
  error: null,
  message: null,
  state: "idle",
  updateAvatarState: "idle",
  deleteAvatarState: "idle",
  passwordState: "idle",
};

// update me
export const updateMe = createAsyncThunk("user/update", async (query) => {
  let data;
  try {
    const response = await axios.put(
      `/api/users/update-me`,
      serialize(query, { indices: true })
    );
    data = await response.data;
    if ((response.status = 200)) {
      return data;
    }
    throw new Error(response.statusText);
  } catch (err) {
    const error = err;
    return Promise.reject(error.message ? error.message : data?.message);
  }
});
// update avatar
export const updateAvatar = createAsyncThunk(
  "user/updateAvatar",
  async (query) => {
    let data;
    try {
      const response = await axios.put(
        `/api/users/update-me`,
        serialize(query, { indices: true })
      );
      data = await response.data;
      if ((response.status = 200)) {
        return data;
      }
      throw new Error(response.statusText);
    } catch (err) {
      const error = err;
      return Promise.reject(error.message ? error.message : data?.message);
    }
  }
);
// delete avatar
export const deleteAvatar = createAsyncThunk(
  "user/deleteavatar",
  async (query) => {
    let data;
    try {
      const response = await axios.put(`/api/users/update-me`, query);
      data = await response.data;
      if ((response.status = 200)) {
        return data;
      }
      throw new Error(response.statusText);
    } catch (err) {
      const error = err;
      return Promise.reject(error.message ? error.message : data?.message);
    }
  }
);
// update me
export const updatePassword = createAsyncThunk(
  "user/updatePwd",
  async (query) => {
    let data;
    try {
      const response = await axios.post(`/auth/update-password`, query);
      data = await response.data;
      if ((response.status = 200)) {
        return data;
      }
      throw new Error(response.statusText);
    } catch (err) {
      const error = err;
      return Promise.reject(error);
    }
  }
);

export const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {},
  extraReducers: {
    //update
    [updateMe.pending]: (state) => {
      state.error = null;
      state.state = "loading";
    },
    [updateMe.fulfilled]: (state, action) => {
      const { user } = action.payload;
      state.user = user;
      state.state = "succeeded";
    },
    [updateMe.rejected]: (state, action) => {
      state.error = action.error.message;
      state.state = "error";
    },
    //pwd
    [updatePassword.pending]: (state) => {
      state.error = null;
      state.passwordState = "loading";
    },
    [updatePassword.fulfilled]: (state, action) => {
      state.passwordState = "succeeded";
    },
    [updatePassword.rejected]: (state, action) => {
      state.error = action.error.message;
      state.passwordState = "error";
    },
    //update avatar
    [updateAvatar.pending]: (state) => {
      state.error = null;
      state.updateAvatarState = "loading";
    },
    [updateAvatar.fulfilled]: (state, action) => {
      state.updateAvatarState = "succeeded";
    },
    [updateAvatar.rejected]: (state, action) => {
      state.error = action.error.message;
      state.updateAvatarState = "error";
    },

    // delete avatar
    [deleteAvatar.pending]: (state) => {
      state.error = null;
      state.deleteAvatarState = "loading";
    },
    [deleteAvatar.fulfilled]: (state, action) => {
      state.deleteAvatarState = "succeeded";
    },
    [deleteAvatar.rejected]: (state, action) => {
      state.error = action.error.message;
      state.deleteAvatarState = "error";
    },
  },
});

export default userSlice.reducer;
