import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "../../utils/axios";

const initialState = {
  isAuthenticated: false,
  isInitialised: false,
  user: null,
  error: null,
  message: null,
  confirmState: null,
  state: "idle",
  users: [],
};

export const setSession = (token) => {
  if (token) {
    localStorage.setItem("token", token);
    axios.defaults.headers.common.Authorization = `Bearer ${token}`;
  } else {
    localStorage.removeItem("token");
    delete axios.defaults.headers.common.Authorization;
  }
};
// login
export const login = createAsyncThunk("auth/students/login", async (query) => {
  const { email, password } = query;
  let data;
  try {
    const response = await axios.post(`/login`, {
      email,
      password,
    });
    data = await response.data;
    if ((response.status = 200)) {
      console.log(data);
      return data;
    }
    throw new Error(response.statusText);
  } catch (err) {
    const error = err;
    return Promise.reject(error.message ? error.message : data?.message);
  }
});

export const users = createAsyncThunk("auth/students/users", async () => {
  let data;
  try {
    console.log("before");
    const response = await axios.get(`/users`);
    console.log("after");
    console.log(data);
    data = await response.data;
    console.log(data);
    if ((response.status = 200)) {
      console.log(data);
      return data;
    }
    throw new Error(response.statusText);
  } catch (err) {
    const error = err;
    return Promise.reject(error.message ? error.message : data?.message);
  }
});
//register
export const register = createAsyncThunk("auth/register", async (query) => {
  const { values, methode } = query;
  let data;
  try {
    const response = await axios.post(`/auth/student/register/${methode}`, values);

    data = await response.data;
    if ((response.status = 200)) {
      return data;
    }
    throw new Error(response.statusText);
  } catch (err) {
    const error = err;
    console.log(error);
    return Promise.reject(error.message ? error.message : error);
  }
});
//confirm
export const confirm = createAsyncThunk("auth/confirm", async (query) => {
  const { confirmationEmailCode, methode } = query;
  let data;
  try {
    const response = await axios.post(`/auth/register/${methode}/confirm`, {
      confirmationEmailCode,
    });
    data = await response.data;
    if ((response.status = 200)) {
      return data;
    }
    throw new Error(response.statusText);
  } catch (err) {
    const error = err;
    console.log(error);
    return Promise.reject(error.message ? error.message : error);
  }
});
//forgot pwd
export const forgotPassword = createAsyncThunk("auth/forgot-pwd", async (query) => {
  let data;
  try {
    const response = await axios.post(`/auth/forgot-password`, query);
    data = await response.data;
    if ((response.status = 200)) {
      return data;
    }
    throw new Error(response.statusText);
  } catch (err) {
    const error = err;
    return Promise.reject(error.message ? error.message : data?.message);
  }
});

//reset pwd
export const resetPassword = createAsyncThunk("auth/reset-pwd", async (query) => {
  const { token, values } = query;

  let data;
  try {
    const response = await axios.post(`/auth/resetPassword/${token}`, values);
    data = await response.data;
    if ((response.status = 200)) {
      return data;
    }
    throw new Error(response.statusText);
  } catch (err) {
    console.log(err);
    const error = err;
    return Promise.reject(error);
  }
});
//get me
export const getMe = createAsyncThunk("api/me", async (query) => {
  let data;
  try {
    const response = await axios.get(`/api/profile/me`);
    data = await response.data;
    if (response.status === 200) {
      return data;
    }
    throw new Error(response.statusText);
  } catch (err) {
    const error = err;
    return Promise.reject(error.message ? error.message : data?.message);
  }
});

export const logout = () => async (dispatch) => {
  try {
    const res = await axios.post("/auth/logout");
    console.log(res);
    if (res) {
      dispatch(logoutSuccess());
      window.location = "/auth/login";
    }
  } catch (e) {
    dispatch(logoutSuccess());
    window.location = "/auth/login";
    return console.error(e.message);
  }
};

export const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    initialise: (state, action) => {
      const { isAuthenticated, user } = action.payload;
      state.isAuthenticated = isAuthenticated;
      (state.isInitialised = true), (state.user = user);
    },
    restore: (state) => {
      state.error = null;
      state.message = null;
    },
    logoutSuccess: (state) => {
      setSession(null);
      state.isAuthenticated = false;
      state.user = null;
      window.location.href = "/";
    },
  },
  extraReducers: {
    //login
    [login.pending]: (state) => {
      state.error = null;
      state.state = "loading";
    },
    [login.fulfilled]: (state, action) => {
      const { token: access_token, user } = action.payload.data;
      setSession(access_token);
      state.isAuthenticated = true;
      state.user = user;
      state.state = "succeeded";
    },
    [login.rejected]: (state, action) => {
      state.error = action.error.message;
      state.state = "error";
    },
    //register email or phone
    [register.pending]: (state) => {
      state.error = null;
      state.state = "loading";
    },
    [register.fulfilled]: (state, action) => {
      state.state = "succeeded";
    },
    [register.rejected]: (state, action) => {
      state.error = action.error.message;
      state.state = "error";
    },
    //confirmation code
    [confirm.pending]: (state) => {
      state.error = null;
      state.confirmState = "loading";
    },
    [confirm.fulfilled]: (state, action) => {
      const { access_token, refresh_token, user } = action.payload;
      setSession(access_token, refresh_token);
      state.isAuthenticated = true;
      state.user = user;
      state.confirmState = "succeeded";
    },
    [confirm.rejected]: (state, action) => {
      state.error = action.error.message;
      state.confirmState = "error";
    },
    //forgot pwd
    [forgotPassword.pending]: (state) => {
      state.error = null;
      state.state = "loading";
    },
    [forgotPassword.fulfilled]: (state, action) => {
      state.state = "succeeded";
    },
    [forgotPassword.rejected]: (state, action) => {
      state.error = action.error.message;
      state.state = "error";
    },
    //reset pwd
    [resetPassword.pending]: (state) => {
      state.error = null;
      state.state = "loading";
    },
    [resetPassword.fulfilled]: (state, action) => {
      state.state = "succeeded";
    },
    [resetPassword.rejected]: (state, action) => {
      state.error = action.error.message;
      state.state = "error";
    },
    //get me
    [getMe.pending]: (state) => {
      state.error = null;
      state.state = "loading";
    },
    [getMe.fulfilled]: (state, action) => {
      state.state = "succeeded";
      state.user = action.payload.payload;
    },
    [getMe.rejected]: (state, action) => {
      state.error = action.error.message;
      state.state = "error";
    },
    [users.pending]: (state) => {
      state.error = null;
      state.state = "loading";
    },
    [users.fulfilled]: (state, action) => {
      state.state = "succeeded";
      state.users = action.payload;
    },
    [users.rejected]: (state, action) => {
      state.error = action.error.message;
      state.state = "error";
    },
  },
});

export const { initialise, logoutSuccess, restore } = authSlice.actions;

export default authSlice.reducer;
