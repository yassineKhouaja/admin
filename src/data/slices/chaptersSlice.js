import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "../../utils/axios";

const initialState = {
  chapters: {
    data: [],
    status: "idle",
    chapter: null,
    error: null,
    message: null,
  },

  purchasedChapters: {
    data: [],
    status: "idle",
    chapter: null,
    error: null,
    message: null,
  },
  purshaseChapter: {
    data: [],
    status: "idle",
    chapter: null,
    error: null,
    message: null,
  },
  chapterWishlist: {
    data: [],
    status: "idle",
    chapter: null,
    error: null,
    message: null,
  },
};

export const getChapters = createAsyncThunk("chapters/getChapters", async (classId) => {
  let data;
  try {
    const response = await axios.get(`/api/chapters/student/class/${classId}`);
    data = await response.data;
    if (response.status === 200) {
      return data;
    }
    throw new Error(response.statusText);
  } catch (err) {
    const error = err;
    console.log(err);
    return Promise.reject(error.message ? error.message : data?.message);
  }
});

export const getPurchasedChapters = createAsyncThunk(
  "chapters/getPurchasedChapters",
  async (classId) => {
    let data;
    try {
      const response = await axios.get("/api/student/chapters/purchase");
      data = await response.data;
      if (response.status === 200) {
        return data;
      }
      throw new Error(response.statusText);
    } catch (err) {
      const error = err;
      console.log(err);
      return Promise.reject(error.message ? error.message : data?.message);
    }
  }
);

export const buyChapter = createAsyncThunk("chapters/buyChapter", async (params) => {
  let data;
  try {
    const response = await axios.post("/api/student/chapters/purchase", params);
    data = await response.data;
    if (response.status === 201) {
      return data;
    }
    throw new Error(response.statusText);
  } catch (err) {
    return Promise.reject(err);
  }
});

export const chapterWishlist = createAsyncThunk("chapters/chapterWishlist", async (params) => {
  let data;
  try {
    const response = await axios.get("/api/chapter-wishlist/by-user");
    data = await response.data;
    if (response.status === 200) {
      return data;
    }
    throw new Error(response.statusText);
  } catch (err) {
    const error = err;
    console.log(err);
    return Promise.reject(error.message ? error.message : data?.message);
  }
});

export const chapterWishlistToggle = createAsyncThunk(
  "chapters/chapterWishlistToggle",
  async (params) => {
    let data;
    try {
      const response = await axios.post("/api/chapter-wishlist/toogle", params);
      data = await response.data;
      if (response.status === 200) {
        return data;
      }
      throw new Error(response.statusText);
    } catch (err) {
      const error = err;
      console.log(err);
      return Promise.reject(error.message ? error.message : data?.message);
    }
  }
);

export const chaptersSlice = createSlice({
  name: "chapters",
  initialState,
  reducers: {},
  extraReducers: {
    // getChapters
    [getChapters.pending]: (state) => {
      state.chapters.status = "loading";
    },
    [getChapters.fulfilled]: (state, action) => {
      state.chapters.status = "succeeded";
      state.chapters.data = action.payload.Class?.[0]?.chapters;
    },
    [getChapters.rejected]: (state, action) => {
      state.chapters.status = "failed";
      state.chapters.error = action.error.message;
    },
    // getPurchasedChapters
    [getPurchasedChapters.pending]: (state) => {
      state.purchasedChapters.status = "loading";
    },
    [getPurchasedChapters.fulfilled]: (state, action) => {
      state.purchasedChapters.status = "succeeded";
      state.purchasedChapters.data = action.payload.payload;
    },
    [getPurchasedChapters.rejected]: (state, action) => {
      state.purchasedChapters.status = "failed";
      state.purchasedChapters.error = action.error.message;
    },
    // buyChapter
    [buyChapter.pending]: (state) => {
      state.purshaseChapter.status = "loading";
    },
    [buyChapter.fulfilled]: (state, action) => {
      state.purshaseChapter.status = "succeeded";
      state.purshaseChapter.data = action.payload.payload;
    },
    [buyChapter.rejected]: (state, action) => {
      state.purshaseChapter.status = "failed";
      state.purshaseChapter.error = action.error.message;
    },

    [chapterWishlist.pending]: (state) => {
      state.chapterWishlist.status = "loading";
    },
    [chapterWishlist.fulfilled]: (state, action) => {
      state.chapterWishlist.status = "succeeded";
      state.chapterWishlist.data = action.payload.payload;
    },
    [chapterWishlist.rejected]: (state, action) => {
      state.chapterWishlist.status = "failed";
      state.chapterWishlist.error = action.error.message;
    },
  },
});

// export const {} = subjectsSlice.actions;

export default chaptersSlice.reducer;
