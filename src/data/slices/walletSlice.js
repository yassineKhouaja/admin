import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "../../utils/axios";

const initialState = {
  pointsTransferHistory: {
    data: [],
    state: "idle",
    error: null,
    message: null,
    transferState: "idle",
  },
  transactions: {
    data: [],
    state: "idle",
    error: null,
    message: null,
  },
  refunds: {
    data: [],
    state: "idle",
    error: null,
    message: null,
  },
  paymentMethods: {
    data: [],
    state: "idle",
    error: null,
    message: null,
  },
};

//points tranfert history
export const getPointsTransferHistory = createAsyncThunk(
  "/api/points/history",
  async () => {
    let data;
    try {
      const response = await axios.get(`/api/points-transfer/by-user`);
      data = await response.data;
      if (response.status === 200) {
        return data;
      }
      throw new Error(response.statusText);
    } catch (err) {
      const error = err;
      return Promise.reject(error.message ? error.message : data?.message);
    }
  }
);
//transactions
export const getTransactions = createAsyncThunk(
  "/api/transactions",
  async (id) => {
    let data;
    try {
      const response = await axios.get(
        `/api/transactions?user_id=${id}&operation=add`
      );
      data = await response.data;
      if (response.status === 200) {
        return data;
      }
      throw new Error(response.statusText);
    } catch (err) {
      const error = err;
      return Promise.reject(error.message ? error.message : data?.message);
    }
  }
);

//transactions
export const getRefunds = createAsyncThunk("/api/refunds", async () => {
  let data;
  try {
    const response = await axios.get(`/api/refunds`);
    data = await response.data;
    if (response.status === 200) {
      return data;
    }
    throw new Error(response.statusText);
  } catch (err) {
    const error = err;
    return Promise.reject(error.message ? error.message : data?.message);
  }
});
//payment methods
export const getPaymentMethods = createAsyncThunk(
  "/api/payment/methods",
  async () => {
    let data;
    try {
      const response = await axios.get(`/api/payment-methods`);
      data = await response.data;
      if (response.status === 200) {
        return data;
      }
      throw new Error(response.statusText);
    } catch (err) {
      const error = err;
      return Promise.reject(error.message ? error.message : data?.message);
    }
  }
);

//transfert
export const transferPoints = createAsyncThunk(
  "/api/transfert",
  async (values) => {
    let data;
    try {
      const response = await axios.post(
        `/api/points-transfer/transfer`,
        values
      );
      data = await response.data;
      if ((response.status = 200)) {
        return data;
      }
      throw new Error(response);
    } catch (err) {
      const error = err;
      console.log(error);
      return Promise.reject(error.status ? error.status : data?.message);
    }
  }
);
//add points
export const addPoints = createAsyncThunk("/api/add/points", async (values) => {
  let data;
  try {
    console.log(values);
    const response = await axios.post(`/api/transactions/user`, values);
    data = await response.data;
    if ((response.status = 200)) {
      return data;
    }
    throw new Error(response);
  } catch (err) {
    const error = err;
    return Promise.reject(error.status ? error.status : data?.message);
  }
});
export const walletSlice = createSlice({
  name: "wallet",
  initialState,
  reducers: {},
  extraReducers: {
    //point transfer
    [getPointsTransferHistory.pending]: (state) => {
      state.pointsTransferHistory.error = null;
      state.pointsTransferHistory.state = "loading";
    },
    [getPointsTransferHistory.fulfilled]: (state, action) => {
      const { pointsTransfer } = action.payload;
      state.pointsTransferHistory.data = pointsTransfer;
      state.pointsTransferHistory.state = "succeeded";
    },
    [getPointsTransferHistory.rejected]: (state, action) => {
      state.pointsTransferHistory.error = action.error.message;
      state.pointsTransferHistory.state = "error";
    },
    //transactions
    [getTransactions.pending]: (state) => {
      state.transactions.error = null;
      state.transactions.state = "loading";
    },
    [getTransactions.fulfilled]: (state, action) => {
      const { payload } = action.payload;
      state.transactions.data = payload;
      state.transactions.state = "succeeded";
    },
    [getTransactions.rejected]: (state, action) => {
      state.transactions.error = action.error.message;
      state.transactions.state = "error";
    },

    //refunds
    [getRefunds.pending]: (state) => {
      state.refunds.error = null;
      state.refunds.state = "loading";
    },
    [getRefunds.fulfilled]: (state, action) => {
      const { payload } = action.payload;
      state.refunds.data = payload;
      state.refunds.state = "succeeded";
    },
    [getRefunds.rejected]: (state, action) => {
      state.refunds.error = action.error.message;
      state.refunds.state = "error";
    },
    //payment methods
    [getPaymentMethods.pending]: (state) => {
      state.paymentMethods.error = null;
      state.paymentMethods.state = "loading";
    },
    [getPaymentMethods.fulfilled]: (state, action) => {
      const { payload } = action.payload;
      state.paymentMethods.data = payload;
      state.paymentMethods.state = "succeeded";
    },
    [getPaymentMethods.rejected]: (state, action) => {
      state.paymentMethods.error = action.error.message;
      state.paymentMethods.state = "error";
    },
    //transferPoint
    [transferPoints.pending]: (state) => {
      state.pointsTransferHistory.error = null;
      state.pointsTransferHistory.transferState = "loading";
    },
    [transferPoints.fulfilled]: (state, action) => {
      // const { pointsTransfer } = action.payload;
      // state.pointsTransferHistory.data.push(pointsTransfer);
      state.pointsTransferHistory.transferState = "succeeded";
    },
    [transferPoints.rejected]: (state, action) => {
      state.pointsTransferHistory.error = action.error.message;
      state.pointsTransferHistory.transferState = "error";
    },
    //add points
    [addPoints.pending]: (state) => {
      state.pointsTransferHistory.error = null;
      state.pointsTransferHistory.transferState = "loading";
    },
    [addPoints.fulfilled]: (state, action) => {
      state.pointsTransferHistory.transferState = "succeeded";
    },
    [addPoints.rejected]: (state, action) => {
      state.pointsTransferHistory.error = action.error.message;
      state.pointsTransferHistory.transferState = "error";
    },
  },
});

export default walletSlice.reducer;
