import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  modals: [
    {
      id: "modal-example",
      open: false,
    },
    {
      id: "modal-purshase",
      open: false,
      data: null,
    },
    {
      id: "modal-purshase-chapter",
      open: false,
      data: null,
    },
    {
      id: "modal-purshase-offer",
      open: false,
      data: null,
    },
    {
      id: "modal-refund",
      open: false,
      data: null,
    },
    {
      id: "modal-messge",
      open: false,
      data: null,
    },
    {
      id: "modal-payment",
      open: false,
      data: null,
    },
    {
      id: "modal-recharge",
      open: false,
      data: null,
    },
    {
      id: "modal-points",
      open: false,
      data: null,
    },
    {
      id: "event-modal",
      open: false,
      data: null,
    },
    {
      id: "verif-modal",
      open: false,
      data: null,
    },
    {
      id: "pay-modal",
      open: false,
      data: null,
    },
    {
      id: "join-modal",
      open: false,
      data: null,
    },
  ],
};

const slice = createSlice({
  name: "modals",
  initialState,
  reducers: {
    openModal: (state, action) => {
      const { id, data } = action.payload;
      const index = state.modals.findIndex((modal) => modal.id === id);
      state.modals[index].open = true;
      state.modals[index].data = data;
    },
    closeModal: (state, action) => {
      const id = action.payload;
      const index = state.modals.findIndex((modal) => modal.id === id);
      state.modals[index].open = false;
    },
  },
});

export const reducer = slice.reducer;

export const openModal = (id, data) => (dispatch) => {
  dispatch(slice.actions.openModal({ id, data }));
};

export const closeModal = (id) => (dispatch) => {
  dispatch(slice.actions.closeModal(id));
};

export default slice;
