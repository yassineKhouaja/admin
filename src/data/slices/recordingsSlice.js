import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "../../utils/axios";

const initialState = {
  recordings: {
    data: [],
    status: "idle",
    chapter: null,
    error: null,
    message: null,
  },
};

export const getRecordings = createAsyncThunk("recordings/getSubjects", async (id) => {
  let data;
  try {
    const response = await axios.get(`/api/chapters/recordings/${id}`);
    data = await response.data;
    if (response.status === 200) {
      return data;
    }
    throw new Error(response.statusText);
  } catch (err) {
    const error = err;
    console.log(err);
    return Promise.reject(error.message ? error.message : data?.message);
  }
});

export const getPurshaseRecordingsrice = createAsyncThunk(
  "subjects/getPurshaseSubjectPrice",
  async () => {
    let data;
    try {
      const response = await axios.get("/api/subject-purchase");
      data = await response.data;
      if (response.status === 200) {
        return data;
      }
      throw new Error(response.statusText);
    } catch (err) {
      const error = err;
      console.log(err);
      return Promise.reject(error.message ? error.message : data?.message);
    }
  }
);
export const buyRecording = createAsyncThunk("subjects/buySubject", async (params) => {
  let data;
  try {
    const response = await axios.post("/api/subject-purchase/buy", params);
    data = await response.data;
    if (response.status === 201) {
      return data;
    }
    throw new Error(response.statusText);
  } catch (err) {
    const error = err;
    console.log(err);
    return Promise.reject(error.message ? error.message : data?.message);
  }
});

export const recordingsWishlist = createAsyncThunk("subjects/subjectWishlist", async (params) => {
  let data;
  try {
    const response = await axios.get("/api/subject-wishlist/by-user");
    data = await response.data;
    if (response.status === 200) {
      return data;
    }
    throw new Error(response.statusText);
  } catch (err) {
    const error = err;
    console.log(err);
    return Promise.reject(error.message ? error.message : data?.message);
  }
});

export const recordingsWishlistToggle = createAsyncThunk(
  "subjects/subjectWishlistToggle",
  async (params) => {
    let data;
    try {
      const response = await axios.post("/api/subject-wishlist/toogle", params);
      data = await response.data;
      if (response.status === 200) {
        return data;
      }
      throw new Error(response.statusText);
    } catch (err) {
      const error = err;
      console.log(err);
      return Promise.reject(error.message ? error.message : data?.message);
    }
  }
);

export const recordingsSlice = createSlice({
  name: "recordings",
  initialState,
  reducers: {
    currentSubject: (state, action) => {
      state.currentSubject.data = action.payload;
    },
  },
  extraReducers: {
    // getSubjects
    [getRecordings.pending]: (state) => {
      state.recordings.status = "loading";
    },
    [getRecordings.fulfilled]: (state, action) => {
      state.recordings.status = "succeeded";
      state.recordings.data = action.payload.payload;
    },
    [getRecordings.rejected]: (state, action) => {
      state.recordings.status = "failed";
      state.recordings.error = action.error.message;
    },

    // // getPurshaseSubjectPrice
    // [getPurshaseSubjectPrice.pending]: (state) => {
    //   state.purshaseSubject.status = "loading";
    // },
    // [getPurshaseSubjectPrice.fulfilled]: (state, action) => {
    //   state.purshaseSubject.status = "succeeded";
    //   state.purshaseSubject.data = action.payload.payload;
    // },
    // [getPurshaseSubjectPrice.rejected]: (state, action) => {
    //   state.purshaseSubject.status = "failed";
    //   state.purshaseSubject.error = action.error.message;
    // },
    // // getPurshaseSubjectPrice
    // [buySubject.pending]: (state) => {
    //   state.purshaseSubject.status = "loading";
    // },
    // [buySubject.fulfilled]: (state, action) => {
    //   state.purshaseSubject.status = "succeeded";
    //   state.purshaseSubject.data = action.payload.payload;
    // },
    // [buySubject.rejected]: (state, action) => {
    //   state.purshaseSubject.status = "failed";
    //   state.purshaseSubject.error = action.error.message;
    // },

    // [subjectWishlist.pending]: (state) => {
    //   state.subjectWishlist.status = "loading";
    // },
    // [subjectWishlist.fulfilled]: (state, action) => {
    //   state.subjectWishlist.status = "succeeded";
    //   state.subjectWishlist.data = action.payload.payload;
    // },
    // [subjectWishlist.rejected]: (state, action) => {
    //   state.subjectWishlist.status = "failed";
    //   state.subjectWishlist.error = action.error.message;
    // },
  },
});

export const {} = recordingsSlice.actions;

export default recordingsSlice.reducer;
