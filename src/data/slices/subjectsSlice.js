import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "../../utils/axios";

const initialState = {
  subjects: {
    data: [],
    status: "idle",
    chapter: null,
    error: null,
    message: null,
  },
  periods: {
    data: [],
    status: "idle",
    chapter: null,
    error: null,
    message: null,
  },

  purshaseSubject: {
    data: [],
    status: "idle",
    chapter: null,
    error: null,
    message: null,
  },
  currentSubject: {
    data: null,
    status: "idle",
    chapter: null,
    error: null,
    message: null,
  },
  subjectWishlist: {
    data: [],
    status: "idle",
    chapter: null,
    error: null,
    message: null,
  },
};

export const getSubjects = createAsyncThunk("subjects/getSubjects", async (id) => {
  let data;
  try {
    const response = await axios.get(`/api/subjects?class_id=${id}`);
    data = await response.data;
    if (response.status === 200) {
      return data;
    }
    throw new Error(response.statusText);
  } catch (err) {
    const error = err;
    console.log(err);
    return Promise.reject(error.message ? error.message : data?.message);
  }
});
export const getPeriods = createAsyncThunk("subjects/getPeriods", async () => {
  let data;
  try {
    const response = await axios.get("/api/reduction-periods");
    data = await response.data;
    if (response.status === 200) {
      return data;
    }
    throw new Error(response.statusText);
  } catch (err) {
    const error = err;
    console.log(err);
    return Promise.reject(error.message ? error.message : data?.message);
  }
});

export const getPurshaseSubjectPrice = createAsyncThunk(
  "subjects/getPurshaseSubjectPrice",
  async () => {
    let data;
    try {
      const response = await axios.get("/api/subject-purchase");
      data = await response.data;
      if (response.status === 200) {
        return data;
      }
      throw new Error(response.statusText);
    } catch (err) {
      const error = err;
      console.log(err);
      return Promise.reject(error.message ? error.message : data?.message);
    }
  }
);
export const buySubject = createAsyncThunk("subjects/buySubject", async (params) => {
  let data;
  try {
    const response = await axios.post("/api/subject-purchase/buy", params);
    data = await response.data;
    if (response.status === 201) {
      return data;
    }
    throw new Error(response.statusText);
  } catch (err) {
    const error = err;
    console.log(err);
    return Promise.reject(error.message ? error.message : data?.message);
  }
});

export const subjectWishlist = createAsyncThunk("subjects/subjectWishlist", async (params) => {
  let data;
  try {
    const response = await axios.get("/api/subject-wishlist/by-user");
    data = await response.data;
    if (response.status === 200) {
      return data;
    }
    throw new Error(response.statusText);
  } catch (err) {
    const error = err;
    console.log(err);
    return Promise.reject(error.message ? error.message : data?.message);
  }
});

export const subjectWishlistToggle = createAsyncThunk(
  "subjects/subjectWishlistToggle",
  async (params) => {
    let data;
    try {
      const response = await axios.post("/api/subject-wishlist/toogle", params);
      data = await response.data;
      if (response.status === 200) {
        return data;
      }
      throw new Error(response.statusText);
    } catch (err) {
      const error = err;
      console.log(err);
      return Promise.reject(error.message ? error.message : data?.message);
    }
  }
);

export const subjectsSlice = createSlice({
  name: "subjects",
  initialState,
  reducers: {
    currentSubject: (state, action) => {
      state.currentSubject.data = action.payload;
    },
  },
  extraReducers: {
    // getSubjects
    [getSubjects.pending]: (state) => {
      state.subjects.status = "loading";
    },
    [getSubjects.fulfilled]: (state, action) => {
      state.subjects.status = "succeeded";
      state.subjects.data = action.payload.payload;
    },
    [getSubjects.rejected]: (state, action) => {
      state.subjects.status = "failed";
      state.subjects.error = action.error.message;
    },
    // getPeriods
    [getPeriods.pending]: (state) => {
      state.periods.status = "loading";
    },
    [getPeriods.fulfilled]: (state, action) => {
      state.periods.status = "succeeded";
      state.periods.data = action.payload.payload;
    },
    [getPeriods.rejected]: (state, action) => {
      state.periods.status = "failed";
      state.periods.error = action.error.message;
    },
    // getPurshaseSubjectPrice
    [getPurshaseSubjectPrice.pending]: (state) => {
      state.purshaseSubject.status = "loading";
    },
    [getPurshaseSubjectPrice.fulfilled]: (state, action) => {
      state.purshaseSubject.status = "succeeded";
      state.purshaseSubject.data = action.payload.payload;
    },
    [getPurshaseSubjectPrice.rejected]: (state, action) => {
      state.purshaseSubject.status = "failed";
      state.purshaseSubject.error = action.error.message;
    },
    // getPurshaseSubjectPrice
    [buySubject.pending]: (state) => {
      state.purshaseSubject.status = "loading";
    },
    [buySubject.fulfilled]: (state, action) => {
      state.purshaseSubject.status = "succeeded";
      state.purshaseSubject.data = action.payload.payload;
    },
    [buySubject.rejected]: (state, action) => {
      state.purshaseSubject.status = "failed";
      state.purshaseSubject.error = action.error.message;
    },

    [subjectWishlist.pending]: (state) => {
      state.subjectWishlist.status = "loading";
    },
    [subjectWishlist.fulfilled]: (state, action) => {
      state.subjectWishlist.status = "succeeded";
      state.subjectWishlist.data = action.payload.payload;
    },
    [subjectWishlist.rejected]: (state, action) => {
      state.subjectWishlist.status = "failed";
      state.subjectWishlist.error = action.error.message;
    },
  },
});

export const { currentSubject } = subjectsSlice.actions;

export default subjectsSlice.reducer;
