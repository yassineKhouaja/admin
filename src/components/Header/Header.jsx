import Searchbar from "../Searchbar";
import Notifications from "../Notifications";
import PointsWidget from "../PointsWidget";
import MenuHeader from "../Menu";
import ViewTitle from "../ViewTitle";
import HamburgerMenuTrigger from "../HamburgerMenuTrigger";
import { ReactComponent as Logo } from "../../assets/images/logo_lockup.svg";
import useWindowSize from "../../hooks/useWindowSize";

const Header = () => {
  const { width } = useWindowSize();

  return (
    <header className="header">
      {width >= 1090 && (
        <div className="header__group">
          <ViewTitle />
        </div>
      )}
      <div className="header__group">
        <HamburgerMenuTrigger />
      </div>
    </header>
  );
};

export default Header;
