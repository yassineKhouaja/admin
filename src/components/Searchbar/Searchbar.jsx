import { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { ReactComponent as SearchIcon } from "../../assets/icons/search.svg";
import useWindowSize from "../../hooks/useWindowSize";
import { search } from "../../data/slices/search";
import folderSvg from "../../assets/icons/folder11.svg";
import doc from "../../assets/icons/doc.svg";
import file from "../../assets/icons/file-text_clean.svg";
import play from "../../assets/icons/Play_circle.svg";
import { CircularProgress, Box } from "@mui/material";
import { useNavigate } from "react-router-dom";
import { currentSubject, getSubjects } from "../../data/slices/subjectsSlice";
import useIsMountedRef from "../../hooks/useIsMounted";
const Searchbar = () => {
  return <></>;
};
// const Searchbar = () => {
//   const isMounted = useIsMountedRef();
//   const dispatch = useDispatch();
//   const navigate = useNavigate();
//   const { data, state } = useSelector((state) => state.search);
//   const { classes } = useSelector((state) => state.auth.user);
//   const { data: subjectsData } = useSelector((state) => state.subjects.subjects);
//   const [value, setValue] = useState("");
//   const { width } = useWindowSize();
//   if (width < 1090) return null;

//   const handleSearch = (value) => {
//     setValue(value);
//     if (value.trim() !== "" && value?.length > 2) {
//       dispatch(search(value));
//     }
//   };

//   const checkEmptyData = (data) => {
//     let isEmpty = true;
//     if (data) {
//       for (let i = 0; i < data.length; i++) {
//         if (data[i].data.length !== 0) {
//           isEmpty = false;
//         }
//       }
//     }
//     return isEmpty;
//   };

//   const affectIcon = (name) => {
//     switch (name) {
//       case "Magazines":
//         return doc;
//       case "Videos":
//         return play;
//       case "Chapters":
//         return file;
//       default:
//         return folderSvg;
//     }
//   };

//   useEffect(() => {
//     if (!isMounted.current) return;
//     if (subjectsData?.length <= 0) {
//       dispatch(getSubjects(classes?.[0]?.id));
//     }
//   }, [dispatch]);

//   const handleNavigate = (item, index) => {
//     dispatch(
//       currentSubject(subjectsData?.filter((subject) => subject?.id === item?.data[index]?.id)?.[0])
//     );
//     switch (item?.name) {
//       case "Subjects":
//         setValue("");
//         return navigate(`/subjects/${item?.data[index]?.id}`);
//       case "Chapters":
//         setValue("");
//         return navigate(`/subjects/${item?.data[index]?.subjects[0]?.id}/${item?.data[index]?.id}`);
//       default:
//         setValue("");
//         return navigate(
//           `/subjects/${item?.data[index]?.sections?.chapter?.subjects[0]?.id}/${item?.data[index]?.sections?.chapter?.id}`
//         );
//     }
//   };

//   return (
//     <div className="section_search">
//       <div className={value.trim() !== "" && value.length > 2 ? "search_bar search" : "search_bar"}>
//         {state === "loading" ? <CircularProgress size={20} /> : <SearchIcon />}
//         <form autoComplete="off" style={{ width: "100%" }}>
//           <input
//             type="text"
//             className="input_search"
//             placeholder="Search for a subject"
//             value={value}
//             onChange={(e) => handleSearch(e.target.value)}
//           />
//         </form>
//       </div>
//       {value.trim() !== "" && value.length > 2 && (
//         <div className="search_content">
//           {checkEmptyData(data) === true && state === "succeeded" && (
//             <div className="no_data">No Subjects</div>
//           )}
//           {state === "loading" && <div className="no_data">loading . . .</div>}
//           {data?.map((item, index) => (
//             <div className="content_items" key={index}>
//               {item?.data?.length !== 0 && (
//                 <>
//                   <div className="item_title">{item?.name}</div>
//                   <div className="scroll_items">
//                     {item?.data?.map((el, index1) => (
//                       <div
//                         className="lists"
//                         key={index1}
//                         onClick={() => handleNavigate(item, index1)}
//                       >
//                         <img src={affectIcon(item?.name)} alt="" />
//                         <div className="item">{el?.name}</div>
//                       </div>
//                     ))}
//                   </div>
//                 </>
//               )}
//             </div>
//           ))}
//         </div>
//       )}
//     </div>
//   );
// };

export default Searchbar;
