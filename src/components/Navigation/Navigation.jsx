import { NavLink } from "react-router-dom";
import { ReactComponent as HomeIcon } from "../../assets/icons/home.svg";
import { ReactComponent as CalendarIcon } from "../../assets/icons/calendar.svg";
import { ReactComponent as FolderIcon } from "../../assets/icons/folder.svg";
import { ReactComponent as BagIcon } from "../../assets/icons/bag.svg";
import { ReactComponent as StarIcon } from "../../assets/icons/star.svg";
import { ReactComponent as WalletIcon } from "../../assets/icons/wallet.svg";
import { ReactComponent as ProfileIcon } from "../../assets/icons/profile.svg";
import { useDispatch, useSelector } from "react-redux";
import { closeSidebar } from "../../data/slices/settingsSlice";

const NavigationLink = ({ icon = null, route = "/", children }) => {
  const dispatch = useDispatch();
  return (
    <NavLink to={route} className="navigation_link" onClick={() => dispatch(closeSidebar())}>
      {icon}
      <span className="navigation_link__label">{children}</span>
    </NavLink>
  );
};

const Navigation = () => {
  return (
    <div className="navigation__container">
      <NavigationLink route="/" icon={<HomeIcon />}>
        Admins
      </NavigationLink>
      <NavigationLink route="/articles" icon={<CalendarIcon />}>
        Articles
      </NavigationLink>
      <NavigationLink route="/about" icon={<FolderIcon />}>
        About HIV
      </NavigationLink>
      <NavigationLink route="/prevention" icon={<BagIcon />}>
        Prevention
      </NavigationLink>
      <NavigationLink route="/testing" icon={<BagIcon />}>
        Testing
      </NavigationLink>

      <NavigationLink route="/transmission" icon={<WalletIcon />}>
        Transmission
      </NavigationLink>
      <NavigationLink route="/living" icon={<ProfileIcon />}>
        Living with HIV
      </NavigationLink>
    </div>
  );
};

export default Navigation;
