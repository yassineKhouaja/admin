import { useLocation } from "react-router-dom";
import useWindowSize from "../../hooks/useWindowSize";

const titles = {
  "/": "Admins ",
  "/articles": "Articles",
  "/about": "About HIV",
  "/prevention": "Prevention HIV",
  "/testing": "Testing HIV",
  "/transmission": "Transmission HIV",
  "/living": "Living with HIV",
};

const ViewTitle = () => {
  const { width } = useWindowSize();
  let location = useLocation();

  // console.log(location.pathname);

  if (width < 1090) return null;

  return (
    <h1 className="view_title">
      {titles[location.pathname] || titles["/" + location.pathname.split("/")[1]] || "Oops..."}
    </h1>
  );
};

export default ViewTitle;
