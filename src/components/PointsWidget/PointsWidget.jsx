import { useSelector } from "react-redux";
import { ReactComponent as PointsWidgetIcon } from "../../assets/icons/points-widget.svg";
import useWindowSize from "../../hooks/useWindowSize";

const PointsWidget = ({ overrideHiding }) => {
  const { width } = useWindowSize();
  const points = useSelector((state) => state.auth.user?.points);

  if (width < 1090 && overrideHiding == null) return null;
  return (
    <div className="points_widget">
      <PointsWidgetIcon />
      <div className="group">
        <span className="label">Votre solde</span>
        <span className="points">{points || 0} Pts</span>
      </div>
    </div>
  );
};

export default PointsWidget;
