import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { closeModal } from "../../data/slices/modals";
import ModalPurchase from "../../features/contents/components/ModalPurchase";
import ModalRefund from "../../features/contents/components/ModalRefund";
import PointModal from "../../features/wallet/components/PointModal/PointModal";
import RechargeModal from "../../features/wallet/components/RechargeModal/RechargeModal";
import SessionDetailsModal from "../../features/liveSession/components/SessionDetailsModal";

import { ModalExample } from "../Modals";
import ModalPayment from "../../features/contents/components/ModalPayment";
import ModalPurchaseChapter from "../../features/contents/components/ModalPurchaseChapter";
import VerificationModal from "../../features/authentification/Register/components/verificationModal";
import PaymentModal from "../../features/liveSession/components/PaymentModal/PaymentModal";
import JoinSessionModal from "../../features/liveSession/components/JoinSessionModal";
import ModalMessage from "../../features/contents/components/ModalMessage/ModalMessage";
import ModalPurchaseOffer from "../../features/offers/components/ModalPurchaseOffer";

const ModalsProvider = (props) => {
  const { modals } = useSelector((state) => state.modals);
  const dispatch = useDispatch();
  const modalState = (id, key) => {
    const res = modals.find((modal) => modal.id === id);
    return res[key];
  };
  const handleClose = (id) => {
    dispatch(closeModal(id));
  };
  return (
    <>
      <ModalExample
        id="modal-example"
        open={modalState("modal-example", "open")}
        data={modalState("modal-example", "data")}
        handleClose={handleClose}
      />
      <ModalPurchase
        id="modal-purshase"
        open={modalState("modal-purshase", "open")}
        data={modalState("modal-purshase", "data")}
        handleClose={handleClose}
      />
      <ModalPurchaseChapter
        id="modal-purshase-chapter"
        open={modalState("modal-purshase-chapter", "open")}
        data={modalState("modal-purshase-chapter", "data")}
        handleClose={handleClose}
      />
      <ModalPurchaseOffer
        id="modal-purshase-offer"
        open={modalState("modal-purshase-offer", "open")}
        data={modalState("modal-purshase-offer", "data")}
        handleClose={handleClose}
      />

      <ModalPayment
        id="modal-payment"
        open={modalState("modal-payment", "open")}
        data={modalState("modal-payment", "data")}
        handleClose={handleClose}
      />
      <ModalRefund
        id="modal-refund"
        open={modalState("modal-refund", "open")}
        data={modalState("modal-refund", "data")}
        handleClose={handleClose}
      />
      <ModalMessage
        id="modal-messge"
        open={modalState("modal-messge", "open")}
        data={modalState("modal-messge", "data")}
        handleClose={handleClose}
      />
      <RechargeModal
        id="modal-recharge"
        open={modalState("modal-recharge", "open")}
        data={modalState("modal-recharge", "data")}
        handleClose={handleClose}
      />
      <PointModal
        id="modal-points"
        open={modalState("modal-points", "open")}
        data={modalState("modal-points", "data")}
        handleClose={handleClose}
      />
      <SessionDetailsModal
        id="event-modal"
        open={modalState("event-modal", "open")}
        data={modalState("event-modal", "data")}
        handleClose={handleClose}
      />
      <VerificationModal
        id="verif-modal"
        open={modalState("verif-modal", "open")}
        data={modalState("verif-modal", "data")}
        handleClose={handleClose}
      />
      <PaymentModal
        id="pay-modal"
        open={modalState("pay-modal", "open")}
        data={modalState("pay-modal", "data")}
        handleClose={handleClose}
      />
      <JoinSessionModal
        id="join-modal"
        open={modalState("join-modal", "open")}
        data={modalState("join-modal", "data")}
        handleClose={handleClose}
      />
    </>
  );
};

export default ModalsProvider;
