import { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { initialise, setSession } from "../data/slices/authSlice";
import jwtDecode from "jwt-decode";
import axios from "../utils/axios";
import useIsMounted from "../hooks/useIsMounted";

const AuthProvider = ({ children }) => {
  const isMounted = useIsMounted();

  const { isInitialised } = useSelector((state) => state.auth);
  const dispatch = useDispatch();

  const isValidToken = (token) => {
    if (!token) {
      return false;
    }

    const decoded = jwtDecode(token);
    // const currentTime = Date.now() / 1000;

    return true;
    // return decoded.exp > currentTime;
  };

  useEffect(() => {
    if (!isMounted.current) return;

    (async () => {
      try {
        const token = window.localStorage.getItem("token");
        if (token && isValidToken(token)) {
          setSession(token);

          dispatch(initialise({ isAuthenticated: true }));
        } else {
          dispatch(initialise({ isAuthenticated: false }));
          setSession(null);
        }
      } catch (err) {
        console.error(err);
        dispatch(initialise({ isAuthenticated: false }));
      }
    })();
  }, []);

  if (!isInitialised) {
    return <>SplashScreen</>;
  }

  return <>{children}</>;
};

export default AuthProvider;
