import { ReactComponent as LogoutIcon } from "../../assets/icons/logout.svg";
import { logout } from "../../data/slices/authSlice";
import useWindowSize from "../../hooks/useWindowSize";
import { useDispatch } from "react-redux";

const LogoutButton = () => {
  const dispatch = useDispatch();

  const { width } = useWindowSize();

  if (width >= 1090) return null;

  return (
    <div onClick={() => dispatch(logout())} className="logout_button">
      <LogoutIcon />
      <span className="logout_button__label">Log Out</span>
    </div>
  );
};

export default LogoutButton;
