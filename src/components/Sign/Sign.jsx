import { ReactComponent as LogoIcon } from "../../assets/images/Icon Ostedhy.svg";
import useWindowSize from "../../hooks/useWindowSize";

const Sign = () => {
  const { width } = useWindowSize();

  if (width < 1090) return null;

  return (
    <div className="sign">
      <div className="sign__icon_container">
        <LogoIcon />
      </div>
      <span className="sign__title">Offer Prepa Pack</span>
      <span className="sign__desc">
        Your chance to subscribe and get 20% discount
      </span>
      <div className="sign__bouton">Subscribe Now</div>
    </div>
  );
};

export default Sign;
