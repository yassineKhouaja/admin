import { ReactComponent as NotificationsIcon } from "../../assets/icons/notifications.svg";

const Notifications = () => {
  return (
    <div className='notifications'>
      <div className='notifications__bouton'>
        <NotificationsIcon />
      </div>
    </div>
  );
};

export default Notifications;
