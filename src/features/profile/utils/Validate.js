const validateObj = (values, user) => {
  console.log(user);
  let newValue = {
    first_name: user?.first_name,
    last_name: user?.last_name,
    classes: [user?.classes[0].id],
    postal_code: user?.postal_code,
    birth_date: user?.birth_date,
    government_id: user?.government_id,
  };
  let newValue1 = {
    first_name: values?.first_name,
    last_name: values?.last_name,
    classes: values?.classes,
    postal_code: values?.postal_code,
    birth_date: values?.birth_date, //.concat("T00:00:00.000Z"),
    government_id: values?.government_id,
  };
  console.log("v1", newValue);
  console.log("v2", newValue1);

  if (JSON.stringify(newValue) === JSON.stringify(newValue1)) {
    return true;
  } else return false;
};

export default validateObj;
