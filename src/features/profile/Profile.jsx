import React, { useState } from "react";
import { Grid } from "@mui/material";
import ProfileDetails from "./components/ProfileDetails";
import ProfileAvatar from "./components/ProfileAvatar";
import NewPassword from "./components/NewPassword";

export default function Profile() {
  return (
    <div className="section_profile">
      <Grid container spacing={2}>
        <Grid item xs={12} sm={12} md={7} lg={8} className="section_left">
          <ProfileDetails />
        </Grid>
        <Grid item xs={12} sm={12} md={5} lg={4} className="section_right">
          <ProfileAvatar />
          <NewPassword />
        </Grid>
      </Grid>
    </div>
  );
}
