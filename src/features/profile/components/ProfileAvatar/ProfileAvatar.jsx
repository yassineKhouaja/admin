import React, { useEffect, useState } from "react";
import { Avatar, Grid } from "@mui/material";
import avatarImg from "../../assets/images/avatar.svg";
import { useDispatch, useSelector } from "react-redux";
import { updateAvatar, deleteAvatar } from "../../../../data/slices/userSlice";
import { getMe } from "../../../../data/slices/authSlice";

let MAX_PROFILE_IMG_SIZE = 2097152; // 2MB

export default function ProfileAvatar() {
  const { user } = useSelector((state) => state.auth);
  const { updateAvatarState } = useSelector((state) => state.user);
  const { deleteAvatarState } = useSelector((state) => state.user);
  const [profileUploadIsLoading, setProfileUploadIsLoading] = useState(false);
  const imageRef = React.useRef();
  const dispatch = useDispatch();

  const onFilesChange = (e) => {
    const img = e.target.files[0];
    if (img?.size > MAX_PROFILE_IMG_SIZE) {
      alert("image size must be less than 2Mo");
      return;
    }
    imageRef.current.value = null;
    dispatch(updateAvatar({ avatar: img }))
      .then((res) => {
        if (res.error) {
          alert(res.error.message);
          return;
        }
        dispatch(getMe());
      })
      .catch((err) => {});
  };

  const handleDeleteAvatar = () => {
    dispatch(deleteAvatar({ photo_url: null }))
      .then((res) => {
        if (res.error) {
          alert(res.error.message);
          return;
        }
        dispatch(getMe());
      })
      .catch((err) => {});
  };

  return (
    <div className="section_avatar">
      <Grid container alignItems="center" justifyContent="space-between">
        <Grid
          item
          xs={12}
          sm={12}
          md={9}
          className="left__section"
          paddingRight="10px"
        >
          <Avatar
            src={user?.photo_url || avatarImg}
            sx={{ width: 74, height: 74 }}
          />

          <div className="section_text">
            <p>Edit your Photo</p>
            <span>The size must be less than 2Mo</span>
          </div>
        </Grid>
        <Grid item xs={12} sm={12} md={3} className="right__section">
          <input
            type="file"
            name="avatar"
            id="imageInputLabel"
            className="upload__btn"
            hidden
            ref={imageRef}
            onChange={onFilesChange}
            accept="image/*"
          />
          <label
            className="image__input__label"
            data-tooltip="Upload"
            htmlFor="imageInputLabel"
          >
            <span className="uploadImgLabel">
              {updateAvatarState === "loading" ? "loading..." : "Upload"}
            </span>
          </label>
          <button
            className="delete__btn"
            onClick={() => {
              handleDeleteAvatar();
            }}
          >
            {deleteAvatarState === "loading" ? "loading ..." : "delete"}
          </button>
        </Grid>
      </Grid>
    </div>
  );
}
