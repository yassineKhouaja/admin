import React, { useEffect, useState } from "react";
import { useFormik } from "formik";
import {
  Grid,
  Autocomplete,
  TextField,
  Tooltip,
  Box,
  CircularProgress,
} from "@mui/material";
import * as Yup from "yup";
import { initialise, setSession } from "../../../../data/slices/authSlice";
import Btn from "../../../../components/Button/Button";
import { ReactComponent as CopyIcon } from "../../assets/icons/copy.svg";
import { updateMe } from "../../../../data/slices/userSlice";
import { useDispatch, useSelector } from "react-redux";
import useIsMounted from "../../../../hooks/useIsMounted";
import {
  getGovernments,
  getClasses,
} from "../../../../data/slices/contentSlice";
import validateObj from "../../utils/Validate";

const phoneRegExp = /2[0-9]{7}|4[0-9]{7}|5[0-9]{7}|9[0-9]{7}$/g;

export default function ProfileDetails() {
  const dispatch = useDispatch();
  const [tooltipText, setTooltipText] = useState("");
  const [errMsg, setErrMsg] = useState("");
  const isMounted = useIsMounted();

  const { governments, classes } = useSelector((state) => state.content);
  const { user } = useSelector((state) => state.auth);
  const { state } = useSelector((state) => state.user);
  console.log(user);
  useEffect(() => {
    if (!isMounted.current) return;
    if (governments.data?.length === 0) {
      dispatch(getGovernments());
    }
    if (classes.data?.length === 0) {
      dispatch(getClasses());
    }
  }, [dispatch]);

  const formik = useFormik({
    initialValues: {
      first_name: user?.first_name,
      last_name: user?.last_name,
      email: user?.email,
      phone: user?.phone,
      birth_date: user?.birth_date.slice(0, 10),
      classes: user?.classes ? [user?.classes[0]?.id] : 0,
      government_id: user?.government_id,
      postal_code: user?.postal_code,
    },
    validationSchema: Yup.object().shape({
      first_name: Yup.string().required("first name is required"),
      last_name: Yup.string().required("last name is required"),
      government_id: Yup.number().required("government is required"),
      birth_date: Yup.string().required("classes is required"),
      postal_code: Yup.string().min(4).required("postal code is required"),
    }),
    onSubmit: (values) => {
      const isChangedField = validateObj(values, user);
      if (!isChangedField) {
        dispatch(updateMe(values)).then((res) => {
          if (res.error) {
            setErrMsg("Failed");
          } else if (res.payload) {
            setErrMsg("Profile Updated successfully");
            dispatch(
              initialise({
                isAuthenticated: true,
                user: res.payload.payload.user,
              })
            );
          }
        });
      }
    },
  });

  const handleCopyStudentId = () => {
    const student_id = user.id;
    navigator.clipboard.writeText(student_id).catch((err) => {
      alert("Error in copying id");
    });
    setTooltipText("Copied");
  };

  const handleGov = (value) => {
    if (value) {
      formik.setFieldValue("government_id", value.id);
    } else {
      formik.setFieldValue("government_id", "");
    }
  };
  const handleDivision = (value) => {
    if (value) {
      formik.setFieldValue("classes", [value.id]);
    } else {
      formik.setFieldValue("classes", "");
    }
  };
  return (
    <Grid
      container
      spacing={3}
      className="profile_details"
      component="form"
      onSubmit={formik.handleSubmit}
    >
      <Grid item xs={12} sm={6}>
        <label htmlFor="name1">First Name</label>
        <input
          className={
            formik.touched.first_name &&
            formik.errors.first_name &&
            "error_field"
          }
          placeholder="Enter your First name"
          id="firstname"
          name="first_name"
          type="text"
          onChange={formik.handleChange}
          value={formik.values.first_name}
          onBlur={formik.handleBlur}
        />
      </Grid>
      <Grid item xs={12} sm={6}>
        <label htmlFor="name">Last Name</label>
        <input
          className={
            formik.touched.last_name && formik.errors.last_name && "error_field"
          }
          placeholder="Enter your Last name"
          id="lastname"
          name="last_name"
          type="text"
          onChange={formik.handleChange}
          value={formik.values.last_name}
          onBlur={formik.handleBlur}
        />
      </Grid>

      <Grid item xs={12} sm={6}>
        <label htmlFor="email" className="disabled_item">
          Email Adress
        </label>
        <input
          disabled
          className={
            formik.touched.email && formik.errors.email && "error_field"
          }
          placeholder="Enter your email adress"
          id="Email"
          name="email"
          type="email"
          onChange={formik.handleChange}
          value={formik.values.email}
          onBlur={formik.handleBlur}
        />
      </Grid>

      <Grid item xs={12} sm={6}>
        <label htmlFor="phone number" className="disabled_item">
          Phone Number
        </label>
        <input
          disabled
          className={
            formik.touched.phone && formik.errors.phone && "error_field"
          }
          placeholder="Enter your Phone number"
          id="phone"
          name="phone"
          type="number"
          onChange={formik.handleChange}
          value={formik.values.phone}
          onBlur={formik.handleBlur}
        />
      </Grid>

      <Grid item xs={12} sm={6} className="student__id__container">
        <label htmlFor="student id" className="disabled_item">
          Student Id
        </label>
        <div>
          <input
            id="studentId"
            name="id"
            type="number"
            value={user.id}
            disabled
          />
          <Tooltip title={tooltipText}>
            <Box onClick={handleCopyStudentId}>
              <CopyIcon />
            </Box>
          </Tooltip>
        </div>
      </Grid>
      <Grid item xs={12} sm={6}>
        <label htmlFor="birth date">Birth Date</label>
        <input
          className={
            formik.touched.birth_date &&
            formik.errors.birth_date &&
            "error_field"
          }
          id="birthDate"
          name="birth_date"
          type="date"
          onChange={formik.handleChange}
          value={formik.values.birth_date}
          onBlur={formik.handleBlur}
        />
      </Grid>

      <Grid item xs={12} sm={6} className="governorate__container">
        <label htmlFor="gov">Governante</label>
        <Autocomplete
          onBlur={formik.handleBlur}
          className={
            formik.touched.government_id &&
            formik.errors.government_id &&
            "error_field"
          }
          defaultValue={user.governments}
          id="checkboxes-tags1-demo"
          options={governments?.data ? governments.data : []}
          onChange={(event, newValue) => {
            handleGov(newValue);
          }}
          getOptionLabel={(option) => option.name}
          renderInput={(params) => (
            <TextField {...params} placeholder="Select your governorate" />
          )}
          isOptionEqualToValue={(option, value) => option.id === value.id}
        />
      </Grid>

      <Grid item xs={12} sm={6} className="governorate__container">
        <label htmlFor="div" className="disabled_item">
          Divisions
        </label>
        <Autocomplete
          disabled
          onBlur={formik.handleBlur}
          className={
            formik.touched.classes && formik.errors.classes && "error_field"
          }
          defaultValue={user?.classes ? user?.classes[0] : []}
          id="checkboxes-tags-demo"
          options={classes?.data ? classes.data : []}
          onChange={(event, newValue) => {
            handleDivision(newValue);
          }}
          getOptionLabel={(option) => option.name || ""}
          renderInput={(params) => (
            <TextField {...params} placeholder="Select your devision" />
          )}
          isOptionEqualToValue={(option, value) => option.id === value.id}
        />
      </Grid>
      <Grid item xs={12} sm={12}>
        <label htmlFor="postal code">Postal Code</label>
        <input
          className={
            formik.touched.postal_code &&
            formik.errors.postal_code &&
            "error_field"
          }
          id="postalCode"
          name="postal_code"
          type="number"
          onChange={formik.handleChange}
          value={formik.values.postal_code}
          onBlur={formik.handleBlur}
        />
      </Grid>
      {state === "error" && <div className="error_msg">{errMsg}</div>}
      {state === "succeeded" && <div className="success_msg">{errMsg}</div>}

      {state === "loading" && (
        <Box className="circlar_progress">
          <CircularProgress />
        </Box>
      )}

      <Grid item xs={12}>
        <Btn
          type="submit"
          onClick={() => console.log("update")}
          text="Save Changes"
        />
      </Grid>
    </Grid>
  );
}
