import React, { useState } from "react";
import { ReactComponent as ClosedEye } from "../../assets/icons/closed-eye.svg";
import { ReactComponent as OpenedEye } from "../../assets/icons/opened-eye.svg";
import { useFormik } from "formik";
import { useDispatch, useSelector } from "react-redux";
import { updatePassword } from "../../../../data/slices/userSlice";
import * as Yup from "yup";
import { Box, Grid, CircularProgress } from "@mui/material";

export default function NewPassword({}) {
  const dispatch = useDispatch();
  const [oldPasswordIsShown, setOldPasswordIsShown] = useState(false);
  const [newPassword1IsShown, setNewPassword1IsShown] = useState(false);
  const [confirmPasswordIsShown, setConfirmPasswordIsShown] = useState(false);
  const [errorMsg, setErrorMsg] = useState("");

  const { updatePasswordPending } = useSelector((state) => state.user);
  const [detailsIsOpen, setDetailsIsOpen] = useState(false);

  const { passwordState } = useSelector((state) => state.user);

  const formik = useFormik({
    initialValues: {
      oldPassword: "",
      newPassword: "",
      confirmPassword: "",
    },
    validationSchema: Yup.object({
      oldPassword: Yup.string()
        .min(8, "At least use 8 characters")
        .required("Required"),
      newPassword: Yup.string()
        .min(8, "At least use 8 characters")
        .required("Required"),
      confirmPassword: Yup.string()
        .oneOf([Yup.ref("newPassword"), null], "Passwords must match")
        .required("Required"),
    }),
    onSubmit: (values) => {
      if (
        !values.oldPassword ||
        !values.newPassword ||
        !values.confirmPassword
      ) {
        alert("all fields are required");
        return;
      }
      if (values.newPassword !== values.confirmPassword) {
        alert("new password should be equal to confirm password");
        return;
      }
      dispatch(
        updatePassword({
          oldPassword: values.oldPassword,
          newPassword: values.newPassword,
        })
      ).then((res) => {
        if (res.error) {
          setErrorMsg(res.error.message);
        } else if (res.payload) {
          setErrorMsg("password updated successfully");
          formik.resetForm();
        }
      });
    },
  });
  return (
    <div className="section_password">
      <Grid container>
        {!detailsIsOpen ? (
          <div className="initial__password">
            <label htmlFor="pasword">Password</label>
            <div>
              <input
                required
                name="password"
                type="password"
                value=".........."
                disabled
              />

              <button
                onClick={() => setDetailsIsOpen(true)}
                className="change__btn"
              >
                Change
              </button>
            </div>
          </div>
        ) : (
          <>
            <Grid item xs={12} className="password__container">
              <label htmlFor="pasword">Old Password</label>
              <div>
                <input
                  className="input"
                  required
                  name="oldPassword"
                  type={oldPasswordIsShown ? "text" : "password"}
                  value={formik.values.oldPassword}
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}
                />

                <Box onClick={() => setOldPasswordIsShown(!oldPasswordIsShown)}>
                  {oldPasswordIsShown ? <OpenedEye /> : <ClosedEye />}
                </Box>
              </div>
              {formik.touched.oldPassword && formik.errors.oldPassword ? (
                <div className="error__msg">{formik.errors.oldPassword}</div>
              ) : null}
            </Grid>
            <Grid item xs={12} className="password__container">
              <label htmlFor="pasword">New Password</label>
              <div>
                <input
                  className="input"
                  required
                  name="newPassword"
                  type={newPassword1IsShown ? "text" : "password"}
                  value={formik.values.newPassword}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                ></input>
                <Box
                  onClick={() => setNewPassword1IsShown(!newPassword1IsShown)}
                >
                  {newPassword1IsShown ? <OpenedEye /> : <ClosedEye />}
                </Box>
              </div>
              {formik.touched.newPassword && formik.errors.newPassword ? (
                <div className="error__msg">{formik.errors.newPassword}</div>
              ) : null}
            </Grid>
            <Grid item xs={12} className="password__container">
              <label htmlFor="pasword">Confirm New Password</label>
              <div>
                <input
                  className="input"
                  required
                  name="confirmPassword"
                  type={confirmPasswordIsShown ? "text" : "password"}
                  value={formik.values.confirmPassword}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                ></input>
                <Box
                  onClick={() =>
                    setConfirmPasswordIsShown(!confirmPasswordIsShown)
                  }
                >
                  {confirmPasswordIsShown ? <OpenedEye /> : <ClosedEye />}
                </Box>
              </div>
              {formik.touched.confirmPassword &&
              formik.errors.confirmPassword ? (
                <div className="error__msg">
                  {formik.errors.confirmPassword}
                </div>
              ) : null}
            </Grid>
            {passwordState === "error" && (
              <div className="error_msg">{errorMsg}</div>
            )}
            {passwordState === "succeeded" && (
              <div className="success_msg">{errorMsg}</div>
            )}

            {passwordState === "loading" && (
              <Box className="circlar_progress">
                <CircularProgress />
              </Box>
            )}
            <Grid item xs={12} className="btns_footer">
              <button
                onClick={() => {
                  setDetailsIsOpen(false);
                  formik.resetForm();
                  setErrorMsg("");
                }}
                className="cancel__btn"
              >
                Cancel
              </button>
              <button
                className={`save__btn ${
                  (!formik.dirty ||
                    Object.keys(formik.errors).length > 0 ||
                    updatePasswordPending) &&
                  "disabled"
                }`}
                onClick={() => {
                  formik.handleSubmit();
                }}
              >
                Save
              </button>
            </Grid>
          </>
        )}
      </Grid>
    </div>
  );
}
