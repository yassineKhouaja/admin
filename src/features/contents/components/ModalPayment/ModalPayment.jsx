import React from "react";
import { Dialog, DialogContent, DialogTitle } from "@mui/material";
import { ReactComponent as Bag } from "../../assets/icons/Bag.svg";
import { ReactComponent as Close } from "../../assets/icons/Close_circle.svg";
import Info from "../Info";
import { ReactComponent as StarInfo } from "../../assets/icons/starInfo.svg";
import { ReactComponent as Chapter } from "../../assets/icons/chapter.svg";
import { ReactComponent as Video } from "../../assets/icons/video.svg";
import { ReactComponent as CreatedBy } from "../../assets/icons/createdBy.svg";
import { ReactComponent as Duration } from "../../assets/icons/duration.svg";
import { ReactComponent as Include } from "../../assets/icons/include.svg";
import Button from "../Button";
import { closeModal } from "../../../../data/slices/modals";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import convertSeconds from "../../utils/convertSeconds";

const ModalPayment = ({ id, open, handleClose, data, ...rest }) => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const subject = useSelector((state) => state.subjects.subjects.data)?.filter(
    (subject) => subject?.id === data?.id
  )?.[0];

  return (
    <Dialog
      open={open}
      onClose={(e, reason) => {
        if (reason !== "backdropClick" && reason !== "escapeKeyDown") {
          handleClose(id);
        }
      }}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
      PaperProps={{
        style: { borderRadius: 16, maxWidth: "unset" },
      }}
      className="dialog__purchase"
    >
      <div className="purchase__modal">
        <DialogTitle id="alert-dialog-title" className="modal__title">
          <div
            className={`icon__container ${
              data?.step === "confirmation" || data?.step === "registred"
                ? "confirmation"
                : "failled"
            } `}
          >
            <Bag />
          </div>
          <p>{data?.title} </p>
          <p className="subtitle">{data?.subtitle} </p>

          <span className="close-btn" onClick={() => handleClose(id)}>
            <Close />
          </span>
        </DialogTitle>
        <DialogContent className="modal__content">
          {data?.step === "confirmation" && (
            <div className="modal__card">
              <div>
                <p className="modal__title">Subject Name</p>
                <p className="modal__subtitle">{subject?.name} </p>
              </div>
              <div className="infos">
                <div className="infos__container ">
                  <div>
                    <Info
                      icon={<StarInfo />}
                      title="Rating"
                      text={` ${subject?.rating} (${subject?.number_of_ratings} Review)`}
                    />
                    <Info
                      icon={<Chapter />}
                      title="Chapters"
                      text={` ${subject?.number_of_chapters} Chapter`}
                    />
                    <Info
                      icon={<Video />}
                      title="Videos"
                      text={` ${subject?.number_of_videos || "-"} video`}
                    />
                  </div>
                  <div>
                    <Info
                      icon={<CreatedBy />}
                      title="Created by"
                      text={` ${subject?.number_of_teachers || "-"} teachers`}
                    />
                    <Info
                      icon={<Duration />}
                      title="Duration"
                      text={subject?.duration && convertSeconds(subject?.duration)}
                    />
                    <Info
                      icon={<Include />}
                      title="Include"
                      text={` ${subject?.includes || "-"} Lessons & Magazine`}
                    />
                  </div>
                </div>
              </div>
            </div>
          )}
          <div className="btn__container">
            <Button
              onClick={() => {
                navigate(data.navigate);
                dispatch(closeModal("modal-payment"));
              }}
              text={data?.btnText}
              type="button"
              className="h_42"
            />
          </div>
        </DialogContent>
      </div>
    </Dialog>
  );
};

export default ModalPayment;
