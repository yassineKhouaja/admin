import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { getComments, postComment, postReply } from "../../../../data/slices/commentsSlice";
import { openModal } from "../../../../data/slices/modals";

const CommentForm = ({ comments, id, commentId, setReply, reply = false }) => {
  const [content, setCntent] = useState("");
  const dispatch = useDispatch();
  return (
    <div className="comments_form">
      {!reply && (
        <label htmlFor="description">
          Comments <span>{comments?.length || 0}</span>
        </label>
      )}
      <textarea
        cols="40"
        rows="3"
        placeholder={`Add your ${reply ? "reply" : "comment"}`}
        id="description"
        name="description"
        type="text"
        onChange={(e) => setCntent(e.target.value)}
        value={content}
      />
      <button
        className="card_btn"
        onClick={() => {
          content !== "" &&
            dispatch(
              reply
                ? postReply({
                    content,
                    id: commentId,
                  })
                : postComment({
                    content,
                    lesson_id: id,
                  })
            )
              .unwrap()
              .then((response) => {
                dispatch(getComments(id));
                setCntent("");
                setReply && setReply(false);
                dispatch(
                  openModal("modal-messge", {
                    step: "confirmation",
                    title: "Message saved",
                    subtitle:
                      "Your message was saved, it will appear when confirmed by the administrateur",
                  })
                );
              });
        }}
      >
        Send
      </button>
    </div>
  );
};

export default CommentForm;
