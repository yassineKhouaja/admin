import * as React from "react";
import { styled } from "@mui/material/styles";
import { Box } from "@mui/material";
import LinearProgress, {
  linearProgressClasses,
} from "@mui/material/LinearProgress";

const BorderLinearProgress = styled(LinearProgress)(({ theme }) => ({
  height: 10,
  borderRadius: 5,
  display: "inline-block",
  marginRight: "24px",
  width: "calc(100% - 60px)",
  [`&.${linearProgressClasses.colorPrimary}`]: {
    backgroundColor: "#C8E8F5",
  },
  [`& .${linearProgressClasses.bar}`]: {
    borderRadius: 5,
    backgroundColor: "#30C1D9",
  },
}));

export default function ProgressBar({ value }) {
  return (
    <>
      <Box sx={{ flexGrow: 1 }}>
        <BorderLinearProgress variant="determinate" value={value} />
        <span className="progress_bar-text">{value}%</span>
      </Box>
    </>
  );
}
