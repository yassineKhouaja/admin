import React from "react";
import { ReactComponent as StarRating } from "../../assets/icons/StarRating.svg";

const SubjectInfo = ({ chapter, seconds, reviews, recording = false }) => {
  return (
    <div className="card__info">
      {!recording && <p>{chapter} Chapter </p>}

      <p>{(seconds / 3600).toFixed(1)} Hours </p>
      <p>
        <StarRating /> {reviews.rating} ({reviews.total} Reviews)
      </p>
    </div>
  );
};

export default SubjectInfo;
