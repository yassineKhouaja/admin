import React, { useEffect } from "react";
import { useParams } from "react-router-dom";
import ProgressBar from "../../components/ProgressBar/ProgressBar";
import { ReactComponent as StarInfo } from "../../assets/icons/starInfo.svg";
import { ReactComponent as Chapter } from "../../assets/icons/chapter.svg";
import { ReactComponent as Video } from "../../assets/icons/video.svg";
import { ReactComponent as CreatedBy } from "../../assets/icons/createdBy.svg";
import { ReactComponent as Duration } from "../../assets/icons/duration.svg";
import { ReactComponent as Include } from "../../assets/icons/include.svg";
import FavoriteButton from "../FavoriteButton";
import Info from "../Info";
import { useDispatch, useSelector } from "react-redux";
import ChaptersCard from "../ChaptersCard/ChaptersCard";
import convertSeconds from "../../utils/convertSeconds";
import PurchaseButton from "../PurchaseButton";
import { subjectWishlist, subjectWishlistToggle } from "../../../../data/slices/subjectsSlice";
import TabsContainer from "../../../library/components/TabsContainer";
import useIsMountedRef from "../../../../hooks/useIsMounted";
import { getRecordings } from "../../../../data/slices/recordingsSlice";

const Chapters = () => {
  const subjectId = parseInt(useParams().subjectId, 10);
  const dispatch = useDispatch();
  const chaptersList = useSelector((state) => state.chapters.chapters.data).filter((chapter) =>
    chapter.subjects.some((subject) => parseInt(subject.id, 10) === parseInt(subjectId, 10))
  );
  const recordingsList = useSelector((state) => state.recordings.recordings.data);

  const currentSubject = useSelector((state) => state.subjects.currentSubject.data);
  const data = useSelector((state) => state.subjects.subjectWishlist.data);
  const chaptersWishlist = useSelector((state) => state.chapters.chapterWishlist.data);
  const { data: subjectsWishlist } = useSelector((state) => state.subjects.subjectWishlist);
  const isMounted = useIsMountedRef();

  useEffect(() => {
    if (!isMounted.current) return;
    dispatch(getRecordings(subjectId));
  }, []);

  return (
    <main className="chapters">
      <section className="chapters__header">
        <div className="card__image-container">
          <p>{currentSubject?.slug}</p>
          <FavoriteButton
            toggleFavorite={() => {
              dispatch(subjectWishlistToggle({ subject_id: subjectId }));
              dispatch(subjectWishlist());
            }}
            isFavorite={data?.find((wishlist) => wishlist.subject_id === subjectId)?.is_fav}
          />
        </div>

        <div className="card__content">
          <p className="card__name">Subject Name</p>

          <p className="card__title">{currentSubject?.name}</p>

          <ProgressBar value={currentSubject?.progress || 0} />
        </div>

        <div className="card__infos">
          <div>
            <Info
              icon={<StarInfo />}
              title="Rating"
              text={` ${currentSubject?.rating} (${currentSubject?.number_of_ratings} Review)`}
            />
            <Info
              icon={<Chapter />}
              title="Chapters"
              text={` ${currentSubject?.number_of_chapters} Chapter`}
            />
            <Info
              icon={<Video />}
              title="Videos"
              text={` ${currentSubject?.number_of_videos || "-"} video`}
            />
          </div>
          <div>
            <Info
              icon={<CreatedBy />}
              title="Created by"
              text={` ${currentSubject?.number_of_teachers || "-"} teachers`}
            />
            <Info
              icon={<Duration />}
              title="Duration"
              text={currentSubject?.duration && convertSeconds(currentSubject?.duration)}
            />
            <Info
              icon={<Include />}
              title="Include"
              text={` ${currentSubject?.includes || "-"} Lessons & Magazine`}
            />
          </div>
        </div>

        <div className="card__btn">
          <p>
            {subjectsWishlist?.find((wish) => wish["subject_id"] === subjectId)?.purchase_due_date
              ? "Subscription duration"
              : "click bellow"}
          </p>
          <div className="btn__container">
            <PurchaseButton attribute={"subject_id"} id={subjectId} wishlist={data} />
          </div>
        </div>
      </section>
      {/* <Filters text="Chapters" filters={["Sort", "Filter"]} /> */}
      <div className="tabsContainer">
        <TabsContainer tabs={["Chapters", "recording sessions"]}>
          <div className="subjects">
            <ChaptersCard
              chaptersList={chaptersList?.filter((chapter) => chapter?.chapter_type_id === 1)}
              chaptersWishlist={chaptersWishlist}
            />
          </div>
          <div className="subjects">
            {recordingsList?.length <= 0 && (
              <div className="no_content">There is no recordings for now</div>
            )}
            {recordingsList?.length > 0 &&
              recordingsList?.map((recording) => (
                <section key={recording.id}>
                  <div className="tab">
                    <p>{"Group session: " + recording?.name}</p>
                  </div>
                  <ChaptersCard
                    chaptersList={recording?.sessions?.map((session) => session.chapter)}
                    chaptersWishlist={chaptersWishlist}
                    studentLevel={recording?.StudentLevels}
                    recording={true}
                  />
                </section>
              ))}
          </div>
        </TabsContainer>
      </div>
    </main>
  );
};

export default Chapters;
