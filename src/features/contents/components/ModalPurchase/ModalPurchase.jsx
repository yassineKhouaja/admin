import React, { useState } from "react";
import { Dialog, DialogContent, DialogTitle } from "@mui/material";
import { ReactComponent as Bag } from "../../assets/icons/Bag.svg";
import { ReactComponent as Close } from "../../assets/icons/Close_circle.svg";
import Info from "../Info";
import { ReactComponent as StarInfo } from "../../assets/icons/starInfo.svg";
import { ReactComponent as Chapter } from "../../assets/icons/chapter.svg";
import { ReactComponent as Video } from "../../assets/icons/video.svg";
import { ReactComponent as CreatedBy } from "../../assets/icons/createdBy.svg";
import { ReactComponent as Duration } from "../../assets/icons/duration.svg";
import { ReactComponent as Include } from "../../assets/icons/include.svg";
import Button from "../Button";
import Period from "../Period";
import { closeModal, openModal } from "../../../../data/slices/modals";
import { useDispatch, useSelector } from "react-redux";
import convertSeconds from "../../utils/convertSeconds";
import { buySubject, subjectWishlist } from "../../../../data/slices/subjectsSlice";
import { getPurchasedChapters } from "../../../../data/slices/chaptersSlice";
import { getMe } from "../../../../data/slices/authSlice";

const ModalPurchase = ({ id, open, handleClose, data, ...rest }) => {
  const dispatch = useDispatch();
  const [reduction, setReduction] = useState(null);
  const subject = useSelector((state) => state.subjects.subjects.data)?.filter(
    (subject) => subject?.id === data?.id
  )?.[0];
  const amount = useSelector((state) => state.subjects.purshaseSubject.data)?.filter(
    (subject) => subject?.subject_id === data?.id
  )?.[0]?.amount;
  const classes = useSelector((state) => state?.auth?.user?.classes);

  return (
    <Dialog
      open={open}
      onClose={(e, reason) => {
        if (reason !== "backdropClick" && reason !== "escapeKeyDown") {
          handleClose(id);
        }
      }}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
      PaperProps={{
        style: { borderRadius: 16, maxWidth: "unset" },
      }}
      className="dialog__purchase"
    >
      <div className="purchase__modal">
        <DialogTitle id="alert-dialog-title" className="modal__title">
          <div className={"icon__container"}>
            <Bag />
          </div>
          <p>{data?.title} </p>
          <span className="close-btn" onClick={() => handleClose(id)}>
            <Close />
          </span>
        </DialogTitle>
        <DialogContent className="modal__content">
          <div className="modal__card">
            <div>
              <p className="modal__title">Subject Name</p>
              <p className="modal__subtitle">{subject?.name} </p>
            </div>
            <div className="infos">
              <div className="infos__container ">
                <div>
                  <Info
                    icon={<StarInfo />}
                    title="Rating"
                    text={` ${subject?.rating} (${subject?.number_of_ratings} Review)`}
                  />
                  <Info
                    icon={<Chapter />}
                    title="Chapters"
                    text={` ${subject?.number_of_chapters} Chapter`}
                  />
                  <Info
                    icon={<Video />}
                    title="Videos"
                    text={` ${subject?.number_of_videos || "-"} video`}
                  />
                </div>
                <div>
                  <Info
                    icon={<CreatedBy />}
                    title="Created by"
                    text={` ${subject?.number_of_teachers || "-"} teachers`}
                  />
                  <Info
                    icon={<Duration />}
                    title="Duration"
                    text={subject?.duration && convertSeconds(subject?.duration)}
                  />
                  <Info
                    icon={<Include />}
                    title="Include"
                    text={` ${subject?.includes || "-"} Lessons & Magazine`}
                  />
                </div>
              </div>
            </div>
          </div>
          <div className="period__container">
            <p>Period</p>
            <Period setReduction={setReduction} />
          </div>
          <div className="btn__container">
            <Button
              onClick={() => {
                dispatch(
                  buySubject({
                    subject_id: subject?.id,
                    reduction_period_id: reduction.id,
                  })
                )
                  .unwrap()
                  .then((originalPromiseResult) => {
                    dispatch(
                      openModal("modal-payment", {
                        period: false,
                        step: "confirmation",
                        title: "your payment has been successfully",
                        subtitle: "Now you have the acces to the chapter",
                        btnText: "Access the chapter",
                        navigate: `/subjects/${data?.id}`,
                        id: data.id,
                      })
                    );

                    dispatch(getMe());
                    dispatch(getPurchasedChapters(classes?.[0]?.id));
                    dispatch(subjectWishlist());
                  })
                  .catch((rejectedValueOrSerializedError) => {
                    dispatch(
                      openModal("modal-payment", {
                        period: false,
                        step: "failled",
                        title: "your payment failled",
                        subtitle: "Please check with the administrator",
                        btnText: "Go back to subjects",
                        navigate: `/subjects`,
                        close: true,
                        id: data.id,
                      })
                    );
                  });

                dispatch(closeModal("modal-purshase"));
              }}
              text={
                amount
                  ? `Buy Now with ${Math.round(
                      amount * reduction?.period * (1 - reduction?.reduction / 100)
                    )} PTS`
                  : "price not availiable"
              }
              type="button"
              className="h_42"
            />
          </div>
        </DialogContent>
      </div>
    </Dialog>
  );
};

export default ModalPurchase;
