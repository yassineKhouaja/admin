import React from "react";
import { ReactComponent as User } from "../../assets/icons/user.svg";

const Overview = ({ data }) => {
  return (
    <div className="overview">
      <table>
        <tbody>
          <tr>
            <td>
              <p>Pièces jointes</p>
            </td>
            <td style={{ width: "16px" }}></td>
            <td>
              <div className="attachement">
                <p>N°1_Concours Prépa</p>
                <button>Download</button>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <p>A propos de l'instructeur</p>
            </td>
            <td style={{ width: "16px" }}></td>
            <td>
              <div className="teacher">
                {data?.teacher?.photo_url ? <img src={data?.teacher?.photo_url} /> : <User />}

                <div className="teacher_info">
                  <p className="title">
                    {data?.teacher?.first_name + " " + data?.teacher?.last_name}
                  </p>
                  <p className="subtitle">Prof chez TakiAcademy</p>
                </div>
              </div>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  );
};

export default Overview;
