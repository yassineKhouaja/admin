import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import useIsMountedRef from "../../../../hooks/useIsMounted";

const Period = ({ setReduction = () => {}, className = "" }) => {
  const [select, setSelect] = useState(0);
  const periods = useSelector((state) => state.subjects.periods.data);
  const max = periods?.length || 0;
  const isMounted = useIsMountedRef();

  useEffect(() => {
    setSelect(0);
    setReduction(periods[0]);
  }, [periods]);

  return (
    <div className={"periods " + className}>
      {periods?.map((period, index) => (
        <React.Fragment key={index}>
          <button
            className={`btn__period ${select === index ? "active" : ""}`}
            onClick={() => {
              setSelect(index);
              setReduction(period);
            }}
          >
            {period.period + " month"}
          </button>
          {max !== index + 1 && <span className="separation"></span>}
        </React.Fragment>
      ))}
    </div>
  );
};

export default Period;
