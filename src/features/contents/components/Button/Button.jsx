import React from "react";

export default function Button({ onClick, text, type, className }) {
  return (
    <button type={type} className={`card_btn ${className}`} onClick={() => onClick()}>
      {text}
    </button>
  );
}
