import { useEffect, useState } from "react";
import { ReactComponent as Favorite } from "../../assets/icons/favorite.svg";

const FavoriteButton = ({ isFavorite, toggleFavorite }) => {
  const [favorite, setFavorite] = useState();

  useEffect(() => {
    setFavorite(isFavorite);
  }, [isFavorite]);
  return (
    <button
      className={favorite ? "btn__favorite active" : "btn__favorite"}
      onClick={() => {
        setFavorite((favorite) => !favorite);
        toggleFavorite();
      }}
    >
      <Favorite />
    </button>
  );
};

export default FavoriteButton;
