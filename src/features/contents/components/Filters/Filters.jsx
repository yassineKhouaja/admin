import React from "react";
import FilterButton from "../FilterButton";

const Filters = ({ text, filters }) => {
  return (
    <div className="filter">
      <p className="filterText">{text} </p>
      <div className="filterOptions">
        {filters.map((filter) => (
          <FilterButton text={filter} key={filter} />
        ))}
      </div>
    </div>
  );
};

export default Filters;
