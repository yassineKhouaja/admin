import moment from "moment/moment";
import React, { useState } from "react";
import { useDispatch } from "react-redux";
import {
  getComments,
  postCommentReaction,
  postReplyReaction,
} from "../../../../data/slices/commentsSlice";
import CommentForm from "../CommentForm";
import { ReactComponent as Emoji } from "../../assets/icons/emoji.svg";
import { ReactComponent as User } from "../../assets/icons/user.svg";

const Comment = ({ comment, isReply = false, id }) => {
  const [state, setState] = useState(false);
  const [reply, setReply] = useState(false);
  const dispatch = useDispatch();
  const reactionCount = (id) =>
    comment?.reactions.reduce((prev, cur) => (cur?.reaction_type_id === id ? prev + 1 : prev), 0);
  const like = { emoji: "👍", count: reactionCount(1) };
  const heart = { emoji: "♥️", count: reactionCount(2) };
  const laugh = { emoji: "😂", count: reactionCount(3) };
  const angry = { emoji: "😡", count: reactionCount(4) };

  const postReaction = (reactionId) => {
    dispatch(
      isReply
        ? postReplyReaction({ id: comment?.id, reactionId: reactionId })
        : postCommentReaction({ id: comment?.id, reactionId: reactionId })
    )
      .unwrap()
      .then((response) => dispatch(getComments(comment?.lesson_id || id)))
      .catch((err) => console.log(err));
  };

  return (
    <div className={isReply ? "comment" : "comment border"}>
      <div className="author">
        {comment?.author?.photo_url ? <img src={comment?.author?.photo_url} /> : <User />}
        <span className="author_name">
          {comment?.author?.first_name} {comment?.author?.last_name}
        </span>
      </div>
      <p className="comment_content">{comment?.content}</p>
      <div className="reaction">
        <p
          className="emoji_react"
          onMouseEnter={() => setState(true)}
          onMouseLeave={() => setState(false)}
        >
          <Emoji />
          {state && (
            <div className="emojis_react">
              <span onClick={() => postReaction(1)}>👍</span>
              <span onClick={() => postReaction(2)}>♥️</span>
              <span onClick={() => postReaction(3)}>😂</span>
              <span onClick={() => postReaction(4)}>😡</span>
            </div>
          )}
        </p>
        {![like, heart, laugh, angry].every((emoji) => emoji.count === 0) && (
          <p className="emoji">
            {[like, heart, laugh, angry].map(
              (emoji, index) =>
                emoji.count !== 0 && (
                  <span key={index}>
                    {emoji.emoji}&nbsp;
                    {emoji.count}
                  </span>
                )
            )}
          </p>
        )}

        {!isReply && (
          <>
            <span></span>
            <p className="reply" onClick={() => setReply((reply) => !reply)}>
              reply
            </p>
          </>
        )}
        <span></span>
        <p>{moment(comment?.created_at).fromNow()} </p>
      </div>
      {reply && !isReply && (
        <div style={{ marginTop: "12px" }}>
          <CommentForm
            id={comment?.lesson_id}
            reply={!isReply}
            commentId={comment?.id}
            setReply={setReply}
          />
        </div>
      )}
      {comment?.replies?.length > 0 && (
        <div className="comment_reply">
          {comment?.replies?.map((reply) => (
            <Comment comment={reply} key={reply?.id} isReply={true} id={comment?.lesson_id} />
          ))}
        </div>
      )}
    </div>
  );
};

export default Comment;
