import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import Progress from "../../../../components/Prgoress/Progress";
import {
  chapterWishlist,
  getChapters,
  getPurchasedChapters,
} from "../../../../data/slices/chaptersSlice";
import { getPeriods, getSubjects, subjectWishlist } from "../../../../data/slices/subjectsSlice";
import useIsMountedRef from "../../../../hooks/useIsMounted";
import SubjectCards from "../SubjectCards";

const Subjects = () => {
  const isMounted = useIsMountedRef();
  const dispatch = useDispatch();
  const { data, status } = useSelector((state) => state.subjects.subjects);
  const { data: subjectsWishlist } = useSelector((state) => state.subjects.subjectWishlist);
  const { classes } = useSelector((state) => state.auth.user);
  const { status: subjectsStatus } = useSelector((state) => state.subjects.subjects);

  useEffect(() => {
    if (!isMounted.current) return;
    dispatch(chapterWishlist());
    dispatch(getPurchasedChapters());
    dispatch(getPeriods());
    dispatch(subjectWishlist());
    dispatch(getChapters(classes?.[0]?.id));
    dispatch(getSubjects(classes?.[0]?.id));
  }, []);

  if (status === "idle" || status === "loading") return <Progress />;

  if (status === "succeeded")
    return (
      // <Filters text="Filter" filters={["Chapters", "Reviews", "Duration"]} />
      <div className="subjects">
        <SubjectCards subjects={data} subjectsWishlist={subjectsWishlist} />
      </div>
    );

  if (status === "failed") return <div>Something went wrong, please contact the admin</div>;
};

export default Subjects;
