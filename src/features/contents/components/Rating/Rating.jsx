import * as React from "react";
import { Rating as RatingMui, Stack } from "@mui/material";

export default function Rating({ rating, setRating, value, readOnly = false }) {
  return (
    <Stack spacing={1}>
      <RatingMui
        name="half-rating"
        precision={0.5}
        onChange={(event, newValue) => {
          setRating(newValue);
        }}
        value={value || rating}
        readOnly={readOnly}
      />
    </Stack>
  );
}
