import React, { useEffect, useState } from "react";
import { Tabs, Tab, Box } from "@mui/material";
import { ReactComponent as PlayIcon } from "../../assets/icons/Play.svg";
import { ReactComponent as Lock } from "../../assets/icons/lock.svg";
import { ReactComponent as CkeckedPlay } from "../../assets/icons/ckeckedPlay.svg";
import { ReactComponent as Magazine } from "../../assets/icons/magazine.svg";
import secondsToMinutes from "../../utils/secondsToMinutes";
import fileDownload from "js-file-download";
import axios from "axios";
import { useDispatch } from "react-redux";
import { getComments } from "../../../../data/slices/commentsSlice";
import { useLocation } from "react-router-dom";
import useIsMountedRef from "../../../../hooks/useIsMounted";

function TabPanel(props) {
  const { children, value, index, ...other } = props;
  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ color: "black" }}>
          <div>{children}</div>
        </Box>
      )}
    </div>
  );
}

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}
const handleMagazineDownload = (url, filename) => {
  const fileExtension = url?.split(".")[url?.split(".")?.length - 1];
  axios
    .get(url, {
      responseType: "blob",
    })
    .then((res) => {
      fileDownload(res.data, `${filename}.${fileExtension}`);
    })
    .catch((err) => {
      alert("There is an error while downloadingt the file");
      return;
    });
};

function TabsContent({
  sections,
  magazines,
  currentVideo,
  currentMagazine,
  setCurrentVideo,
  setCurrentMagazine,
  setIsPlaying,
}) {
  const disptach = useDispatch();
  const isMounted = useIsMountedRef();
  const location = useLocation();
  const [value, setValue] = useState(0);

  const handleChange = (lessons, newValue) => {
    setValue(newValue);
    newValue === 0 ? setCurrentMagazine(null) : setCurrentVideo(null);
  };

  useEffect(() => {
    if (!isMounted.current) return;

    return () => {
      setCurrentVideo(null);
      setCurrentMagazine(null);
    };
  }, [location]);
  magazines?.length <= 0 && currentMagazine !== "" && setCurrentMagazine("");

  return (
    <Box sx={{ width: "100%" }}>
      <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
        <Tabs
          value={value}
          onChange={handleChange}
          TabIndicatorProps={{ style: { background: "#937FF1" } }}
          aria-label="basic tabs example"
        >
          <Tab label="Lessons" {...a11yProps(0)} disableRipple />
          <Tab label="Magazines" {...a11yProps(1)} disableRipple />
        </Tabs>
      </Box>
      <TabPanel value={value} index={0}>
        <div className="videos">
          {sections?.map((section, indexSection) => (
            <div className="videos__group" key={section.id}>
              <p className="videos__title">{section?.description} </p>
              {section?.lessons
                ?.filter((lesson) => lesson?.lessonTypes?.value !== "magazine")
                ?.map((lesson, indexLesson) => {
                  if (
                    currentVideo === null &&
                    value === 0 &&
                    indexSection === 0 &&
                    indexLesson === 0
                  )
                    setCurrentVideo(lesson);

                  return (
                    <p
                      className={
                        currentVideo?.id === lesson?.id ? "video__info active" : "video__info"
                      }
                      key={`video${lesson.id}`}
                      onClick={() => {
                        setCurrentVideo(lesson);
                        setIsPlaying(false);
                        disptach(getComments(lesson?.id));
                      }}
                    >
                      {lesson?.video_url ? <PlayIcon /> : <Lock />}
                      <span className="video__title">{lesson?.name}</span>
                      <span className="video__duration">
                        {lesson?.duration && secondsToMinutes(lesson?.duration)}
                      </span>
                    </p>
                  );
                })}
            </div>
          ))}
        </div>
      </TabPanel>
      <TabPanel value={value} index={1}>
        <div className="magazines">
          {magazines?.length <= 0 ? (
            <p className="no_magazine">No magazine for this chapter</p>
          ) : (
            magazines?.map((magazine, index) => {
              if (currentMagazine === null && value === 1 && index === 0)
                setCurrentMagazine(magazine);
              return (
                <p
                  className={
                    currentMagazine?.id === magazine?.id
                      ? "magazine__info active"
                      : "magazine__info"
                  }
                  key={`video${magazine?.id}`}
                  onClick={() => setCurrentMagazine(magazine)}
                >
                  <span className="icon__container">
                    <Magazine />
                  </span>
                  <span className="magazine__title">{magazine?.name}</span>

                  <button
                    className="btn__download"
                    disabled={!magazine?.is_downloadable}
                    onClick={() =>
                      handleMagazineDownload(magazine?.file_name || "magazine", magazine?.name)
                    }
                  >
                    Download
                  </button>
                </p>
              );
            })
          )}
        </div>
      </TabPanel>
    </Box>
  );
}

export default TabsContent;
