import * as React from "react";
import { MenuItem, FormHelperText, FormControl, Select } from "@mui/material";

import { Formik } from "formik";

export default function SelectOption({ options, formik }) {
  // const [age, setAge] = React.useState("");

  const handleChange = (event) => {
    formik?.setFieldValue("refund_reason_message_id", event.target.value);
  };

  return (
    <FormControl>
      <Select
        value={formik.values.refund_reason_message_id}
        onChange={handleChange}
        inputProps={{ "aria-label": "Without label" }}
        sx={{ width: "100%", borderRadius: "10px", height: "52px" }}
      >
        <MenuItem value="">Choose your reason</MenuItem>
        {options?.map((option) => (
          <MenuItem value={option.id} key={option.id}>
            {option.content}
          </MenuItem>
        ))}
      </Select>
      <FormHelperText sx={{ color: "#e72b47" }}>
        {formik?.errors?.refund_reason_message_id || " "}
      </FormHelperText>
    </FormControl>
  );
}
