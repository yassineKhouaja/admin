const Info = ({ icon, title, text }) => {
  return (
    <div className="info__container">
      {icon}
      <p>
        {title ? title + ":" : ""}
        <span className="w-500">{text}</span>
      </p>
    </div>
  );
};

export default Info;
