import React, { useState } from "react";
import { ReactComponent as Arrow } from "../../assets/icons/arrowBottom.svg";

const FilterButton = ({ text }) => {
  const [state, setstate] = useState(false);
  return (
    <button
      className={state ? "filter-btn active" : "filter-btn"}
      onClick={() => setstate((state) => !state)}
    >
      <span className="text-btn">{text}</span>
      <Arrow />
    </button>
  );
};

export default FilterButton;
