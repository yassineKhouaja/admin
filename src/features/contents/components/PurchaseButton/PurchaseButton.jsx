import React from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { openModal } from "../../../../data/slices/modals";
import { getPurshaseSubjectPrice } from "../../../../data/slices/subjectsSlice";
import Button from "../Button";

const PurchaseButton = ({ attribute, id, wishlist, subjectId, price }) => {
  const dispatch = useDispatch();
  const isPurchased = wishlist?.find((wish) => wish[attribute] === id);
  const navigate = useNavigate();
  return (
    <Button
      onClick={() => {
        if (!isPurchased?.purchase_due_date) {
          if (attribute === "subject_id") {
            dispatch(getPurshaseSubjectPrice());
            dispatch(
              openModal("modal-purshase", {
                step: "purshase",
                title: "Buy a Subject",
                id: id,
              })
            );
          } else {
            dispatch(
              openModal("modal-purshase-chapter", {
                step: "purshase",
                title: "Buy a chapter",
                id: id,
                price,
              })
            );
          }
          return;
        }
        if (attribute === "subject_id") {
          navigate(`/subjects/${id}`);
        } else {
          navigate(`/subjects/${subjectId}/${id}`);
        }
      }}
      text={
        isPurchased?.purchase_due_date
          ? `Expires on ${isPurchased?.purchase_due_date?.slice(0, 10)}`
          : "Buy now"
      }
      type="button"
      className={isPurchased?.purchase_due_date ? "purchased" : ""}
    />
  );
};

export default PurchaseButton;
