import React from "react";
import { NavLink } from "react-router-dom";
import ProgressBar from "../ProgressBar";
import SubjectInfo from "../SubjectInfo/SubjectInfo";
import { useDispatch } from "react-redux";
import FavoriteButton from "../FavoriteButton";
import {
  currentSubject,
  subjectWishlist,
  subjectWishlistToggle,
} from "../../../../data/slices/subjectsSlice";
import PurchaseButton from "../PurchaseButton";

const SubjectCards = ({ subjects, subjectsWishlist, filter = false, favorite = false }) => {
  const dispatch = useDispatch();
  const subjectsList = subjects
    .filter((subject) =>
      filter ? subjectsWishlist.find((sw) => sw.id === subject.id)?.purchase_due_date : true
    )
    .filter((subject) =>
      favorite ? subjectsWishlist.find((sw) => sw.id === subject.id)?.is_fav : true
    );
  return (
    <>
      {subjectsList?.length <= 0 && <div className="no_content">There is no subjects for now</div>}
      {subjectsList?.length > 0 && (
        <section className="cards">
          {subjectsList.map((subject) => (
            <div
              className="card"
              key={subject.id}
              onClick={() => {
                dispatch(currentSubject(subject));
              }}
            >
              <FavoriteButton
                toggleFavorite={() => {
                  dispatch(subjectWishlistToggle({ subject_id: subject.id }))
                    .unwrap()
                    .then((response) => dispatch(subjectWishlist()))
                    .catch((response) => console.log(response));
                }}
                isFavorite={
                  subjectsWishlist?.find((wishlist) => wishlist.subject_id === subject.id)?.is_fav
                }
              />
              <NavLink to={`/subjects/${subject.id}`}>
                <div className="card__image-container">
                  <p>{subject.slug}</p>
                </div>
                <div className="card__content">
                  <p className="card__title">{subject.name} </p>
                  <div className="card__info">
                    <ProgressBar value={subject.progress || 0} />
                  </div>
                  <SubjectInfo
                    chapter={subject.number_of_chapters}
                    seconds={subject.duration}
                    reviews={{ rating: subject.rating, total: subject.number_of_ratings }}
                  />
                </div>
              </NavLink>
              <div className="btn__container">
                <PurchaseButton
                  id={subject.id}
                  attribute="subject_id"
                  wishlist={subjectsWishlist}
                />
              </div>
            </div>
          ))}
        </section>
      )}
    </>
  );
};

export default SubjectCards;
