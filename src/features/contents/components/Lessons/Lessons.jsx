import React, { useEffect, useState } from "react";
import ReactPlayer from "react-player";
import { ReactComponent as Play } from "../../assets/images/play.svg";
import { ReactComponent as StarInfo } from "../../assets/icons/starInfo.svg";
import { ReactComponent as Chapter } from "../../assets/icons/chapter.svg";
import { ReactComponent as Duration } from "../../assets/icons/duration.svg";
import { ReactComponent as Dots } from "../../assets/icons/dots.svg";
import { ReactComponent as Refund } from "../../assets/icons/refund.svg";
import { ReactComponent as LockBtn } from "../../assets/icons/lockBtn.svg";
import Info from "../Info";
import TabsContent from "../TabsContainer";
import { openModal } from "../../../../data/slices/modals";
import { useDispatch, useSelector } from "react-redux";
import { useLocation, useParams } from "react-router-dom";
import useIsMountedRef from "../../../../hooks/useIsMounted";
import { getRefundReasons, getSections } from "../../../../data/slices/lessonsSlice";
import convertSeconds from "../../utils/convertSeconds";
import TabsContainer from "../../../library/components/TabsContainer/TabsContainer";

import Comment from "../Comment";
import CommentForm from "../CommentForm";
import useWindowSize from "../../../../hooks/useWindowSize";
import Overview from "../Overview";
import RatingFeedback from "../RatingFeedback/RatingFeedback";
import { getReviews } from "../../../../data/slices/commentsSlice";

const Lessons = () => {
  const [isPlaying, setIsPlaying] = useState(false);
  const [refund, setRefund] = useState(false);
  const [currentVideo, setCurrentVideo] = useState(null);
  const [currentMagazine, setCurrentMagazine] = useState(null);
  const isMounted = useIsMountedRef();
  const { subjectId, chapterId } = useParams();
  const dispatch = useDispatch();
  const chapters = useSelector((state) => state.lessons.sections.data);
  const comments = useSelector((state) => state.comments.comments.data);
  const location = useLocation();
  const { width } = useWindowSize();
  const [tabIndex, setTabIndex] = useState(0);
  console.log(currentVideo);
  useEffect(() => {
    if (!isMounted.current) return;
    dispatch(getSections(chapterId));
    dispatch(getReviews(chapterId));
  }, []);
  useEffect(() => {
    if (!isMounted.current) return;

    return () => {
      setCurrentVideo(null);
      setCurrentMagazine(null);
    };
  }, [location]);

  return (
    <main className="lessons">
      {currentVideo === null && currentMagazine === "" && (
        <div className="no_content" style={{ height: "500px" }}>
          There is no magazine for this lesson
        </div>
      )}
      {currentMagazine !== null && currentMagazine !== "" && (
        <iframe
          src={
            currentMagazine?.file_name?.includes("http")
              ? currentMagazine?.file_name + "#toolbar=0"
              : ""
          }
          width="100%"
        ></iframe>
      )}
      {currentVideo !== null && currentVideo?.video_url !== null && (
        <div className="container">
          <section className="video__container">
            <div className="title">
              <p className="tag">{chapters?.levels?.[0]?.name || "Easy"}</p>
              <p>{chapters?.name} </p>
              <span className="icon__container" onClick={() => setRefund((prev) => !prev)}>
                <Dots />
              </span>
              {refund && (
                <div
                  className="refund"
                  onClick={() => {
                    dispatch(getRefundReasons());
                    setRefund(false);
                    dispatch(openModal("modal-refund", { chapterId: parseInt(chapterId, 10) }));
                  }}
                >
                  <Refund />
                  <p>Refund</p>
                </div>
              )}
            </div>

            {!isPlaying && (
              <div className="play__btn">
                <Play />
              </div>
            )}

            <ReactPlayer
              controls
              width={"100%"}
              height={"auto"}
              onPlay={() => setIsPlaying(true)}
              url={currentVideo?.video_url}
              onClick={() => {
                setIsPlaying(true);
              }}
              onPause={() => {
                setIsPlaying(false);
              }}
            />
          </section>
          <main className="tabsContainer">
            <TabsContainer
              tabs={
                width > 1500
                  ? ["Lesson overview", "Comments", "Rating"]
                  : ["Lesson overview", "Comments", "Rating", "Content"]
              }
              setTabIndex={setTabIndex}
            >
              <Overview data={chapters} />
              <div>
                <CommentForm comments={comments} id={currentVideo?.id} />
                <div className="comments_container">
                  {[...comments]
                    ?.sort((a, b) => new Date(b.created_at) - new Date(a.created_at))
                    ?.map((comment) => (
                      <Comment comment={comment} key={comment?.id} />
                    ))}
                </div>
              </div>
              <RatingFeedback />
              <div></div>
            </TabsContainer>
          </main>
        </div>
      )}
      {currentVideo?.video_url === null && (
        <div className="container" style={{ height: "100%" }}>
          <section className="video__container">
            <div className="title">
              <p className="tag">{chapters?.levels?.[0]?.name || "Easy"}</p>
              <p>{chapters?.name} </p>
              <span className="icon__container">
                <Dots />
              </span>
            </div>
            <div className="lock_container">
              <div className="play__btn">
                <LockBtn />
              </div>
            </div>
          </section>
        </div>
      )}

      <section
        className="contents"
        style={{
          display: width < 1500 ? (tabIndex === 3 ? "block" : "none") : "block",
        }}
      >
        <div className="contents__header">
          <p className="title">Overview</p>
          <div className="icons__container">
            <Info icon={<Chapter />} text={`${chapters?.sections?.length} sections`} />
            <Info
              icon={<Duration />}
              text={
                chapters?.total_duration && convertSeconds(parseInt(chapters?.total_duration, 10))
              }
            />
            <Info
              icon={<StarInfo />}
              text={`${chapters?.ratings_average || 0} (${
                chapters?.number_of_ratings || 0
              } Review)`}
            />
          </div>
        </div>
        <TabsContent
          sections={chapters?.sections}
          magazines={chapters?.sections
            ?.map((section) =>
              section?.lessons?.filter((lesson) => lesson?.lessonTypes?.value === "magazine")
            )
            .flat()}
          currentVideo={currentVideo}
          currentMagazine={currentMagazine}
          setCurrentVideo={setCurrentVideo}
          setCurrentMagazine={setCurrentMagazine}
          setIsPlaying={setIsPlaying}
          tabIndex={tabIndex}
        />
      </section>
    </main>
  );
};

export default Lessons;
