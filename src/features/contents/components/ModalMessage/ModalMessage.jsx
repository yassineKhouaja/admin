import React from "react";
import { Dialog, DialogContent, DialogTitle } from "@mui/material";
import { ReactComponent as Bag } from "../../assets/icons/Bag.svg";
import { ReactComponent as Close } from "../../assets/icons/Close_circle.svg";

const ModalMessage = ({ id, open, handleClose, data, ...rest }) => {
  return (
    <Dialog
      open={open}
      onClose={(e, reason) => {
        if (reason !== "backdropClick" && reason !== "escapeKeyDown") {
          handleClose(id);
        }
      }}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
      PaperProps={{
        style: { borderRadius: 16, maxWidth: "unset" },
      }}
      className="dialog__purchase"
    >
      <div className="purchase__modal">
        <DialogTitle id="alert-dialog-title" className="modal__title">
          <div
            className={`icon__container ${
              data?.step === "confirmation" ? "confirmation" : "failled"
            } `}
          >
            <Bag />
          </div>
          <p>{data?.title} </p>
          <p className="subtitle">{data?.subtitle} </p>

          <span className="close-btn" onClick={() => handleClose(id)}>
            <Close />
          </span>
        </DialogTitle>
        {data?.content && (
          <DialogContent className="modal__content">
            <div className="modal__card">
              <div>
                <p className="modal__title">Subject Name</p>
                <p className="modal__subtitle">{data?.content} </p>
              </div>
            </div>
          </DialogContent>
        )}
      </div>
    </Dialog>
  );
};

export default ModalMessage;
