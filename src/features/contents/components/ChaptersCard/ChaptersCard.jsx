import React from "react";
import { Link, useParams } from "react-router-dom";
import ProgressBar from "../../components/ProgressBar/ProgressBar";
import FavoriteButton from "../FavoriteButton";
import SubjectInfo from "../SubjectInfo/SubjectInfo";
import { ReactComponent as User } from "../../assets/icons/user.svg";
import PurchaseButton from "../PurchaseButton";
import { useDispatch, useSelector } from "react-redux";
import { chapterWishlist, chapterWishlistToggle } from "../../../../data/slices/chaptersSlice";
import { ReactComponent as Recording } from "../../assets/icons/recording.svg";

const ChaptersCard = ({
  chaptersList,
  chaptersWishlist,
  filter = false,
  favorite = false,
  studentLevel = null,
  recording = false,
}) => {
  const { subjectId } = useParams();
  const data = useSelector((state) => state.chapters.purchasedChapters.data);
  const dispatch = useDispatch();
  const chapters = chaptersList
    ?.filter((chapter) =>
      filter
        ? data.find((purchased) => purchased.chapter_id === chapter.id)?.purchase_due_date
        : true
    )
    ?.filter((chapter) =>
      favorite
        ? chaptersWishlist.find((chapterWish) => chapterWish.chapter_id === chapter.id)?.is_fav
        : true
    );
  return (
    <>
      {(chapters?.length <= 0 || !chapters) && (
        <div className="no_content">
          There is no {recording ? "recordings" : "chapters"} for now
        </div>
      )}

      {chapters?.length > 0 && (
        <section className={`cards chapters_card ${recording ? "recorded__sessions" : ""}`}>
          {chapters?.map((chapter) => (
            <div className="card" key={`${recording ? "recording" : "chapter"}${chapter.id}`}>
              <FavoriteButton
                isFavorite={
                  chaptersWishlist?.find((wishlist) => wishlist?.chapter_id === chapter?.id)?.is_fav
                }
                toggleFavorite={() => {
                  dispatch(chapterWishlistToggle({ chapter_id: chapter.id }))
                    .unwrap()
                    .then((response) => dispatch(chapterWishlist()))
                    .catch((response) => console.log(response));
                }}
              />
              <Link to={`/subjects/${subjectId || chapter?.subjects?.[0]?.id}/${chapter.id}`}>
                {!recording && (
                  <div className="card__image-container">
                    {chapter.image && <img src={chapter.image} />}
                  </div>
                )}
                {recording && (
                  <div
                    className="card__image-container recordings"
                    style={{ backgroundColor: "#3C6EE1" }}
                  >
                    <Recording />
                  </div>
                )}
              </Link>

              <div className="card__content">
                <div className="card__title">
                  <p>{chapter.name}</p>
                  <span className="tag">
                    {chapter?.levels?.[0]?.name || studentLevel?.name || "Easy"}
                  </span>
                </div>
                <div className="card__info">
                  <ProgressBar value={0} />
                </div>
                <SubjectInfo
                  chapter={chapter.videos || 0}
                  seconds={chapter.duration || 0}
                  reviews={{ rating: chapter.rating || 0, total: chapter.number_of_ratings || 0 }}
                  recording={recording}
                />
                <Link to={`/${chapter?.teacher_id}`}>
                  <div className="card__profile">
                    <p className="title">Teacher: </p>
                    {chapter?.teacher?.photo_url ? (
                      <img src={chapter?.teacher?.photo_url} />
                    ) : (
                      <User />
                    )}
                    <p>{`${chapter?.teacher?.first_name} ${chapter?.teacher?.last_name}`} </p>
                  </div>
                </Link>
              </div>
              <div className="btn__container">
                <PurchaseButton
                  id={chapter.id}
                  attribute="chapter_id"
                  wishlist={data}
                  subjectId={subjectId}
                  price={chapter?.price}
                />
              </div>
            </div>
          ))}
        </section>
      )}
    </>
  );
};

export default ChaptersCard;
