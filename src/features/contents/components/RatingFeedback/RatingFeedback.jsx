import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { getReviews, postReview } from "../../../../data/slices/commentsSlice";
import { openModal } from "../../../../data/slices/modals";
import Rating from "../Rating";
import { ReactComponent as User } from "../../assets/icons/user.svg";

const RatingFeedback = () => {
  const [rating, setRating] = useState(2.5);
  const [content, setCntent] = useState("");
  const { chapterId } = useParams();
  const reviews = useSelector((state) => state.comments.reviews.data);
  console.log("reviews", reviews);
  const dispatch = useDispatch();
  return (
    <>
      <div className="rating_feedback">
        <div className="rating">
          <p className="title">Your Rating</p>
          <div className="rating_container">
            <div className="star_container">
              <Rating rating={rating} setRating={setRating} />
            </div>
            <div>
              <p>{rating} Star</p>
              <p>rating</p>
            </div>
          </div>
          <p className="title">Your Feedback</p>
          <div className="comments_form">
            <textarea
              cols="40"
              rows="3"
              placeholder={`Add your feedback`}
              id="description"
              name="description"
              type="text"
              onChange={(e) => setCntent(e.target.value)}
              value={content}
            />
            <button
              className="card_btn"
              onClick={() => {
                dispatch(
                  postReview({
                    content,
                    rating,
                    chapter_id: chapterId,
                  })
                )
                  .unwrap()
                  .then((response) => {
                    dispatch(getReviews(chapterId));
                    dispatch(
                      openModal("modal-messge", {
                        step: "confirmation",
                        title: "Review saved",
                        subtitle: `Your review was saved,${
                          content === ""
                            ? " thank for your review"
                            : "it will appear when confirmed by the administrateur"
                        }  `,
                      })
                    );
                    setCntent("");
                  })
                  .catch((err) =>
                    dispatch(
                      openModal("modal-messge", {
                        step: "failled",
                        title: "Review failled",
                        subtitle: "You have already reviewed this chapter",
                      })
                    )
                  );
              }}
            >
              Send
            </button>
          </div>
        </div>

        {reviews?.map((review) => (
          <div className="review" key={review?.id}>
            <div className="container">
              <div className="user">
                {review?.usuer?.photo_url ? <img src={review?.suer?.photo_url} /> : <User />}
                <span className="user_name">
                  {review?.user?.first_name} {review?.user?.last_name}
                </span>
              </div>
              <div className="star_container">
                <Rating readOnly={true} value={5} />
              </div>
            </div>
            <p className="content">{review?.content}</p>
          </div>
        ))}
      </div>
    </>
  );
};

export default RatingFeedback;
