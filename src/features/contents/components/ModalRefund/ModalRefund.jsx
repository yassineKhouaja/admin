import React from "react";
import { Dialog, DialogContent, DialogTitle } from "@mui/material";
import { ReactComponent as Bag } from "../../assets/icons/refundIcon.svg";
import { ReactComponent as Close } from "../../assets/icons/Close_circle.svg";
import { useDispatch, useSelector } from "react-redux";
import { useFormik } from "formik";
import Select from "../Select";
import * as Yup from "yup";
import { postRefundReasons } from "../../../../data/slices/lessonsSlice";
import { closeModal, openModal } from "../../../../data/slices/modals";

const ModalRefund = ({ id, open, handleClose, data, ...rest }) => {
  const dispatch = useDispatch();
  const formik = useFormik({
    initialValues: {
      refund_reason_message_id: "",
      description: "",
      chapter_id: "",
    },
    validationSchema: Yup.object().shape({
      refund_reason_message_id: Yup.string().required("please select a reason"),
    }),

    onSubmit: (values) => {
      dispatch(postRefundReasons(values))
        .unwrap()
        .then((originalPromiseResult) => {
          dispatch(
            openModal("modal-payment", {
              period: false,
              step: "registred",
              title: "your refund request has been successfully saved",
              subtitle: "You will get soon a response",
              btnText: "Go to wallet",
              navigate: `/wallet`,
              close: true,
            })
          );
        })
        .catch((rejectedValueOrSerializedError) => {
          dispatch(
            openModal("modal-payment", {
              period: false,
              step: "failled",
              title: "your refund request failled",
              subtitle: rejectedValueOrSerializedError?.message,
              btnText: "Go back to subjects",
              navigate: `/subjects`,
              close: true,
            })
          );
        });

      dispatch(closeModal("modal-refund"));
    },
  });

  const refundReasons = useSelector((state) => state.lessons.refundReasons.data);
  return (
    <Dialog
      open={open}
      onClose={(e, reason) => {
        if (reason !== "backdropClick" && reason !== "escapeKeyDown") {
          handleClose(id);
        }
      }}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
      PaperProps={{
        style: { borderRadius: 16, maxWidth: "unset" },
      }}
      className="refund__dialog"
    >
      <div className="refund__modal">
        <DialogTitle id="alert-dialog-title" className="modal__title">
          <div className={"icon__container"}>
            <Bag />
          </div>
          <p>Refund this Chapter</p>

          <span className="close-btn" onClick={() => handleClose(id)}>
            <Close />
          </span>
        </DialogTitle>
        <DialogContent className="modal__content">
          <form className="form" onSubmit={formik.handleSubmit}>
            <label htmlFor="reason">
              Reason <span>*</span>
            </label>
            <Select options={refundReasons} formik={formik} />

            <label htmlFor="description">Description</label>
            <textarea
              cols="40"
              rows="3"
              placeholder="Add your description"
              id="description"
              name="description"
              type="text"
              onChange={formik.handleChange}
              value={formik.values.description}
            />
            <button
              type="submit"
              className="card_btn"
              onClick={() => {
                formik.setFieldValue("chapter_id", data.chapterId);
              }}
            >
              Send
            </button>
          </form>
        </DialogContent>
      </div>
    </Dialog>
  );
};

export default ModalRefund;
