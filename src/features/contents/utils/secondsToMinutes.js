const secondsToMinutes = (_seconds) => {
  const minutes = Math.floor(_seconds / 60);

  const seconds = _seconds % 60;

  function padTo2Digits(num) {
    return num.toString().padStart(2, "0");
  }

  return `${padTo2Digits(minutes)}:${padTo2Digits(seconds)}`;
};
export default secondsToMinutes;
