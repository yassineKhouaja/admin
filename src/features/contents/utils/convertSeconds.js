const convertSeconds = (seconds = 0) => {
  const s = parseInt(seconds, 10);
  const result = new Date(s * 1000).toISOString().slice(11, 19).split(":");
  return result[0] + " H " + result[1] + " Min " + result[2] + " Sec";
};

export default convertSeconds;
