export const lessons = [
  {
    title: "Introduction",
    videosList: [
      {
        id: 1,
        isWatched: true,
        title: "Loi de Lenz exemple 2",
        duration: "7:06",
        url: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4",
      },
      {
        id: 2,
        isWatched: true,
        title: "Loi de Lenz",
        duration: "12:06",
        url: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/WhatCarCanYouGetForAGrand.mp4",
      },
    ],
  },
  {
    title: "Lesson 0",
    videosList: [
      {
        id: 3,
        isWatched: true,
        title: "Expérience historique de Faraday 1",
        duration: "10:06",
        url: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerBlazes.mp4",
      },
      {
        id: 4,
        isWatched: false,
        title: "Expérience historique de Faraday 2",
        duration: "17:06",
        url: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerEscapes.mp4",
      },
      {
        id: 5,
        isWatched: false,
        title: "Expérience historique de Faraday 3",
        duration: "14:36",
        url: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerBlazes.mp4",
      },
    ],
  },
  {
    title: "Lesson 1",
    videosList: [
      {
        id: 6,
        isWatched: false,
        title: "Expérience de Blank",
        duration: "11:56",
        url: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/WhatCarCanYouGetForAGrand.mp4",
      },
      {
        id: 7,
        isWatched: false,
        title: "Expérience de Blank 2",
        duration: "1:06",
        url: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerBlazes.mp4",
      },
      {
        id: 8,
        isWatched: false,
        title: "Expérience de Blank 3",
        duration: "7:36",
        url: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerEscapes.mp4",
      },
      {
        id: 9,
        isWatched: false,
        title: "Expérience de Blank 4",
        duration: "8:46",
        url: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/WhatCarCanYouGetForAGrand.mp4",
      },
    ],
  },
  {
    title: "Lesson 2",
    videosList: [
      {
        id: 10,
        isWatched: false,
        title: "Expérience historique de Faraday",
        duration: "9:06",
        url: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4",
      },
      {
        id: 11,
        isWatched: false,
        title: "Expérience historique de Faraday 2",
        duration: "17:06",
        url: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/WhatCarCanYouGetForAGrand.mp4",
      },
    ],
  },
];

export const magazines = [
  { id: 1, title: "Loi de Lenz", downloadable: true },
  { id: 2, title: "Loi de Lenz", downloadable: true },
  { id: 3, title: "Loi de Lenz", downloadable: false },
  { id: 4, title: "Loi de Lenz", downloadable: false },
  { id: 5, title: "Loi de Lenz", downloadable: true },
  { id: 6, title: "Loi de Lenz", downloadable: true },
  { id: 7, title: "Loi de Lenz", downloadable: true },
  { id: 8, title: "Loi de Lenz", downloadable: true },
  { id: 9, title: "Loi de Lenz", downloadable: true },
  { id: 10, title: "Loi de Lenz", downloadable: true },
  { id: 11, title: "Loi de Lenz", downloadable: true },
  { id: 12, title: "Loi de Lenz", downloadable: true },
  { id: 13, title: "Loi de Lenz", downloadable: true },
  { id: 14, title: "Loi de Lenz", downloadable: true },
  { id: 15, title: "Loi de Lenz", downloadable: true },
  { id: 16, title: "Loi de Lenz", downloadable: true },
];
