import * as React from "react";
import ChaptersCard from "../../../contents/components/ChaptersCard/ChaptersCard";
import SubjectCards from "../../../contents/components/SubjectCards";
import { ReactComponent as Chevron } from "../../assets/icons/chevron-up.svg";
import { useState } from "react";
import { useSelector } from "react-redux";

export default function LibraryAccordion() {
  const [subjectsTab, setSubjectsTab] = useState(true);
  const [chaptersTab, setChaptersTab] = useState(true);
  const [recordingsTab, setRecodingsTab] = useState(true);
  const { data } = useSelector((state) => state.subjects.subjects);
  const { data: subjectsWishlist } = useSelector((state) => state.subjects.subjectWishlist);
  const chaptersList = useSelector((state) => state.chapters.chapters.data);
  const chaptersWishlist = useSelector((state) => state.chapters.chapterWishlist.data);

  return (
    <div className="LibraryAccordion">
      <div className="tab" onClick={() => setSubjectsTab((prev) => !prev)}>
        <p>Subjects</p>
        <p className={subjectsTab ? "" : "chevron_rotate"}>
          <Chevron />
        </p>
      </div>
      {subjectsTab && (
        <div className="subjects">
          <SubjectCards subjects={data} subjectsWishlist={subjectsWishlist} favorite={true} />
        </div>
      )}
      <div className="tab" onClick={() => setChaptersTab((prev) => !prev)}>
        <p>Chapters</p>
        <p className={chaptersTab ? "" : "chevron_rotate"}>
          <Chevron />
        </p>
      </div>

      {chaptersTab && (
        <div className="chapters">
          <ChaptersCard
            chaptersList={chaptersList?.filter((chapter) => chapter?.chapter_type_id === 1)}
            chaptersWishlist={chaptersWishlist}
            favorite={true}
          />
        </div>
      )}
      <div className="tab" onClick={() => setRecodingsTab((prev) => !prev)}>
        <p>Recordings</p>
        <p className={recordingsTab ? "" : "chevron_rotate"}>
          <Chevron />
        </p>
      </div>
      {recordingsTab && (
        <div className="chapters">
          <ChaptersCard
            chaptersList={chaptersList?.filter((chapter) => chapter?.chapter_type_id === 2)}
            chaptersWishlist={chaptersWishlist}
            favorite={true}
            recording={true}
          />
        </div>
      )}
    </div>
  );
}
