import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  chapterWishlist,
  getChapters,
  getPurchasedChapters,
} from "../../../../data/slices/chaptersSlice";
import { getPeriods, getSubjects, subjectWishlist } from "../../../../data/slices/subjectsSlice";
import useIsMountedRef from "../../../../hooks/useIsMounted";
import ChaptersCard from "../../../contents/components/ChaptersCard/ChaptersCard";
import SubjectCards from "../../../contents/components/SubjectCards";
import LibraryAccordion from "../LibraryAccordion";
import TabsContainer from "../TabsContainer";

const Library = () => {
  const dispatch = useDispatch();
  const status = useSelector((state) => state.subjects.subjects.status);
  const recordingsList = useSelector((state) => state.recordings.recordings.data);
  const isMounted = useIsMountedRef();
  const { classes } = useSelector((state) => state.auth.user);

  useEffect(() => {
    if (!isMounted.current) return;
    if (status === "idle") {
      dispatch(getSubjects(classes?.[0]?.id));
      dispatch(getPeriods());
      dispatch(subjectWishlist());
      dispatch(getPurchasedChapters());
      dispatch(getChapters(classes?.[0]?.id));
      dispatch(chapterWishlist());
    }
  }, []);

  const { data } = useSelector((state) => state.subjects.subjects);
  const { data: subjectsWishlist } = useSelector((state) => state.subjects.subjectWishlist);
  const chaptersList = useSelector((state) => state.chapters.chapters.data);
  const chaptersWishlist = useSelector((state) => state.chapters.chapterWishlist.data);
  return (
    <main className="tabsContainer">
      {/* <LibrayTabs /> */}
      <TabsContainer tabs={["subjects", "chapters", "recording", "favorite"]}>
        <div className="subjects">
          <SubjectCards subjects={data} subjectsWishlist={subjectsWishlist} filter={true} />
        </div>
        <main className="chapters">
          <ChaptersCard
            chaptersList={chaptersList?.filter((chapter) => chapter?.chapter_type_id === 1)}
            chaptersWishlist={chaptersWishlist}
            filter={true}
          />
        </main>
        <main className="chapters">
          {/* <RecordedSessions liveSessions={LIVE_SESSIONS} /> */}
          <main className="chapters">
            <ChaptersCard
              chaptersList={chaptersList?.filter((chapter) => chapter?.chapter_type_id === 2)}
              chaptersWishlist={chaptersWishlist}
              filter={true}
              recording={true}
            />
          </main>
        </main>
        <LibraryAccordion />
      </TabsContainer>
    </main>
  );
};

export default Library;
