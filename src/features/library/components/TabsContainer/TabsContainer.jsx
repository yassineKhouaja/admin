import React from "react";
import { Tabs, Tab, Box } from "@mui/material";

function TabPanel(props) {
  const { children, value, index, ...other } = props;
  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ color: "black" }}>
          <div>{children}</div>
        </Box>
      )}
    </div>
  );
}

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}

function TabsContainer({ tabs, children, setTabIndex }) {
  const [value, setValue] = React.useState(0);
  const handleChange = (lessons, newValue) => {
    setValue(newValue);
    setTabIndex && setTabIndex(newValue);
  };

  return (
    <Box sx={{ width: "100%" }}>
      <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
        <Tabs
          value={value}
          onChange={handleChange}
          TabIndicatorProps={{ style: { background: "#937FF1" } }}
          aria-label="basic tabs example"
          variant="scrollable"
          scrollButtons
          allowScrollButtonsMobile
        >
          {tabs?.map((tab, index) => (
            <Tab label={tab} {...a11yProps(index)} disableRipple key={index} />
          ))}
        </Tabs>
      </Box>

      {children &&
        React.Children.map(children, (child, index) => (
          <TabPanel value={value} index={index} key={index}>
            {React.cloneElement(child, { style: { ...child?.props?.style } })}
          </TabPanel>
        ))}
    </Box>
  );
}

export default TabsContainer;
