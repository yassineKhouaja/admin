export const LIVE_SESSIONS = [
  {
    id: 1,
    title: "ANALYSE_2ème_MP",
    tag: "Math",
    teacher: "Tarek benrhaiem",
    experiationDate: "25/11/2022",
    date: "Monday, 04 July 2022 . 08:00 - 10:00",
  },
  {
    id: 2,
    title: "Physique_2ème_MP",
    tag: "Chimie",
    teacher: "Tarek benrhaiem",
    experiationDate: "10/09/2022",
    date: "Monday, 04 July 2022 . 08:00 - 10:00",
  },
  {
    id: 3,
    title: "Crystale_2ème_MP",
    tag: "Chimie",
    teacher: "Tarek benrhaiem",
    experiationDate: "20/19/2022",
    date: "Monday, 04 July 2022 . 08:00 - 10:00",
  },
  {
    id: 4,
    title: "Chimie_2ème_MP",
    tag: "Chimie",
    teacher: "Tarek benrhaiem",
    experiationDate: "29/10/2022",
    date: "Monday, 04 July 2022 . 08:00 - 10:00",
  },
];
