import React, { useState } from "react";
import { ReactComponent as Tick } from "../../assets/icons/tick.svg";
import { ReactComponent as Dash } from "../../assets/icons/dash.svg";
import Period from "../../../contents/components/Period/Period";
import { ReactComponent as Prepa } from "../../assets/icons/prepa.svg";
import { openModal } from "../../../../data/slices/modals";
import { useDispatch } from "react-redux";

const OfferCard = ({ offer }) => {
  const disptach = useDispatch();
  const [reduction, setReduction] = useState(null);
  console.log(reduction);
  return (
    <section className="offer__card">
      <div className="icon__container">
        <Prepa />
      </div>
      <p className="title">{offer?.name}</p>

      <Period setReduction={setReduction} />

      <div className="price">
        {Math.round(offer?.amount * reduction?.period * (1 - reduction?.reduction / 100)) /
          reduction?.period}
        PTS/Month
      </div>
      <ul>
        {offer?.details?.map((detail, index) => (
          <li key={index}>
            <p>
              {detail?.state === "tic" ? <Tick /> : <Dash />} <span>{detail?.item}</span>
            </p>
          </li>
        ))}
      </ul>
      <button
        className="btn__explore"
        onClick={() => disptach(openModal("modal-purshase-offer", { offer }))}
      >
        Buy Now
      </button>
    </section>
  );
};

export default OfferCard;
