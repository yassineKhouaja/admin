import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getPeriods } from "../../../../data/slices/subjectsSlice";
import useIsMountedRef from "../../../../hooks/useIsMounted";
import { OFFERS } from "../../data/offers";
import OfferCard from "../OfferCard/OfferCard";
import ReactPlayer from "react-player";
import { getOffers } from "../../../../data/slices/offersSlice";

const Offers = () => {
  const isMounted = useIsMountedRef();
  const dispatch = useDispatch();
  const data = useSelector((state) => state.subjects.periods.data);
  const offers = useSelector((state) => state.offers.offers.data);

  useEffect(() => {
    if (!isMounted.current) return;
    dispatch(getOffers());
    if (data?.length <= 0) {
      dispatch(getPeriods());
    }
  }, []);
  return (
    <main className="offers">
      <div className="container">
        <div className="sub_container">
          <div className="video_container">
            <ReactPlayer
              controls
              width={"100%"}
              height={"100%"}
              url="https://www.youtube.com/watch?v=8qpI6K3qg4w&ab_channel=Ostedhy-%D8%A3%D8%B3%D8%AA%D8%A7%D8%B0%D9%8A"
              onClick={() => {
                setIsPlaying(true);
              }}
              onPause={() => {
                setIsPlaying(false);
              }}
            />
          </div>
          <p className="description">{offers?.[0]?.description}</p>
        </div>
        <OfferCard offer={offers?.[0]} />
      </div>
    </main>
  );
};

export default Offers;
