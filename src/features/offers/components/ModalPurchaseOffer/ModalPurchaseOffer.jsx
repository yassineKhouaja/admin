import React, { useState } from "react";
import { Dialog, DialogContent, DialogTitle } from "@mui/material";
import { ReactComponent as Bag } from "../../../contents/assets/icons/Bag.svg";
import { ReactComponent as Close } from "../../assets/icons/Close_circle.svg";
import { closeModal, openModal } from "../../../../data/slices/modals";
import { useDispatch, useSelector } from "react-redux";
import { getPurchasedChapters } from "../../../../data/slices/chaptersSlice";
import { getMe } from "../../../../data/slices/authSlice";
import Button from "../../../contents/components/Button";
import Period from "../../../contents/components/Period";
import { buyOffer } from "../../../../data/slices/offersSlice";
import { subjectWishlist } from "../../../../data/slices/subjectsSlice";

const ModalPurchaseOffer = ({ id, open, handleClose, data, ...rest }) => {
  const dispatch = useDispatch();
  const [reduction, setReduction] = useState({});
  const classes = useSelector((state) => state?.auth?.user?.classes);

  const amount = data?.offer?.amount || 0;
  return (
    <Dialog
      open={open}
      onClose={(e, reason) => {
        if (reason !== "backdropClick" && reason !== "escapeKeyDown") {
          handleClose(id);
        }
      }}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
      PaperProps={{
        style: { borderRadius: 16, maxWidth: "unset" },
      }}
      className="dialog__purchase"
    >
      <div className="purchase__modal">
        <DialogTitle id="alert-dialog-title" className="modal__title">
          <div className={"icon__container"}>
            <Bag />
          </div>
          <p>{data?.offer?.name} </p>
          <span className="close-btn" onClick={() => handleClose(id)}>
            <Close />
          </span>
        </DialogTitle>
        <DialogContent className="modal__content">
          <div className="period__container">
            <p>Period</p>
            <Period setReduction={setReduction} />
          </div>
          <div className="btn__container">
            <Button
              onClick={() => {
                dispatch(
                  buyOffer({
                    reduction_period_id: reduction.id,
                  })
                )
                  .unwrap()
                  .then((originalPromiseResult) => {
                    dispatch(
                      openModal("modal-payment", {
                        period: false,
                        step: "registred",
                        title: "your payment has been successfully",
                        subtitle: "Now you have the acces to all subjects",
                        btnText: "Access the subjects",
                        navigate: `/subjects/`,
                        id: data.id,
                      })
                    );
                    dispatch(getMe());
                    dispatch(getPurchasedChapters(classes?.[0]?.id));
                    dispatch(subjectWishlist());
                  })
                  .catch((rejectedValueOrSerializedError) => {
                    dispatch(
                      openModal("modal-payment", {
                        period: false,
                        step: "failled",
                        title: "your payment failled",
                        subtitle:
                          rejectedValueOrSerializedError?.message ||
                          "Please check with the administrator",
                        btnText: "Go back to subjects",
                        navigate: `/subjects/${data.subjectId || ""}`,
                        id: data.id,
                      })
                    );
                  });

                dispatch(closeModal("modal-purshase-offer"));
              }}
              text={
                amount
                  ? `Buy Now with ${Math.round(
                      amount * reduction?.period * (1 - reduction?.reduction / 100)
                    )} PTS`
                  : "price not availiable"
              }
              type="button"
              className="h_42"
            />
          </div>
        </DialogContent>
      </div>
    </Dialog>
  );
};

export default ModalPurchaseOffer;
