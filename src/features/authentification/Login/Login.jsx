import React, { useState } from "react";
import { useLocation } from "react-router-dom";
import AuthCard from "../components/AuthCard/AuthCard";
import AuthNav from "../components/AuthNav/AuthNav";
import StudentForm from "./components/StudentForm";

export default function Login() {
  const [item, setItem] = useState(1);
  const { search } = useLocation();

  if (search) {
    const switchToken = search.includes("switchToken")
      ? search.split("=")?.[search.split("=")?.length - 1]
      : null;

    if (switchToken) window.localStorage.setItem("token", switchToken);
  }
  return (
    <AuthCard title="Login" isArrowLeft={false}>
      <AuthNav
        data={{
          text1: "Student",
          text2: "Teacher",
          item: item,
          setItem: setItem,
        }}
      />
      <StudentForm userType={item} />
    </AuthCard>
  );
}
