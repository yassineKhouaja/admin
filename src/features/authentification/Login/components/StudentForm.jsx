import React, { useState } from "react";
import { useFormik } from "formik";
import { Grid } from "@mui/material";
import Btn from "../../../../components/Button/Button";
import { login } from "../../../../data/slices/authSlice";
import { useDispatch, useSelector } from "react-redux";
import * as Yup from "yup";
import { CircularProgress, Box } from "@mui/material";

export default function StudentForm() {
  const dispatch = useDispatch();
  const { error, state } = useSelector((state) => state.auth);
  const [errorMsg, setErrMsg] = useState("");

  const formik = useFormik({
    initialValues: {
      email: "super-admin@gmail.com",
      phone: "",
      password: "test1234",
    },
    validationSchema: Yup.object().shape({
      email: Yup.string().email().required("email is required"),
      password: Yup.string()
        .min(8, "password must be at least 8 characters")
        .required("password is required"),
    }),

    onSubmit: (values) => {
      dispatch(login({ email: values.email, password: values.password })).then((res) => {
        if (res.payload) {
          setErrMsg("success login");
        } else if (res.error) {
          setErrMsg("Incorrect email or password");
        }
      });
    },
  });
  return (
    <div className="form_login_card">
      <Grid spacing={3} container component="form" onSubmit={formik.handleSubmit}>
        <Grid item xs={12}>
          <label htmlFor="email">
            Email or Phone Number <span>*</span>
          </label>
          <input
            className={formik.touched.email && formik.errors.email && "error_field"}
            // required
            placeholder="Enter your email or phone number"
            id="Email"
            name="email"
            type="email"
            onChange={formik.handleChange}
            value={formik.values.email}
            onBlur={formik.handleBlur}
          />
          <div className="error_msg">{formik.touched.email && formik.errors.email}</div>
        </Grid>

        <Grid item xs={12}>
          <label htmlFor="password">
            Password <span>*</span>
          </label>
          <input
            className={formik.touched.password && formik.errors.password && "error_field"}
            placeholder="Enter your password"
            id="Password"
            name="password"
            type="password"
            onChange={formik.handleChange}
            value={formik.values.password}
            onBlur={formik.handleBlur}
          />
          <div className="error_msg">{formik.touched.password && formik.errors.password}</div>
        </Grid>

        <Grid item xs={12} className="pwd_remember">
          <div className="s_left">
            {/* <input type="checkbox" /> */}
            <p></p>
          </div>
          <a href="/auth/forgot-password">Forgot password</a>
        </Grid>
        {state === "error" && (
          <Grid item xs={12}>
            <div className="error_msg">{errorMsg}</div>
          </Grid>
        )}

        {state === "loading" && (
          <Box className="circlar_progress">
            <CircularProgress />
          </Box>
        )}
        <Grid item xs={12}>
          <Btn type="submit" onClick={formik.handleSubmit} text="Log In" />
        </Grid>
      </Grid>
    </div>
  );
}
