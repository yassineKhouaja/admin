import React, { useState } from "react";
import AuthCard from "../components/AuthCard/AuthCard";
import { useFormik } from "formik";
import Btn from "../../../components/Button/Button";
import { useDispatch, useSelector } from "react-redux";
import * as Yup from "yup";
import { CircularProgress, Box } from "@mui/material";
import { Grid } from "@mui/material";
import { forgotPassword } from "../../../data/slices/authSlice";

export default function ForgotPassword() {
  const dispatch = useDispatch();
  const [errorMsg, setErrMsg] = useState("");
  const { error, state } = useSelector((state) => state.auth);

  const formik = useFormik({
    initialValues: {
      email: "",
    },
    validationSchema: Yup.object().shape({
      email: Yup.string().email().required("email is required"),
    }),

    onSubmit: (values) => {
      dispatch(forgotPassword({ email: values.email })).then((res) => {
        if (res.error) {
          setErrMsg("There is no user with this email");
        } else if (res.payload) {
          setErrMsg("Please check your email");
        }
      });
    },
  });

  return (
    <AuthCard
      title="Forget Password"
      subtitle="Enter you email address so you can recover your account"
      isArrowLeft={true}
    >
      <div className="form_login_card">
        <Grid
          spacing={3}
          container
          component="form"
          onSubmit={formik.handleSubmit}
        >
          <Grid item xs={12}>
            <label htmlFor="email">
              Email or Phone Number <span>*</span>
            </label>
            <input
              className={
                formik.touched.email && formik.errors.email && "error_field"
              }
              // required
              placeholder="Enter your email or phone number"
              id="Email"
              name="email"
              type="text"
              onChange={formik.handleChange}
              value={formik.values.email}
              onBlur={formik.handleBlur}
            />
            <div className="error_msg">
              {formik.touched.email && formik.errors.email}
            </div>
          </Grid>
          {state === "error" && (
            <Grid item xs={12} className="error_msg">
              {errorMsg}
            </Grid>
          )}
          {state === "succeeded" && <h3 className="success_msg">{errorMsg}</h3>}

          {state === "loading" && (
            <Box className="circlar_progress">
              <CircularProgress />
            </Box>
          )}
          <Grid item xs={12}>
            <Btn
              type="submit"
              onClick={() => console.log("forgot-password")}
              text="Recover"
            />
          </Grid>
        </Grid>
      </div>
    </AuthCard>
  );
}
