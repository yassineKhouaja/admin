import React, { useEffect, useState } from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import { Grid, Autocomplete, TextField, Tooltip } from "@mui/material";
import Btn from "../../../../components/Button/Button";
import { useDispatch, useSelector } from "react-redux";
import {
  getGovernments,
  getClasses,
} from "../../../../data/slices/contentSlice";
import { openModal } from "../../../../data/slices/modals";
import useIsMounted from "../../../../hooks/useIsMounted";
import { saveData } from "../../../../data/slices/registerData";

const max = "2012-12-31";
export default function StudentForm({ userType }) {
  const [fields, setFields] = useState({});
  const isMounted = useIsMounted();
  const dispatch = useDispatch();
  const { governments, classes } = useSelector((state) => state.content);
  const { data } = useSelector((state) => state.registerData);

  console.log(
    data.classes
      ? governments.data.filter((el) => el.id === data.government_id)
      : "j"
  );
  useEffect(() => {
    if (!isMounted.current) return;
    if (governments.data?.length === 0) {
      dispatch(getGovernments());
    }
    if (classes.data?.length === 0) {
      dispatch(getClasses());
    }
  }, [dispatch]);

  const formik = useFormik({
    initialValues: {
      first_name: data.first_name || "",
      last_name: data.last_name || "",
      email: data.email || "",
      phone: data.phone || "",
      birth_date: data.birth_date || "",
      password: data.password || "",
      classes: "",
      government_id: "",
      postal_code: data.postal_code || "",
    },
    validationSchema: Yup.object().shape({
      first_name: Yup.string()
        .min(3, "first name must be at least 3 characters")
        .matches(
          /^[a-zA-ZÀ-ÖÙ-öù-ÿĀ-žḀa-zA-Zء-ي ]+$/,
          "Please enter valid name"
        )
        .required("first name is required"),
      last_name: Yup.string()
        .min(3, "last name must be at least 3 characters")
        .matches(
          /^[a-zA-ZÀ-ÖÙ-öù-ÿĀ-žḀa-zA-Zء-ي ]+$/,
          "Please enter valid last name."
        )
        .required("last name is required"),
      email: Yup.string().email().required("email is required"),
      password: Yup.string()
        // .min(8, "Password has to be longer than 8 characters")
        .required("password is required")
        .matches(
          /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/,
          "password must Contain 8 Characters, One Uppercase, One Lowercase, One Number and one special case Character"
        ),
      phone: Yup.string()
        .matches(
          /2[0-9]{7}|4[0-9]{7}|5[0-9]{7}|9[0-9]{7}$/,
          "invalid phone number"
        )
        .min(8, "invalid phone number")
        .required("phone number is required"),
      government_id: Yup.number().required("governante is required"),
      classes: Yup.array(Yup.number().required()).required(
        "division is required"
      ),
      //
      birth_date: Yup.date()
        .max(max, `birth date should be equal or earlier than ${max}`)
        .required("birth date is required"),
      postal_code: Yup.string()
        .matches(/^[0-9]+$/, "Must be only digits")
        .min(4, "Must be exactly 4 digits")
        .max(4, "Must be exactly 4 digits")
        .required("postal code is required"),
    }),
    onSubmit: (values) => {
      dispatch(openModal("verif-modal", values));
      dispatch(saveData(values));
    },
  });

  //handle changes for select government and classes
  const handleGov = (value) => {
    if (value) {
      formik.setFieldValue("government_id", value.id);
    } else {
      formik.setFieldValue("government_id", "");
    }
  };
  const handleDivision = (value) => {
    if (value) {
      formik.setFieldValue("classes", [value.id]);
    } else {
      formik.setFieldValue("classes", "");
    }
  };
  return (
    <div className="form_login_card">
      <Grid
        container
        spacing={3}
        component="form"
        onSubmit={formik.handleSubmit}
      >
        <Grid item xs={12} sm={6}>
          <label htmlFor="email">
            First Name <span>*</span>
          </label>
          <input
            className={
              formik.touched.first_name &&
              formik.errors.first_name &&
              "error_field"
            }
            placeholder="Enter your First name"
            id="firstname"
            name="first_name"
            type="text"
            onChange={formik.handleChange}
            value={formik.values.first_name}
            onBlur={formik.handleBlur}
          />
          <div className="error_msg">
            {formik.touched.first_name && formik.errors.first_name}
          </div>
        </Grid>
        <Grid item xs={12} sm={6}>
          <label htmlFor="email">
            Last Name <span>*</span>
          </label>
          <input
            className={
              formik.touched.last_name &&
              formik.errors.last_name &&
              "error_field"
            }
            placeholder="Enter your Last name"
            id="lastname"
            name="last_name"
            type="text"
            onChange={formik.handleChange}
            value={formik.values.last_name}
            onBlur={formik.handleBlur}
          />
          <div className="error_msg">
            {formik.touched.last_name && formik.errors.last_name}
          </div>
        </Grid>

        <Grid item xs={12} sm={6}>
          <label htmlFor="email">
            Email <span>*</span>
          </label>
          <input
            className={
              formik.touched.email && formik.errors.email && "error_field"
            }
            placeholder="Enter your email adress"
            id="Email"
            name="email"
            type="email"
            onChange={formik.handleChange}
            value={formik.values.email}
            onBlur={formik.handleBlur}
          />
          <div className="error_msg">
            {formik.touched.email && formik.errors.email}
          </div>
        </Grid>
        <Grid item xs={12} sm={6}>
          <label htmlFor="password">
            Password <span>*</span>
          </label>
          <input
            className={
              formik.touched.password && formik.errors.password && "error_field"
            }
            placeholder="Enter your Password"
            id="pwd"
            name="password"
            type="password"
            onChange={formik.handleChange}
            value={formik.values.password}
            onBlur={formik.handleBlur}
          />
          <div className="error_msg">
            {formik.touched.password && formik.errors.password}
          </div>
        </Grid>
        <Grid item xs={12} sm={6}>
          <label htmlFor="phone number">
            Phone Number <span>*</span>
          </label>
          <input
            className={
              formik.touched.phone && formik.errors.phone && "error_field"
            }
            placeholder="Enter your Phone number"
            id="phone"
            name="phone"
            type="string"
            onChange={formik.handleChange}
            value={formik.values.phone}
            onBlur={formik.handleBlur}
          />
          <div className="error_msg">
            {formik.touched.phone && formik.errors.phone}
          </div>
        </Grid>
        <Grid item xs={12} sm={6}>
          <label htmlFor="birth date">
            Birth Date <span>*</span>
          </label>
          <input
            className={
              formik.touched.birth_date &&
              formik.errors.birth_date &&
              "error_field"
            }
            id="birthDate"
            name="birth_date"
            type="date"
            onChange={formik.handleChange}
            value={formik.values.birth_date}
            onBlur={formik.handleBlur}
          />
          <div className="error_msg">
            {formik.touched.birth_date && formik.errors.birth_date}
          </div>
        </Grid>
        <Grid item xs={12} sm={6} className="governorate__container">
          <label htmlFor="gov">
            Division <span>*</span>
          </label>
          <Autocomplete
            onBlur={formik.handleBlur}
            className={
              formik.touched.classes && formik.errors.classes && "error_field"
            }
            id="checkboxes-tags-demo"
            options={classes?.data ? classes.data : []}
            onChange={(event, newValue) => {
              handleDivision(newValue);
            }}
            getOptionLabel={(option) => option.name}
            renderInput={(params) => (
              <TextField {...params} placeholder="Select your devision" />
            )}
          />
          <div className="error_msg">
            {formik.touched.classes && formik.errors.classes}
          </div>
        </Grid>

        <Grid item xs={12} sm={6} className="governorate__container">
          <label htmlFor="gov">
            Governante <span>*</span>
          </label>
          <Autocomplete
            className={
              formik.touched.government_id &&
              formik.errors.government_id &&
              "error_field"
            }
            id="checkboxes-tags1-demo"
            options={governments?.data}
            onChange={(event, newValue) => {
              handleGov(newValue);
            }}
            onBlur={formik.handleBlur}
            getOptionLabel={(option) => option.name}
            renderInput={(params) => (
              <TextField {...params} placeholder="Select your governante" />
            )}
          />
          <div className="error_msg">
            {formik.touched.government_id && formik.errors.government_id}
          </div>
        </Grid>
        <Grid item xs={12} sm={12}>
          <label htmlFor="postal code">
            Postal Code <span>*</span>
          </label>
          <input
            className={
              formik.touched.postal_code &&
              formik.errors.postal_code &&
              "error_field"
            }
            id="postalCode"
            name="postal_code"
            type="number"
            onBlur={formik.handleBlur}
            onChange={formik.handleChange}
            value={formik.values.postal_code}
            placeholder="Select your postal code"
          />
          <div className="error_msg">
            {formik.touched.postal_code && formik.errors.postal_code}
          </div>
        </Grid>

        <Grid item xs={12}>
          <Btn
            type="submit"
            text="Sign Up"
            onClick={() => console.log("register")}
          />
          <p
            style={{ textAlign: "center", color: "#707c9a" }}
            className="footer_card"
          >
            Don’t have an account?
            <span style={{ textAlign: "center" }}>
              <a href="/auth/login"> Sign In</a>
            </span>
          </p>
        </Grid>
      </Grid>
    </div>
  );
}
