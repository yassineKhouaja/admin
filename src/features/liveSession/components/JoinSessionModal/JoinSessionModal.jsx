import React, { useEffect } from "react";
import { Dialog, DialogContent, DialogTitle } from "@mui/material";
import { ReactComponent as Close } from "../../assets/icons/Close_circle.svg";
import dayjs from "dayjs";
import avatarImg from "../../assets/images/avatar.svg";
import { useDispatch, useSelector } from "react-redux";
import { getAttachments } from "../../../../data/slices/eventsSlice";
import { useDate } from "../../utils/useDate";

const JoinSessionModal = ({ id, open, handleClose, data, ...rest }) => {
  // let currentTime = dayjs(new Date()).format("YYYY-MM-DDTHH:mm:ss");
  let currentTime = useDate();

  let { color } = data || {};
  let { event } = data || {};
  let { subscription } = data || {};

  const dispatch = useDispatch();
  useEffect(() => {
    if (data) {
      dispatch(getAttachments(event?.id));
    }
  }, [dispatch, data]);

  const { attachments } = useSelector((state) => state.events);

  return (
    <Dialog
      open={open}
      onClose={(e, reason) => {
        if (reason !== "backdropClick" && reason !== "escapeKeyDown") {
          handleClose(id);
        }
      }}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
      className="ta-modal join_modal"
    >
      <DialogTitle>
        <div className="modal_top">
          <span className="s_info">Session Info</span>
          <div className="close_btn1">
            <Close onClick={() => handleClose(id)} />
          </div>
        </div>
      </DialogTitle>
      <DialogContent className="content_modals">
        <div className="title">{event?.name}</div>
        <div className="session_day_time">
          <span>{dayjs(event?.start_date).format("dddd ,DD MM YYYY")}</span>
          <p style={{ margin: "10px" }}>. </p>
          <span>
            {event?.start_date.slice(11, 16)} - {event?.end_date.slice(11, 16)}
          </span>
        </div>
        <div className="m_prof">
          By:
          <span className="img_prof">
            <img
              src={event?.groupSession?.teachers?.photo_url || avatarImg}
              alt=""
            />
          </span>
          <span className="name_prof">
            {event?.groupSession?.teachers?.first_name}{" "}
            {event?.groupSession?.teachers?.last_name}
          </span>
        </div>
        <div className="divider"></div>
        <div className="level_title">Level Info</div>
        <div className="m_subject">
          Subject: <span>{event?.groupSession?.subjects[0]?.name}</span>
        </div>
        <div className="m_class">
          Class: <span>{event?.groupSession?.subjects[0]?.classes?.name}</span>
        </div>
        <div className="divider"></div>
        <div className="attachment">Attachement</div>
        {attachments?.data?.map((el, index) => (
          <div className="s_download">
            <p>
              N°{index + 1} {el?.file_name_to_show}
            </p>
            <a target="_blank" className="link_down" href={el?.url}>
              download
            </a>
          </div>
        ))}

        <a
          href={
            dayjs(currentTime).isBefore(event?.end_date)
              ? subscription?.join_link
              : `subjects/${event?.groupSession?.subjects[0]?.id}`
          }
          target={
            dayjs(currentTime).isBefore(event?.end_date) ? "_blank" : "_self"
          }
          style={{ backgroundColor: color?.principalColor }}
        >
          {dayjs(currentTime).isBefore(event?.end_date)
            ? "Join"
            : "Go To Recording"}
        </a>
      </DialogContent>
    </Dialog>
  );
};

export default JoinSessionModal;
