import React, { useState } from "react";
import { Dialog, DialogContent, DialogTitle } from "@mui/material";
import { ReactComponent as Close } from "../../assets/icons/Close_circle.svg";
import profile from "../../assets/images/avatar.svg";
import { useDispatch, useSelector } from "react-redux";
import { openModal, closeModal } from "../../../../data/slices/modals";
import SessionDetailsCard from "../SessionDetailsCard/SessionDetailsCard";
import {
  buyGroupSession,
  getSubscription,
} from "../../../../data/slices/eventsSlice";
import { CircularProgress, Box } from "@mui/material";
import getTotalPrice from "../../utils/totalPrice";
import getTotatRecordingPrice from "../../utils/totalRecordingPrice";
import { getMe } from "../../../../data/slices/authSlice";
import dayjs from "dayjs";
import { useNavigate } from "react-router-dom";

const SessionDetailsModal = ({ id, open, handleClose, data, ...rest }) => {
  const navigate = useNavigate();
  let currentTime = dayjs(new Date()).format("YYYY-MM-DDTHH:mm:ss");

  const [errorMsg, setErrMsg] = useState("");
  const [recording, setRecording] = useState(false);
  let { event } = data || {};
  let { color } = data || {};
  const { state } = useSelector((state) => state.events.events);
  const { user } = useSelector((state) => state.auth);

  const dispatch = useDispatch();

  const handleClick = () => {
    let currentTime = dayjs(new Date()).format("YYYY-MM-DDTHH:mm:ss");

    if (dayjs(currentTime).isAfter(event?.end_date)) {
      navigate(`subjects/${event?.groupSession?.subjects[0]?.id}`);
      handleClose(id);
    } else if (dayjs(currentTime).isBefore(event?.end_date)) {
      dispatch(
        buyGroupSession({ id: event?.groupSession?.id, recording: recording })
      ).then((res) => {
        if (res.payload) {
          dispatch(
            getSubscription({ id: user?.id, session_id: event?.id })
          ).then((res) => {
            if (res.payload) {
              handleClose(id);
              dispatch(
                openModal("pay-modal", {
                  event,
                  color,
                  subscription: res?.payload?.payload[0],
                  recording,
                })
              );
            } else if (res.error) {
              setErrMsg(res.error.message);
            }
          });
          dispatch(getMe());
        } else if (res.error) {
          setErrMsg(res.error.message);
        }
      });
    }
  };

  const handleLevelColor = (level) => {
    switch (level) {
      case "easy":
        return "green";
      case "meduim":
        return "#FED02B";
      default:
        return "#D93030";
    }
  };
  return (
    <Dialog
      open={open}
      onClose={(e, reason) => {
        if (reason !== "backdropClick" && reason !== "escapeKeyDown") {
          handleClose(id);
        }
      }}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
      className="ta-modal event_modal"
    >
      <DialogTitle>
        <div className="modal_top">
          <p>Groupe Session Info</p>
          <div className="close_btn">
            <Close
              onClick={() => {
                handleClose(id);
                setErrMsg("");
              }}
            />
          </div>
        </div>
        <span className="title_modal_event">{event?.groupSession?.name}</span>
        <div className="modal_event_nav">
          <div className="m_subject">
            Subject:{" "}
            <span style={{ background: color?.principalColor }}>
              {event?.groupSession?.subjects[0]?.name}
            </span>
          </div>
          <div className="m_class">
            Class:{" "}
            <span>{event?.groupSession?.subjects[0]?.classes?.name}</span>
          </div>
          <div className="m_level">
            Level:{" "}
            <span
              style={{
                background: handleLevelColor(
                  event?.groupSession?.StudentLevels?.name
                ),
              }}
            >
              {event?.groupSession?.StudentLevels?.name}
            </span>
          </div>
          <div className="m_prof">
            By:
            <span className="img_prof">
              <img
                src={event?.groupSession?.teachers?.photo_url || profile}
                alt=""
              />
            </span>
            <span className="name_prof">
              {event?.groupSession?.teachers?.first_name}{" "}
              {event?.groupSession?.teachers?.last_name}
            </span>
          </div>
        </div>
      </DialogTitle>
      <DialogContent className="content_modals">
        <div style={{ marginBottom: "25px" }} className="divider"></div>

        <div className="content_sub_title">
          {event?.groupSession?.sessions?.length} live sessions
        </div>
        <div className="sessions_cards">
          {event?.groupSession?.sessions.map((session, index) => (
            <React.Fragment key={index}>
              <SessionDetailsCard session={session} />
            </React.Fragment>
          ))}
        </div>
        <div className="divider"> </div>
        {dayjs(currentTime).isBefore(event?.end_date) && (
          <div className="modal_footer">
            <div className="s_footer_left">
              <input
                type="checkbox"
                onChange={() => setRecording(!recording)}
                defaultChecked={recording}
              ></input>
              <span>buy live session records</span>
            </div>
            <div className="s_prix">
              +{getTotatRecordingPrice(event)} PTS/month
            </div>
          </div>
        )}

        {state === "error" && (
          <div item xs={12} className="error_msg">
            {errorMsg}
          </div>
        )}
        {state === "succeeded" && <div className="success_msg">{errorMsg}</div>}

        {state === "loading" && (
          <Box className="circlar_progress">
            <CircularProgress />
          </Box>
        )}
        <button
          onClick={handleClick}
          className="btn_event"
          style={{ background: color?.principalColor }}
        >
          {!dayjs(currentTime).isBefore(event?.end_date)
            ? "Go To Recording"
            : `Buy Now with
          ${getTotalPrice(event, recording)} PTS`}
        </button>
      </DialogContent>
    </Dialog>
  );
};

export default SessionDetailsModal;
