import dayjs from "dayjs";
import React from "react";
import player from "../../assets/images/player.svg";

export default function SessionDetailsCard({ session }) {
  const getSessionState = (sessionDate) => {
    let currentTime = dayjs(new Date()).format("YYYY-MM-DDTHH:mm:ss");
    if (dayjs(sessionDate).isBefore(currentTime)) {
      return "session_statue_closed";
    } else if (dayjs(sessionDate).isAfter(currentTime)) {
      return "session_statue_upcoming";
    }
  };

  const getSessionState1 = (sessionDate) => {
    let currentTime = dayjs(new Date()).format("YYYY-MM-DDTHH:mm:ss");
    if (dayjs(sessionDate).isBefore(currentTime)) {
      return "session_big_title_closed";
    } else if (dayjs(sessionDate).isAfter(currentTime)) {
      return "session_big_title_upcoming";
    }
  };
  const getCurrentSession = (start_date, end_date) => {
    let currentTime = dayjs(new Date()).format("YYYY-MM-DDTHH:mm:ss");
    if (dayjs(currentTime).isBetween(start_date, end_date)) {
      return true;
    } else return false;
  };

  return (
    <div className="item_session">
      <div className="play_current_session">
        {!getCurrentSession(session?.start_date, session?.end_date) ? (
          <span style={{ marginRight: "15px" }}></span>
        ) : (
          <img src={player} alt="" />
        )}
      </div>
      <div
        className={
          getCurrentSession(session?.start_date, session?.end_date)
            ? `session_content current`
            : "session_content"
        }
      >
        <div className="session_title">
          <span>session1:</span>
          <div className={getSessionState(session?.start_date)}>
            {getSessionState(session?.start_date) === "session_statue_closed"
              ? "Closed"
              : "Upcoming"}
          </div>
        </div>
        <div className={getSessionState1(session?.start_date)}>
          {session?.name}
        </div>
        <div className="session_day_time">
          {/* <span>Monday, 04 July 2022 </span> */}
          <span>{dayjs(session?.start_date).format("dddd ,DD MM YYYY")}</span>
          <p style={{ margin: "10px" }}>. </p>
          <span>
            {session?.start_date.slice(11, 16)} -{" "}
            {session?.end_date.slice(11, 16)}
          </span>
        </div>
      </div>
    </div>
  );
}
