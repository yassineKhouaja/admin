import React from "react";
import { Dialog, DialogContent, DialogTitle } from "@mui/material";
import { ReactComponent as Close } from "../../assets/icons/Close_circle.svg";
import payImg from "../../assets/icons/payment.svg";
import getTotalPrice from "../../utils/totalPrice";

const PaymentModal = ({ id, open, handleClose, data, ...rest }) => {
  let { color } = data || {};
  let { event } = data || {};
  let { subscription } = data || {};
  let { recording } = data || {};

  return (
    <Dialog
      open={open}
      onClose={(e, reason) => {
        if (reason !== "backdropClick" && reason !== "escapeKeyDown") {
          handleClose(id);
        }
      }}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
      className="ta-modal pay_modal"
    >
      <DialogTitle>
        <div className="modal_top">
          <span></span>
          <img src={payImg}></img>
          <div className="close_btn">
            <Close onClick={() => handleClose(id)} />
          </div>
        </div>
      </DialogTitle>
      <DialogContent className="content_modals">
        <div className="title_pay">your payment has been successfully</div>
        <div className="sub_title_pay">
          Now you have the acces to your live session
        </div>
        <div className="content_card_pay">
          <div className="left_card">
            <p>Group Session info</p>
            <span>{event?.groupSession?.name || "--"}</span>
            <div className="nb_sessions">
              {event?.groupSession?.sessions?.length} live sessions
            </div>
            <div className="teacher">
              by {event?.groupSession?.teachers?.first_name}{" "}
              {event?.groupSession?.teachers?.last_name}
            </div>
            <div className="purchase">purchase</div>
            <div className="link_session">
              Live Session {recording === true && " + Record"}{" "}
            </div>
          </div>
          <div className="right_card">
            <p>level Info</p>
            <span>
              Subject :{" "}
              <span className="subject_item">
                {event?.groupSession?.subjects[0]?.name}
              </span>
            </span>
            <div className="division_item">
              Division:{" "}
              <span className="division">
                {event?.groupSession?.subjects[0]?.classes?.name}
              </span>
            </div>
            <div className="invoice">invoice</div>
            <div className="points">
              {getTotalPrice(event, recording) || "--"} <span>PTS</span>
            </div>
          </div>
        </div>
        <a
          href={subscription?.join_link || "notFound"}
          target="_blank"
          style={{ backgroundColor: color?.principalColor }}
        >
          Join Now
        </a>
      </DialogContent>
    </Dialog>
  );
};

export default PaymentModal;
