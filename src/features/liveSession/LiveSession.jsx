import React, { useState, useEffect, useRef, useLayoutEffect } from "react";
import arrLeft from "./assets/icons/arrowLeft1.svg";
import arrRight1 from "./assets/images/arrRight1.svg";
import * as dayjs from "dayjs";
import customParseFormat from "dayjs/plugin/customParseFormat";
import relativeTime from "dayjs/plugin/relativeTime";
import isToday from "dayjs/plugin/isToday";
import isTomorrow from "dayjs/plugin/isTomorrow";
import isYesterday from "dayjs/plugin/isYesterday";
import isBetween from "dayjs/plugin/isBetween";
import { openModal } from "../../data/slices/modals";
import { useDispatch, useSelector } from "react-redux";
import { getSessions, getSubscription } from "../../data/slices/eventsSlice";
import getColor from "./utils/handleColor";
import handleCurrentTime from "./utils/copyCurrentTime";
import { useDate } from "./utils/useDate";
import copyEventsToTimeline from "./utils/CopyEventToTimeline";
import useWindowSize from "../../hooks/useWindowSize";

export default function LiveSession() {
  const { width } = useWindowSize();
  const currentTime = useDate();
  const dispatch = useDispatch();
  const respoContEl = useRef(null);
  const calendarContEl = useRef(null);
  const { events } = useSelector((state) => state.events);
  console.log(events);
  const { user } = useSelector((state) => state.auth);

  let time = {
    start_date: currentTime,
    // start_date: "2022-10-03T22:00:00.000Z",
  };

  dayjs.locale("en");
  dayjs.extend(customParseFormat);
  dayjs.extend(relativeTime);
  dayjs.extend(isToday);
  dayjs.extend(isTomorrow);
  dayjs.extend(isYesterday);
  dayjs.extend(isBetween);
  const [offset, setOffset] = useState(0);
  const [week, setWeek] = useState([]);
  const [isEventsCopied, setIsEventsCopied] = useState(false);
  const [isTimeCopied, setIsTimeCopied] = useState(false);

  const [calendarContHeight, setCalendarContHeight] = useState(0);

  const updateCalendarDemonsions = (taH) => {
    respoContEl.current.style.height = `${taH}px`;
  };

  useLayoutEffect(() => {
    setCalendarContHeight(calendarContEl.current.clientHeight);
    updateCalendarDemonsions(calendarContHeight);
  });

  const setTimeline = (day) => {
    const timeline = [];
    for (let index = 8; index <= 24; index = index + 2) {
      timeline.push({
        id: index - 6,
        name: `${
          index === 24 ? `00:00` : index < 10 ? `0${index}:00` : `${index}:00`
        }`,
        start: `${day.format("YYYY-MM-DD")}T${
          index === 24 ? `00:00` : index < 10 ? `0${index}:00` : `${index}:00`
        }:00.000Z`,
        end: `${day.format("YYYY-MM-DD")}T${
          index === 24 ? `00:00` : index < 10 ? `0${index}:59` : `${index}:59`
        }:59.000Z`,
        events: [],
        // isCurrent: isCurrentWeek(offset),
        currentTime: [],
      });
    }
    return timeline;
  };

  const setWeekCalendar = (offset) => {
    const weekArr = [];
    for (let index = 1; index <= 7; index++) {
      weekArr.push({
        id: index,
        name: dayjs()
          .subtract(1, "day")
          .day(index + offset * 7)
          .format("dddd"),
        subName: dayjs()
          .subtract(1, "day")
          .day(index + offset * 7)
          .format("DD MMMM")
          .slice(0, 6),
        date: dayjs()
          .subtract(1, "day")
          .day(index + offset * 7)
          .format("YYYY-MM-DD"),
        today: dayjs()
          .subtract(1, "day")
          .day(index + offset * 7)
          .isToday(),
        // className: setClassName(
        //   dayjs()
        //     .subtract(1, "day")
        //     .day(index + offset * 7)
        // ),
        timeline: setTimeline(
          dayjs()
            .subtract(1, "day")
            .day(index + offset * 7)
        ),
      });
    }
    setWeek(weekArr);
  };

  useEffect(() => {
    setIsEventsCopied(false);
    dispatch(
      getSessions({
        monday: dayjs()
          .subtract(1, "day")
          .day(1 + offset * 7)
          .format("YYYY-MM-DD"),
      })
    );
    setWeekCalendar(offset);
  }, [offset]);

  useEffect(() => {
    if (events.copyEventState === "idle") {
      setIsEventsCopied(false);
    }
    if (events.copyEventState === "success" && !isEventsCopied) {
      if (events?.data.length >= 0) {
        copyEventsToTimeline(events?.data, week, setWeek, setIsEventsCopied);
      } else {
        setIsEventsCopied(true);
      }
    }
  }, [events.copyEventState, dispatch]);

  //line calendar
  useEffect(() => {
    // if (!isEventsCopied) {
    handleCurrentTime(time, week, setWeek, setIsTimeCopied);
    // }
  }, [currentTime, isTimeCopied, week, dispatch]);

  const handleClick = (event, color) => {
    dispatch(getSubscription({ id: user?.id, session_id: event?.id })).then(
      (res) => {
        if (res.payload) {
          // the student doesn't buy this session
          if (res.payload.payload.length === 0) {
            dispatch(openModal("event-modal", { event, color }));
          } else {
            // the student was buy this session
            dispatch(
              openModal("join-modal", {
                event,
                color,
                subscription: res?.payload?.payload[0],
              })
            );
          }
        } else if (res.error) {
          console.log(res.error);
        }
      }
    );
  };
  return (
    <div className="s_live_session">
      {week.length > 0 && (
        <div className="s_navigation_weeks">
          <p>{`${dayjs(week[6].date).format("MMMM YYYY")}`}</p>
          <button
            onClick={() => setOffset(offset - 1)}
            className="btn btn_left"
          >
            <img src={arrLeft} alt="" />
          </button>
          <button
            onClick={() => setOffset(offset + 1)}
            className="btn btn_right"
          >
            <img src={arrRight1} alt="" />
          </button>
        </div>
      )}
      <div className="s_divider"></div>
      <div className="s_divider_fix"></div>

      <div className="ta-calendar">
        <div className="calendar-responsive" ref={respoContEl}>
          <div className="calendar-container ta-calendar" ref={calendarContEl}>
            {week.length > 0 && (
              <>
                <div className="body">
                  {week.map((day, indexW) => {
                    return (
                      <div key={indexW} className="day">
                        <div className="s_header">
                          <div className="name">{day.name}</div>
                          <div className="sub-name">{day.subName}</div>
                        </div>
                        <div className="divider"></div>
                        <div className="time-zone"></div>
                        {day.timeline.map((timeZone, index1) => {
                          return (
                            <div className="time-zone" key={index1}>
                              {isEventsCopied === true &&
                                timeZone.events.length > 0 &&
                                timeZone.events.map((event, index) => {
                                  return (
                                    <React.Fragment key={index}>
                                      <div
                                        className={
                                          // event.isdone === true
                                          //   ? "event done"
                                          event.duration > 1.5
                                            ? "event"
                                            : "event small_event"
                                        }
                                        style={{
                                          width: `calc(100% / ${timeZone.events.length})`,
                                          height: event.duration * 40 - 1,
                                          top: event.difference * 40,
                                          left:
                                            timeZone.events.length > 1
                                              ? `${
                                                  index *
                                                  (100 / timeZone.events.length)
                                                }%`
                                              : 0,
                                          right: 0,
                                          borderLeft: `7px solid ${
                                            getColor(index).principalColor
                                          }`,
                                          background:
                                            getColor(index).secondColor,
                                        }}
                                        key={index}
                                        title={event.name}
                                        onClick={() =>
                                          handleClick(event, getColor(index))
                                        }
                                      >
                                        <div className="content_card">
                                          <div
                                            className="s_c_title"
                                            data-tip={event?.name}
                                          >
                                            {event?.name}
                                          </div>
                                          <p>
                                            {event?.groupSession?.teachers
                                              ?.first_name ||
                                              "no teacher name"}{" "}
                                            {
                                              event?.groupSession?.teachers
                                                ?.last_name
                                            }
                                          </p>
                                          <div className="s_c_footer">
                                            <div className="s_c_time">
                                              {event?.start_date.slice(11, 16)}{" "}
                                              - {event?.end_date.slice(11, 16)}
                                            </div>
                                            <div className="s_c_btn">
                                              {event?.groupSession?.subjects[0]
                                                ?.name || "------"}
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </React.Fragment>
                                  );
                                })}

                              {timeZone.currentTime.length > 0 &&
                                timeZone.currentTime.map((event, index) => {
                                  return (
                                    <React.Fragment key={index}>
                                      <div
                                        className="time_now"
                                        key={index}
                                        title={event.name}
                                        style={{
                                          top: event.difference * 40 - 1,
                                          left:
                                            width > 1600
                                              ? `-${indexW}11%`
                                              : width > 1400 && width < 1600
                                              ? `-${indexW}11%`
                                              : `-${indexW}10%`,
                                          width:
                                            width > 1090
                                              ? width - 360
                                              : width > 980 && width < 1090
                                              ? width - 90
                                              : width - 75,
                                        }}
                                      >
                                        <div className="head">
                                          <span>
                                            {currentTime.slice(11, 16)}
                                          </span>
                                        </div>
                                      </div>
                                    </React.Fragment>
                                  );
                                })}
                            </div>
                          );
                        })}
                      </div>
                    );
                  })}
                  <div className="timeline">
                    {week[0].timeline.map((timeZone, index) => {
                      return (
                        <span className="time" key={index}>
                          {timeZone.name}
                        </span>
                      );
                    })}
                  </div>
                </div>
              </>
            )}
          </div>
        </div>
      </div>
    </div>
  );
}
