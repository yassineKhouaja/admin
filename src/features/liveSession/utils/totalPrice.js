import dayjs from "dayjs";

const getTotalPrice = (event, recording) => {
  let T = 0;
  let currentTime = dayjs(new Date()).format("YYYY-MM-DDTHH:mm:ss");

  for (let i = 0; i < event?.groupSession?.sessions.length; i++) {
    if (
      dayjs(event?.groupSession?.sessions[i]?.start_date).isAfter(currentTime)
    ) {
      if (recording === false) {
        T = T + event?.groupSession?.sessions[i]?.price;
      } else {
        T =
          T +
          event?.groupSession?.sessions[i]?.price +
          event?.groupSession?.sessions[i]?.recording_price;
      }
    }
  }
  return T;
};
export default getTotalPrice;
