import dayjs from "dayjs";

const getTotatRecordingPrice = (event) => {
  let T = 0;
  let currentTime = dayjs(new Date()).format("YYYY-MM-DDTHH:mm:ss");

  for (let i = 0; i < event?.groupSession?.sessions.length; i++) {
    if (
      dayjs(event?.groupSession?.sessions[i]?.start_date).isAfter(currentTime)
    ) {
      T = T + event?.groupSession?.sessions[i]?.recording_price || 0;
    }
  }
  return T;
};
export default getTotatRecordingPrice;
