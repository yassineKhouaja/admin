import * as dayjs from "dayjs";

const handleCurrentTime = (time, week, setWeek, setIsTimeCopied) => {
  if (week.length > 0) {
    const newWeek = week;
    for (let index = 0; index < week.length; index++) {
      if (dayjs(week[index].date).isSame(time.start_date, "day")) {
        const index3 = newWeek[index].timeline.findIndex(
          (item, j) =>
            dayjs(time.start_date).isBetween(item.start, item.end) ||
            dayjs(time?.start_date).isSame(item?.start) ||
            dayjs(time?.start_date).isBetween(
              newWeek[index]?.timeline[j]?.start,
              newWeek[index]?.timeline[j + 1]?.start
            )
        );
        if (index3 > -1) {
          newWeek[index].timeline[index3].currentTime = [
            {
              ...time,
              difference: Math.abs(
                dayjs(newWeek[index].timeline[index3].start).diff(
                  dayjs(time.start_date),
                  "hour",
                  true
                )
              ),
              duration: Math.abs(
                dayjs(time.start_date).diff(dayjs(time?.end_date), "hour", true)
              ),
            },
          ];
        }
      }
    }
    // setWeek(newWeek);
    setIsTimeCopied(true);
  }
};

export default handleCurrentTime;
