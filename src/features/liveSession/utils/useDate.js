import React from "react";
import * as dayjs from "dayjs";

export const useDate = () => {
  const locale = "en";
  const [today, setDate] = React.useState(
    dayjs(new Date()).format("YYYY-MM-DDTHH:mm:ss").concat(".000Z")
  ); // Save the current date to be able to trigger an update

  React.useEffect(() => {
    const timer = setInterval(() => {
      setDate(dayjs(new Date()).format("YYYY-MM-DDTHH:mm:ss").concat(".000Z"));
    }, 10000);
    return () => {
      clearInterval(timer); // Return a funtion to clear the timer so that it will stop being called on unmount
    };
  }, []);

  return today;
};
