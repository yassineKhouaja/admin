import * as dayjs from "dayjs";

const copyEventsToTimeline = (events, week, setWeek, setIsEventsCopied) => {
  if (week.length > 0) {
    const newWeek = week;
    for (let k = 0; k < events.length; k++) {
      for (let index = 0; index < week.length; index++) {
        for (let index2 = 0; index2 < events[k].sessions.length; index2++) {
          let eventsList = events[k].sessions;

          if (
            dayjs(week[index].date).isSame(eventsList[index2].start_date, "day")
          ) {
            const index3 = newWeek[index].timeline.findIndex(
              (item, j) =>
                dayjs(eventsList[index2].start_date).isBetween(
                  item.start,
                  item.end
                ) ||
                dayjs(eventsList[index2]?.start_date).isSame(item?.start) ||
                dayjs(eventsList[index2]?.start_date).isBetween(
                  newWeek[index]?.timeline[j]?.start,
                  newWeek[index]?.timeline[j + 1]?.start
                )
            );

            if (index3 > -1) {
              newWeek[index].timeline[index3].events.push({
                ...eventsList[index2],
                difference: Math.abs(
                  dayjs(newWeek[index].timeline[index3].start).diff(
                    dayjs(eventsList[index2].start_date),
                    "hour",
                    true
                  )
                ),
                duration: Math.abs(
                  dayjs(eventsList[index2].start_date).diff(
                    dayjs(eventsList[index2].end_date),
                    "hour",
                    true
                  )
                ),
                isdone: !dayjs(eventsList[index2].end_date).isBefore(dayjs()),
                groupSession: events[k],
              });
            }
          }
        }
      }
    }
    setWeek(newWeek);
    setIsEventsCopied(true);
  }
};

export default copyEventsToTimeline;
