let colors = [
  { principalColor: "#30C1D9", secondColor: "#E9F9FB" },
  { principalColor: "#937FF1", secondColor: "#EFEDFD" },
  { principalColor: "#FED02B", secondColor: "#FFF9E6" },
  { principalColor: "#3C6EE1", secondColor: "#E9EFFC" },
  { principalColor: "#D93030", secondColor: "#FBE9E9" },
  { principalColor: "#30C1D9", secondColor: "#E9F9FB" },
  { principalColor: "#937FF1", secondColor: "#EFEDFD" },
  { principalColor: "#FED02B", secondColor: "#FFF9E6" },
  { principalColor: "#3C6EE1", secondColor: "#E9EFFC" },
  { principalColor: "#D93030", secondColor: "#FBE9E9" },
  { principalColor: "#30C1D9", secondColor: "#E9F9FB" },
  { principalColor: "#937FF1", secondColor: "#EFEDFD" },
  { principalColor: "#FED02B", secondColor: "#FFF9E6" },
  { principalColor: "#3C6EE1", secondColor: "#E9EFFC" },
  { principalColor: "#D93030", secondColor: "#FBE9E9" },
  { principalColor: "#30C1D9", secondColor: "#E9F9FB" },
  { principalColor: "#937FF1", secondColor: "#EFEDFD" },
  { principalColor: "#FED02B", secondColor: "#FFF9E6" },
  { principalColor: "#3C6EE1", secondColor: "#E9EFFC" },
  { principalColor: "#D93030", secondColor: "#FBE9E9" },
];

const getColor = (index) => {
  if (colors.length > index) {
    return colors[index];
  }

  if (colors.length <= index) {
    return colors[index - colors.length];
  }
};
export default getColor;
