import biat from "./../assets/images/biat.png";
import post from "./../assets/images/post.png";
import zitounaBank from "./../assets/images/zitouna.png";

export const PointsHistoryColumns = [
  {
    accessor: "reference",
    name: "Code",
    minWidth: 80,
    type: "text",
    width: "10%",
  },
  // {
  //   accessor: "operation",
  //   name: "Operation",
  //   minWidth: 80,
  //   type: "text",
  //   width: "10%",
  // },
  {
    accessor: "payment_method_id",
    name: "Payement Methode",
    minWidth: 80,
    type: "text",
    width: "18%",
  },
  {
    accessor: "amount",
    name: "Amount In Dinar",
    minWidth: 80,
    type: "amount",
    width: "14%",
  },
  {
    accessor: "description",
    name: "Description",
    minWidth: 80,
    type: "text",
    width: "14%",
  },
  {
    accessor: "created_at",
    name: "Date",
    minWidth: 80,
    type: "date",
    width: "12%",
  },
  {
    accessor: "status",
    name: "Status",
    minWidth: 80,
    width: "10%",
    type: "text",
  },
];
export const PointTransfertHistoryColumns = [
  {
    accessor: "recepient",
    name: "Recipient ID",
    minWidth: 80,
    type: "text",
    width: "25%",
  },
  {
    accessor: "recepients",
    name: "Recipient",
    minWidth: 80,
    type: "text",
    width: "25%",
  },
  {
    accessor: "senders",
    name: "Sender",
    minWidth: 80,
    type: "text",
    width: "25%",
  },
  {
    accessor: "amount",
    name: "Amount In Dinar",
    minWidth: 80,
    type: "amount",
    width: "25%",
  },

  {
    accessor: "created_at",
    name: "Date",
    minWidth: 80,
    type: "date",
    width: "25%",
  },
  // {
  //   accessor: "status",
  //   name: "Status",
  //   minWidth: 80,
  //   width: "8%",
  //   type: "date",
  // },
];

export const RefoundHistoryColumns = [
  {
    accessor: "id",
    name: "Demand ID",
    minWidth: 80,
    type: "text",
    width: "20%",
  },
  {
    accessor: "refund_reason_message_id",
    name: "Cause",
    minWidth: 80,
    type: "text",
    width: "20%",
  },

  {
    accessor: "created_at",
    name: "Date",
    minWidth: 80,
    type: "date",
    width: "20%",
  },
  {
    accessor: "status",
    name: "Status",
    minWidth: 80,
    width: "20%",
    type: "text",
  },
  {
    accessor: "refund_rejection_message_id",
    name: "Reason",
    minWidth: 80,
    width: "20%",
    type: "date",
  },
];
export const transactions = [
  {
    user_id: 0,
    payment_method_id: 0,
    reference: "35786100",
    amount: 400,
    description: "description",
    status: "pending",
    paymentAt: "15/01/1998",
    createdAt: "15/01/1998",
  },
  {
    user_id: 0,
    payment_method_id: 0,
    reference: "35786100",
    amount: 400,
    description: "description",
    status: "pending",
    paymentAt: "15/01/1998",
    createdAt: "15/01/1998",
  },
  {
    user_id: 1,
    payment_method_id: 0,
    reference: "35786100",
    amount: 400,
    description: "description",
    status: "approuved",
    paymentAt: "15/01/1998",
    createdAt: "15/01/1998",
  },
  {
    user_id: 3,
    payment_method_id: 0,
    reference: "35786100",
    amount: 400,
    description: "description",
    status: "pending",
    paymentAt: "15/01/1998",
    createdAt: "15/01/1998",
  },
  {
    user_id: 4,
    payment_method_id: 0,
    reference: "35786100",
    amount: 400,
    description: "description",
    status: "pending",
    paymentAt: "15/01/1998",
    createdAt: "15/01/1998",
  },
];

export const items = [
  {
    title: "Biat Bank",
    account: "Ostedhy",
    rib: "25 13 7000 0000 9567 7745",
    icon: biat,
  },
  {
    title: "Zitouna bank",
    account: "Ostedhy",
    rib: "0813 9031 0110 0012 0604",
    icon: zitounaBank,
  },
  {
    title: "Tunisian Post",
    account: "Ostedhy",
    rib: "503 0000003087195 77",
    icon: post,
  },
];
