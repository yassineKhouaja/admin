import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { openModal } from "../../../../data/slices/modals";

export default function PointCard() {
  const { user } = useSelector((state) => state.auth);
  const dispatch = useDispatch();

  const addPoint = () => {
    dispatch(openModal("modal-recharge"));
  };
  const transfertPoint = () => {
    dispatch(
      openModal("modal-points", {
        title: "Transfert Points",
        type: "transfert",
        fields: [
          {
            name: "Beneficiary ID*",
            label: "recepient",
            type: "number",
            value: "",
            required: true,
          },
          {
            name: "Amount",
            label: "amount",
            type: "number",
            value: "",
            required: true,
          },
        ],
      })
    );
  };
  return (
    <div className="point_card">
      <div className="s_balance">
        <p>Your Current Balance:</p>
        <span>
          {user?.points || "0"}
          <span className="pts"> PTS</span>
        </span>
      </div>
      <div className="card_btns">
        <button className="btn btn_add" onClick={addPoint}>
          Add Point
        </button>
        <button className="btn btn_transfert" onClick={transfertPoint}>
          Transfert Point
        </button>
      </div>
    </div>
  );
}
