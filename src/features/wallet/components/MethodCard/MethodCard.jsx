const MethodCard = ({ item, index }) => {
  return (
    <div className={`method_card method_card${index}`}>
      <div className="method_title">
        <img src={item?.icon} alt="" />
        <p>{item?.title}</p>
      </div>
      <div className="method_account">
        <p>account:</p>
        <span>{item.account}</span>
      </div>
      <div className="method_rib">
        <p>RIB:</p>
        <span>{item.rib}</span>
      </div>
    </div>
  );
};
export default MethodCard;
