import React, { useEffect, useState } from "react";
import { Dialog, DialogContent, DialogTitle } from "@mui/material";
import { ReactComponent as Close } from "../../assets/icons/Close_circle.svg";
import { useFormik } from "formik";
import { useDispatch, useSelector } from "react-redux";
import { transferPoints, addPoints } from "../../../../data/slices/walletSlice";
import { getMe } from "../../../../data/slices/authSlice";
import {
  getPointsTransferHistory,
  getTransactions,
} from "../../../../data/slices/walletSlice";
import { CircularProgress, Box } from "@mui/material";

const PointModal = ({ id, open, handleClose, data, ...rest }) => {
  const [errMsg, setErrMsg] = useState("");
  const dispatch = useDispatch();
  const { pointsTransferHistory } = useSelector((state) => state.wallet);
  const { user } = useSelector((state) => state.auth);

  const formik = useFormik({
    initialValues: {},

    onSubmit: (values) => {
      if (data?.type === "transfert") {
        setErrMsg("");
        dispatch(transferPoints(values)).then((res) => {
          if (res.payload) {
            setErrMsg("");
            dispatch(getMe());
            dispatch(getPointsTransferHistory());
            handleClose(id);
          } else if (res.error) {
            setErrMsg(res.error.message);
          }
        });
      } else if (data.id === 1) {
        setErrMsg("");
        // dispatch(getTransactions(user?.id));
        dispatch(
          addPoints({
            ...values,
            payment_method_id: data.id,
            operation: "add",
          })
        ).then((res) => {
          if (res.payload) {
            setErrMsg("");
            if (res?.payload?.transaction?.status === "approved") {
              dispatch(getMe());
            }
            dispatch(getTransactions(user?.id));
            handleClose(id);
          } else if (res.error) {
            setErrMsg(res.error.message);
          }
        });
      }
    },
  });

  useEffect(() => {
    formik.setValues({});
  }, [data]);

  return (
    <Dialog
      open={open}
      onClose={(e, reason) => {
        if (reason !== "backdropClick" && reason !== "escapeKeyDown") {
          handleClose(id);
        }
      }}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
      className="ta-modal recharge_modal"
    >
      <DialogTitle id="alert-dialog-title">
        {/* <div></div> */}
        <span className="s_modal_title">{data?.title}</span>
        <span className="close-btn" onClick={() => handleClose(id)}>
          <Close />
        </span>
      </DialogTitle>
      <DialogContent className="content_modal">
        <span>
          {data?.type === "transfert"
            ? "Add your beneficiary ID and amount"
            : "add the necessary info"}
        </span>
        <form
          className="s_modal_content_input"
          component="form"
          onSubmit={formik.handleSubmit}
        >
          {data?.fields.map((field, index) => (
            <div key={index} className="s_modal_input">
              <label htmlFor="reason">
                {field?.label} {field.required && <span>*</span>}
              </label>
              <input
                required={field.required}
                placeholder="Add your reason"
                id={field?.label}
                name={field?.label}
                type={field?.type}
                onChange={formik.handleChange}
              />
            </div>
          ))}
          {pointsTransferHistory.transferState === "succeeded" && (
            <div className="success">{errMsg}</div>
          )}

          {pointsTransferHistory.transferState === "error" && (
            <div className="error_msg">{errMsg}</div>
          )}

          {pointsTransferHistory.transferState === "loading" && (
            <Box className="circlar_progress">
              <CircularProgress />
            </Box>
          )}
          <button type="submit" className="btn btn_point">
            Add Points
          </button>
        </form>
      </DialogContent>
    </Dialog>
  );
};

export default PointModal;
