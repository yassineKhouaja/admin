import React from "react";
import {
  ListItem,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TablePagination,
  TableRow,
} from "@mui/material";
import dayjs from "dayjs";
import { useDispatch, useSelector } from "react-redux";

function Row(props) {
  const { row, index, columnsList } = props;
  const [open, setOpen] = React.useState(false);
  const { paymentMethods } = useSelector((state) => state.wallet);

  const handleMethode = (methode_id) => {
    let payment_method = "--";
    if (paymentMethods.data) {
      payment_method = paymentMethods?.data.filter(
        (el) => el.id === methode_id
      );
    }
    if (payment_method.length !== 0) {
      return payment_method[0]?.name;
    } else return "--";
  };

  const handleReasonMessage = (reason) => {
    if (reason) {
      return reason?.content;
    } else return "No Proof";
  };

  const handleDate = (date) => {
    if (date) {
      return dayjs(date).format("DD/MM/YYYY");
    } else return "--";
  };

  const handleAmount = (amount) => {
    if (amount) {
      return amount + " TND";
    } else return "--";
  };

  const handleStatus = (status) => {
    if (status === "approved" || status === true) {
      return "Approved";
    } else if (status === "pending" || status === 2) {
      return "Pending";
    } else if (status === "ignored" || status === false) {
      return "Rejected";
    } else return status;
  };

  const handleRefoundCause = (cause) => {
    if (cause?.content) {
      return cause?.content;
    } else return "--";
  };
  const handleRecepients = (recepient) => {
    if (recepient) {
      return recepient["first_name"] + " " + recepient["last_name"];
    } else return "--";
  };

  const getClassName = (i, accessor, value) => {
    if (i === 0) {
      return "first_col";
    } else if (accessor === "Status") {
      if (value === "pending" || value === 2) {
        return "status_pending";
      } else if (value === "approved" || value === true) {
        return "status_approuved";
      } else if (value === "ignored" || value === false) {
        return "status_rejected";
      }
    }
  };

  return (
    <React.Fragment>
      <TableRow>
        {columnsList?.map((column, i) => (
          <TableCell
            key={i}
            className={getClassName(i, column.name, row[column.accessor])}
            align="left"
          >
            {column.accessor == "recepients" || column.accessor == "senders"
              ? handleRecepients(row[column.accessor])
              : column.accessor == "refund_rejection_message_id"
              ? handleReasonMessage(row[column.accessor])
              : column.type == "date"
              ? handleDate(row[column.accessor])
              : column.type === "amount"
              ? handleAmount(row[column.accessor])
              : column.accessor === "payment_method_id"
              ? handleMethode(row[column.accessor])
              : column.accessor === "status"
              ? handleStatus(row[column.accessor])
              : column.accessor === "refund_reason_message_id"
              ? handleRefoundCause(row[column.accessor])
              : row[column.accessor] || <>--</>}
          </TableCell>
        ))}
      </TableRow>
    </React.Fragment>
  );
}

function ViewTable({ columnsList, data }) {
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(4);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  return (
    <Paper
      elevation={0}
      sx={{ width: "100%" }}
      className="wallet_section_table"
    >
      <TableContainer sx={{ maxHeight: 440 }}>
        <Table>
          <TableHead>
            <TableRow className="head_row">
              {columnsList.map((column, i) => (
                <TableCell
                  key={i}
                  style={{
                    top: 57,
                    minWidth: column.minWidth,
                    width: column.width,
                  }}
                >
                  {column.name}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {data
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((row, index) => {
                return (
                  <React.Fragment key={index}>
                    <Row
                      key={row.name}
                      row={row}
                      index={index}
                      columnsList={columnsList}
                    />
                  </React.Fragment>
                );
              })}
          </TableBody>
        </Table>
        {data.length === 0 && <div className="empty_data">Empty</div>}
      </TableContainer>
      {data?.length !== 0 && (
        <TablePagination
          rowsPerPageOptions={[4, 10, 100]}
          component="div"
          count={data.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      )}
    </Paper>
  );
}

export default ViewTable;
