import React from "react";
import qrCode from "../../assets/images/qrCode.png";
import d17img from "../../assets/images/d17img.png";

export default function QrCard() {
  return (
    <div className="qr_card">
      <div className="qr_card_section">
        <img src={qrCode} alt="" />
        <div className="qr_card_right">
          <img src={d17img} alt="" />
          <p>SCAN & PLAY</p>
          <span>1235468</span>
        </div>
      </div>
    </div>
  );
}
