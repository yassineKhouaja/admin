import React from "react";
import { Dialog, DialogContent, DialogTitle } from "@mui/material";
import { ReactComponent as Close } from "../../assets/icons/Close_circle.svg";
import bank_img from "../../assets/images/bank_img.png";
import online_img from "../../assets/images/online_pay.png";
import d17 from "../../assets/images/d17.png";
import { useDispatch } from "react-redux";
import { openModal, closeModal } from "../../../../data/slices/modals";

let items = [
  {
    id: 1,
    title: "Bank Transfer",
    icon: bank_img,
    fields: [
      {
        name: "code",
        label: "reference",
        type: "text",
        value: "",
        required: true,
      },
      { label: "amount", type: "number", value: "", required: true },
      { label: "description", type: "text", value: "", required: false },
    ],
  },
  {
    id: 2,
    title: "Online Payment",
    icon: online_img,
    fields: [{ label: "amount", type: "number", value: "", required: true }],
  },
  {
    id: 3,
    title: "Through D17",
    icon: d17,
    fields: [{ label: "amount", type: "number", value: "", required: true }],
  },
];
const RechargeModal = ({ id, open, handleClose, data, ...rest }) => {
  const dispatch = useDispatch();

  const handleMethode = (index) => {
    switch (index) {
      case 0:
        handleClose(id);
        dispatch(openModal("modal-points", items[index]));
        break;
      // case 1:
      //   dispatch(openModal("modal-points", items[index]));
      //   break;
      // default:
      //   dispatch(openModal("modal-points", items[index]));

      //   break;
    }
  };
  return (
    <Dialog
      open={open}
      onClose={(e, reason) => {
        if (reason !== "backdropClick" && reason !== "escapeKeyDown") {
          handleClose(id);
        }
      }}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
      className="ta-modal recharge_modal"
    >
      <DialogTitle id="alert-dialog-title">
        <div></div>
        <span className="s_modal_title">Recharge My Account</span>
        <span className="close-btn" onClick={() => handleClose(id)}>
          <Close />
        </span>
      </DialogTitle>
      <DialogContent className="content_modal">
        <span>Choose your Fill-Up method</span>
        <div className="s_modal_content">
          {items.map((item, index) => (
            <div key={index} onClick={() => handleMethode(index)}>
              <img src={item.icon} />
              <p>{item.title}</p>
            </div>
          ))}
        </div>
      </DialogContent>
    </Dialog>
  );
};

export default RechargeModal;
