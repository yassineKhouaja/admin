import React, { useEffect } from "react";
import MethodCard from "./components/MethodCard";
import PointCard from "./components/PointCard";
import ViewTable from "./components/viewTable";
import {
  items,
  PointsHistoryColumns,
  PointTransfertHistoryColumns,
  RefoundHistoryColumns,
} from "./constant/data";

import QrCard from "./components/QrCard/QrCard";
import { useDispatch, useSelector } from "react-redux";
import {
  getPointsTransferHistory,
  getTransactions,
  getRefunds,
  getPaymentMethods,
} from "../../data/slices/walletSlice";
import useIsMounted from "../../hooks/useIsMounted";

export default function Wallet() {
  const isMounted = useIsMounted();
  const dispatch = useDispatch();

  const { user } = useSelector((state) => state.auth);
  const { pointsTransferHistory, transactions, refunds, paymentMethods } =
    useSelector((state) => state.wallet);

  useEffect(() => {
    if (!isMounted.current) return;
    if (pointsTransferHistory.data.length === 0) {
      dispatch(getPointsTransferHistory());
    }
    if (transactions.data.length === 0) {
      dispatch(getTransactions(user?.id));
    }
    if (refunds.data.length === 0) {
      dispatch(getRefunds());
    }
    if (paymentMethods.data.length === 0) {
      dispatch(getPaymentMethods());
    }
  }, [dispatch]);

  return (
    <div className="section_wallet">
      <div className="points_cards">
        <PointCard />
        {items.map((el, index) => (
          <React.Fragment key={index}>
            <MethodCard index={index} item={el} />
          </React.Fragment>
        ))}
        <div className="qr_methode">
          <QrCard />
        </div>
      </div>

      <div className="points_history">
        <p className="s_point_title">Point History</p>
        <ViewTable
          columnsList={PointsHistoryColumns}
          data={transactions?.data}
        />
      </div>
      <div className="points_transfert_history">
        <p className="s_point_title">Points transfer history</p>
        <ViewTable
          columnsList={PointTransfertHistoryColumns}
          data={pointsTransferHistory?.data}
        />
      </div>
      <div className="Refound_history">
        <p className="s_point_title">Refound History</p>
        <ViewTable columnsList={RefoundHistoryColumns} data={refunds?.data} />
      </div>
    </div>
  );
}
