import React, { useEffect } from "react";
import item from "../../assets/images/subj.svg";
import { useDispatch, useSelector } from "react-redux";
import { getLastChapters } from "../../../../data/slices/contentSlice";
import useIsMounted from "../../../../hooks/useIsMounted";
import dayjs from "dayjs";
import { useNavigate } from "react-router-dom";

export default function LatestUploadVideo() {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const isMounted = useIsMounted();

  const { data, state } = useSelector((state) => state.content.lastChapters);
  useEffect(() => {
    if (!isMounted.current) return;
    if (data?.length === 0) {
      dispatch(getLastChapters());
    }
  }, [dispatch]);
  return (
    <div className="section_latest">
      <div className="section_title">Chapter</div>
      <div className="divider"></div>
      <div className="section_content">
        {data.length !== 0 ? (
          data.map((chapter, index) => (
            <div key={index} className="items">
              <div
                className="items_left"
                onClick={() =>
                  navigate(`/subjects/${chapter?.teacher?.subjects[0]?.id}`)
                }
              >
                <div className="img_chapter">
                  <img src={item}></img>
                </div>
                <div className="details">
                  <p>{chapter?.name}</p>
                  <div className="video_details">
                    <span>{chapter?.number_of_videos} video</span>
                    <p> . </p>
                    <div>
                      {(chapter?.total_duration / 3600).toFixed(1)} Hours
                    </div>
                  </div>
                </div>
              </div>
              <div className="items_right">
                <p>{dayjs(chapter?.created_at).format("DD/MM/YYYY")}</p>
                <span>
                  by{" "}
                  <span style={{ color: "#937FF1" }}>
                    {chapter?.teacher?.first_name.slice(0, 1)}
                    {"."}
                    {chapter?.teacher?.last_name}
                  </span>
                </span>
              </div>
            </div>
          ))
        ) : (
          <span className="no_data"> There is no chapter in your library.</span>
        )}
      </div>
    </div>
  );
}
