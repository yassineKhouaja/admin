import * as React from "react";
import { styled } from "@mui/material/styles";
import { Box } from "@mui/material";
import LinearProgress, {
  linearProgressClasses,
} from "@mui/material/LinearProgress";

const BorderLinearProgress = styled(LinearProgress)(({ theme }) => ({
  height: 14,
  borderRadius: 18,
  display: "inline-block",
  marginRight: "15px",
  width: "100%",
  [`&.${linearProgressClasses.colorPrimary}`]: {
    backgroundColor: "#C8E8F5",
  },
  [`& .${linearProgressClasses.bar}`]: {
    borderRadius: 18,
    backgroundColor: "#30C1D9",
  },
}));

export default function ProgressBar({ value }) {
  return (
    <>
      <Box
        style={{
          display: "flex",
          // justifyContent: "center",
          alignItems: "center",
        }}
        sx={{ flexGrow: 1 }}
      >
        <BorderLinearProgress variant="determinate" value={value} />
        <p className="progress_bar-value">{value}%</p>
      </Box>
    </>
  );
}
