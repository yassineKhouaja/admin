import React, { useEffect, useState } from "react";
import Time from "../../assets/icons/TimeCircle.svg";
import Chart from "../../assets/icons/Chart.svg";

import Video from "../../assets/icons/Video1.svg";
import folder from "../../assets/icons/Folder2.svg";
import ViewCard from "../../components/viewCard";
import { useDispatch, useSelector } from "react-redux";
import { getPurchaseChapters } from "../../../../data/slices/contentSlice";
import useIsMounted from "../../../../hooks/useIsMounted";

export default function OverView() {
  const isMounted = useIsMounted();
  const dispatch = useDispatch();

  const { data } = useSelector((state) => state.content.purchaseChapters);
  let myChapters = data.filter((item) => item?.chapter?.chapter_type_id === 1);
  let myRecording = data.filter((item) => item?.chapter?.chapter_type_id === 2);

  useEffect(() => {
    if (!isMounted.current) return;
    if (data?.length === 0) {
      dispatch(getPurchaseChapters());
    }
  }, [dispatch]);

  return (
    <div className="section_cards">
      <div className="card_item">
        <ViewCard icon={Time} title="Learning Hours" value={0} />
      </div>
      <div className="card_item">
        <ViewCard
          icon={folder}
          title="My chapters"
          value={myChapters?.length}
        />
      </div>
      <div className="card_item">
        <ViewCard
          icon={Video}
          title="Recorded Sessions"
          value={myRecording?.length}
        />
      </div>
      <div className="card_item">
        <ViewCard icon={Chart} title="learning progress" value={0} />
      </div>
    </div>
  );
}
