import * as React from "react";
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
} from "@mui/material";

import ProgressBar from "../progress/ProgressBar";
import dayjs from "dayjs";

export default function ViewTable({ type, columnsList, data, state }) {
  return (
    <div className="section_table">
      <TableContainer elevation={0} component={Paper}>
        <Table aria-label="simple table" className="table_content">
          <TableHead>
            <TableRow>
              {columnsList.map((row, index) => (
                <TableCell
                  style={{ width: row.width }}
                  key={index}
                  align="left"
                >
                  {row.name}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>

          <TableBody className="table_body">
            {data?.map((el, index) => (
              <React.Fragment key={index}>
                <TableRow
                  sx={{
                    "&:last-child td, &:last-child th": {
                      border: 0,
                      margin: 0,
                    },
                  }}
                  key={index}
                  className="table_row"
                >
                  {columnsList.map((item, i) => (
                    <TableCell
                      className={
                        item.type === "date" ? "table_cell_date" : "table_cell"
                      }
                      key={i}
                      align="left"
                      style={{ width: item.width }}
                    >
                      {i === 0 && type === "progress" && (
                        <div className="progress_icon">
                          <span></span>
                          <p>{el[item["accessor"]]}</p>
                        </div>
                      )}

                      {i === 1 && type === "progress" && (
                        <ProgressBar value={el[item["accessor"]]}></ProgressBar>
                      )}

                      {type !== "progress" &&
                        !item.accessor1 &&
                        !item.accessor2 && (
                          <p>
                            {item.type === "date" && "expire on "}
                            {dayjs(el[item["accessor"]]).format("DD/MM/YYYY")}
                          </p>
                        )}

                      {item.accessor1 && !item.accessor2 && (
                        <p>
                          {el[item["accessor"]]
                            ? el[item["accessor"]][item["accessor1"]]
                            : "null"}
                        </p>
                      )}
                      {el?.chapter?.subjects.length !== 0 &&
                        item?.accessor2 && (
                          <p>
                            {el[item?.["accessor"]]
                              ? el[item?.["accessor"]]?.[
                                  item?.["accessor1"]
                                ]?.[0][item?.["accessor2"]]
                              : ""}
                          </p>
                        )}
                    </TableCell>
                  ))}
                </TableRow>
              </React.Fragment>
            ))}
          </TableBody>
        </Table>
        {(data?.length === 0 || !data) && (
          <h3>
            There is no
            {type === "progress" ? " progress " : ` chapter in your library`}.
          </h3>
        )}
        {/* {state === "loading" && (
          <Box className="circlar_progress">
            <CircularProgress />
          </Box>
        )} */}
      </TableContainer>
    </div>
  );
}
