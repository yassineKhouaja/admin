import React from "react";

const Viewtitle = ({ title, icon }) => {
  return (
    <div className="dashboard_title">
      <img src={icon}></img>
      <span>{title}</span>
    </div>
  );
};

export default Viewtitle;
