const ViewCard = ({ icon, title, value }) => {
  return (
    <div className="view_card">
      <div className="card_left">
        <img src={icon}></img>
      </div>
      <div className="card_right">
        <p>{title}</p>
        <span>{value ? value : "-"}</span>
      </div>
    </div>
  );
};

export default ViewCard;
