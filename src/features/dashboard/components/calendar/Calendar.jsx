import * as React from "react";
import dayjs from "dayjs";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { CalendarPicker } from "@mui/x-date-pickers/CalendarPicker";
// import Arrow_Left_rectangle from "../../assets/icons/Arrow_Left_rectangle.svg";
import locale from "date-fns/locale/en-US";
import { fr } from "date-fns/locale";

export default function Calendar({ setDate, date }) {
  // console.log(date);

  return (
    <LocalizationProvider dateAdapter={AdapterDayjs} adapterLocale={fr}>
      <CalendarPicker date={date} onChange={(newDate) => setDate(newDate)} />
    </LocalizationProvider>
  );
}
