import React, { useEffect, useState } from "react";
import Calendar from "../calendar/Calendar";
import SessionCard from "../sessionCard";
import { useDispatch, useSelector } from "react-redux";
import { getSessions } from "../../../../data/slices/contentSlice";
import useIsMounted from "../../../../hooks/useIsMounted";
import dayjs from "dayjs";
import { CircularProgress, Box } from "@mui/material";
import getColor from "../../../liveSession/utils/handleColor";
export default function LiveSession() {
  const [date, setDate] = useState(dayjs());
  const isMounted = useIsMounted();
  const dispatch = useDispatch();

  const { data, state } = useSelector((state) => state.content.sessions);

  useEffect(() => {
    // if (!isMounted.current) return;
    dispatch(getSessions(dayjs(date).format("YYYY-MM-DD")));
  }, [dispatch, date]);

  return (
    <div className="section_live_session">
      <div className="s_calendar">
        <Calendar setDate={setDate} date={date} />
      </div>
      <div className="divider"></div>
      <div className="mobile_title">Today's Live Sessions</div>
      <p>Live Sessions</p>

      <div
        className={
          data?.length === 0
            ? "live_session_cards empty_section"
            : data?.length === 1
            ? "live_session_cards one_item"
            : data?.length === 2
            ? "tow_item"
            : "live_session_cards"
        }
      >
        {state === "loading" ? (
          <Box className="circlar_progress">
            <CircularProgress />
          </Box>
        ) : (
          state === "success" && (
            <>
              {data?.map((session, index) => (
                <React.Fragment key={index}>
                  <SessionCard color={getColor(index)} session={session} />
                </React.Fragment>
              ))}
            </>
          )
        )}
        {(data?.length === 0 || !data) && state !== "loading" && (
          <span className="no_data">
            No sessions for this day, have a rest .
          </span>
        )}
      </div>
    </div>
  );
}
