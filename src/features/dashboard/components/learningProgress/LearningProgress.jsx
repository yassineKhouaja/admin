import React, { useEffect, useState } from "react";
import ViewTable from "../viewTable/ViewTable";
import { useDispatch, useSelector } from "react-redux";
// import { getChapters } from "../../../../data/slices/contentSlice";

const columnsList = [
  {
    accessor: "name",
    name: "Subject",
    width: "30%",
    type: "text",
    icon: "",
  },
  {
    accessor: "progress",
    name: "Progress",
    width: "70%",
    type: "number",
    icon: "",
  },
];
const data = [
  // { name: "physique_2eme_MP", progress: 60 },
  // { name: "physique_2eme_MP", progress: 20 },
  // { name: "physique_2eme_MP", progress: 20 },
  //  { name: "physique_2eme_MP", progress: 20 },
];
export default function LearningProgress() {
  const dispatch = useDispatch();

  return (
    <div className="learning_progress_section">
      <ViewTable type="progress" columnsList={columnsList} data={data} />
    </div>
  );
}
