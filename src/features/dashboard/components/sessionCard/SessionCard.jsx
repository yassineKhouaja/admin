import React from "react";
import dayjs from "dayjs";

export default function SessionCard({ color, session }) {
  // console.log(session);
  return (
    <div
      className="session_card"
      style={{
        borderLeft: `7px solid ${color?.principalColor}`,
        background: color?.secondColor,
      }}
    >
      <div className="s_title">{session?.name}</div>
      <span>
        {session?.groupSessions?.teachers?.first_name}{" "}
        {session?.groupSessions?.teachers?.last_name}
      </span>
      <div className="s_footer">
        <div className="s_time">
          {dayjs(session.start_date).subtract(1, "hour").format("H:mm")} -{" "}
          {dayjs(session.end_date).subtract(1, "hour").format("H:mm")}
        </div>
        <div
          className="s_btn"
          style={{ backgroundColor: color?.principalColor }}
        >
          {session?.groupSessions?.subjects[0]?.name}
        </div>
      </div>
    </div>
  );
}
