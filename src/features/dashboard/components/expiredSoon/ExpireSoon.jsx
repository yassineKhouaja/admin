import React, { useEffect, useState } from "react";
import ViewTable from "../viewTable/ViewTable";
import { useDispatch, useSelector } from "react-redux";
import { getExpireSoonChapters } from "../../../../data/slices/contentSlice";
import useIsMounted from "../../../../hooks/useIsMounted";

const columnsList = [
  {
    accessor: "chapter",
    accessor1: "name",
    name: "Chapter",
    width: "30%",
    type: "text",
  },
  {
    accessor: "chapter",
    accessor1: "subjects",
    accessor2: "name",
    name: "Subject",
    width: "30%",
    type: "text",
  },
  {
    accessor: "purchase_due_date",
    name: "Date",
    width: "40%",
    type: "date",
  },
];

export default function ExpireSoon() {
  const isMounted = useIsMounted();
  const dispatch = useDispatch();

  const { data, state } = useSelector(
    (state) => state.content.expireSoonChapters
  );
  useEffect(() => {
    if (!isMounted.current) return;
    if (data?.length === 0) {
      dispatch(getExpireSoonChapters("/student/expires-soon"));
    }
  }, [dispatch]);
  return (
    <div className="expire_soon_section">
      <ViewTable
        type="expireSoon"
        state={state}
        columnsList={columnsList}
        data={data}
      />
    </div>
  );
}
