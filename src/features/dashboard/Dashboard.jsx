import React from "react";
import Overview from "./components/overview";
import ViewTitle from "./components/viewTitle";
import BarChart from "./assets/icons/BarChart.svg";
import learningProg from "./assets/icons/learningProg.svg";
import video from "./assets/icons/video.svg";
import CalendarActive from "./assets/icons/CalendarActive.svg";
import ExpireSoon from "./components/expiredSoon";
import LatestUploadVideo from "./components/lastUploadedVideo";
import LearningProgress from "./components/learningProgress";
import LiveSession from "./components/liveSession";

export default function Dashboard() {
  return (
    <div className="section_dashboard">
      <div className="section_left">
        <div className="live_session_mobile">
          <LiveSession />
        </div>
        <div className="section_overview">
          <ViewTitle icon={BarChart} title="Overview" />
          <Overview />
        </div>
        <div className="section_tabs">
          <div className="tabs_left">
            <ViewTitle icon={CalendarActive} title="Expire Soon" />
            <ExpireSoon />
            <ViewTitle icon={learningProg} title="Learning Progress" />
            <LearningProgress />
          </div>
          <div className="tabs_right">
            <ViewTitle icon={video} title=" Last Uploaded Chapters" />
            <LatestUploadVideo />
          </div>
        </div>
      </div>
      <div className="section_right">
        <LiveSession />
      </div>
    </div>
  );
}
