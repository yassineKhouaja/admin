import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom";
import ModalsProvider from "./components/ModalsProvider";
import GuestLayout from "./layouts/GuestLayout";
import MainLayout from "./layouts/MainLayout";
import { lazy } from "react";
import { Suspense } from "react";
import Progress from "./components/Prgoress/Progress";

const RoutesProvider = () => {
  const LoginView = lazy(() => import("./views/Login.js"));
  const ForgotPasswordView = lazy(() => import("./views/ForgotPassword.js"));
  const DashboardView = lazy(() => import("./views/Dashboard.js"));
  const NotFoundView = lazy(() => import("./components/NotFound/index.js"));
  return (
    <BrowserRouter>
      <Suspense fallback={<Progress />}>
        <Routes>
          {/* Guests Routes */}
          <Route path="/auth" element={<GuestLayout />}>
            <Route index element={<Navigate to="login" />} />
            <Route path="login" element={<LoginView />} />
            <Route path="forgot-password" element={<ForgotPasswordView />} />
          </Route>

          {/* Users Routes */}
          <Route path="/" element={<MainLayout />}>
            <Route index element={<DashboardView />} />
            <Route path="articles" element={<div>Articles</div>} />
            <Route path="about" element={<div>about</div>} />
            <Route path="prevention" element={<div>prevention</div>} />
            <Route path="testing" element={<div>testing</div>} />
            <Route path="transmission" element={<div>transmission</div>} />
            <Route path="living" element={<div>living</div>} />
          </Route>

          {/* Uncreated Routes */}
          <Route path="*" element={<NotFoundView />} />
        </Routes>
        <ModalsProvider />
      </Suspense>
    </BrowserRouter>
  );
};

export default RoutesProvider;
