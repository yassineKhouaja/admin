import React from "react";
import ReactDOM from "react-dom/client";
import "./index.scss";
import { store } from "./data";
import { Provider } from "react-redux";
// import "./data/__mocks__";
import AuthProvider from "./components/AuthProvider";
import RoutesProvider from "./routes";

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <Provider store={store}>
      <AuthProvider>
        <RoutesProvider />
      </AuthProvider>
    </Provider>
  </React.StrictMode>
);
