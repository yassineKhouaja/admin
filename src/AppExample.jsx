import { useEffect } from "react";
import reactLogo from "./assets/react.svg";
import { useSelector, useDispatch } from "react-redux";
import { increment } from "./data/slices/exampleSlice";
import { login } from "./data/slices/authSlice";
import useIsMounted from "./hooks/useIsMounted";

function App() {
  const isMounted = useIsMounted();
  const count = useSelector((state) => state.example.value);
  const dispatch = useDispatch();

  useEffect(() => {
    if (!isMounted.current) return;
    // dispatch(login({ email: "demo@ostedhy.io", password: "Password123" }));
  }, []);

  return (
    <div className="App">
      <div>
        <a href="https://vitejs.dev" target="_blank">
          <img src="/vite.svg" className="logo" alt="Vite logo" />
        </a>
        <a href="https://reactjs.org" target="_blank">
          <img src={reactLogo} className="logo react" alt="React logo" />
        </a>
      </div>
      <h1>Vite + React</h1>
      <div className="card">
        <button aria-label="Increment value" onClick={() => dispatch(increment())}>
          count is {count}
        </button>
        <p>
          Edit <code>src/App.jsx</code> and save to test HMR
        </p>
      </div>
      <p className="read-the-docs">Click on the Vite and React logos to learn more</p>
    </div>
  );
}

export default App;
