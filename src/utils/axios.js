import axios from "axios";
const headers = {
  Accept: "application/json",
  "Content-Type": "application/json",
};
const axiosInstance = axios.create({
  headers,
});
axiosInstance.defaults.baseURL = import.meta.env.VITE_BASE_URL;

axiosInstance.interceptors.response.use(
  (response) => response,
  (error) =>
    Promise.reject(
      (error.response && error.response.data) || "Something went wrong"
    )
);

export default axiosInstance;
